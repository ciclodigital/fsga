/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.negocio;

import br.com.fisioforma.db.Util;
import br.com.fisioforma.model.Tratamento;
import br.com.fisioforma.model.TratamentoStatusEnum;
import br.com.fisioforma.model.service.SetorService;
import br.com.fisioforma.swing.ErrorMessage;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JDialog;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.joda.time.LocalDate;

/**
 *
 * @author allan
 */
public class TratamentoAlertRPG {
    private static final Logger log = Logger.getLogger(TratamentoAlertRPG.class);
    private TratamentoAlertRPG(){}
    private static List<TratamentoAlert> tratamentosAlert = null;
    
    public static List<TratamentoAlert> getTratamentosAlert(){
        if( tratamentosAlert == null ) {
            tratamentosAlert = new ArrayList<TratamentoAlert>();
        }
        
        for( Tratamento tratamento : tratamentosCom9Meses() ) {
            tratamentosAlert.add( new TratamentoAlert(tratamento, "Este tratamento já ultrapassou 9 meses atendimento.") );
        }
        
        for( Tratamento tratamento : tratamentosCom36Atendimentos() ) {
            tratamentosAlert.add( new TratamentoAlert(tratamento, "Tratamento possui mais de 36 atendimentos") );
        }
        
        return tratamentosAlert;
    }
    
    private static List<Tratamento> getTratamentos(){
        List<Tratamento> t36 = (List<Tratamento>) tratamentosCom36Atendimentos();
        List<Tratamento> meses9 = (List<Tratamento>) tratamentosCom9Meses();
        meses9.addAll(t36);
        return meses9;
    }

    private static List<Tratamento> tratamentosCom9Meses( ) {
        Session session = null;
        List<Tratamento> tratamentos = new ArrayList<Tratamento>();
        
        // 9 meses
        try {
            session = Util.getSessionFactory().openSession();
            
            LocalDate date = LocalDate.now().minusMonths(9);
            
            Query query = session.createQuery("SELECT t from Tratamento t WHERE t.status = :status and t.setor = :setor and t.createdAt < :date");
            query.setParameter("setor", SetorService.getInstance().findBy("nome", "RPG"));
            query.setParameter("status", TratamentoStatusEnum.ANDAMENTO);
            query.setParameter("date", date.toDate() );
            
            tratamentos = (List<Tratamento>) query.list();
            
        } catch (Exception e) {
            log.error(e.getLocalizedMessage().toString(), e);
            JDialog ew = new ErrorMessage("O sistema será finalizado devido ao erro", e);
            ew.setTitle(e.getLocalizedMessage().toString());
            ew.setVisible(true);
            System.exit(0);
        }finally {
            if( session != null ) session.close();
        }
        
        return tratamentos;
    }
    
    private static List<Tratamento> tratamentosCom36Atendimentos() {
        Session session = null;
        List<Tratamento> tratamentos = new ArrayList<Tratamento>();
        
        // 9 meses
        try {
            session = Util.getSessionFactory().openSession();
            
            LocalDate date = LocalDate.now().minusMonths(9);

            Query query = session.createQuery("SELECT DISTINCT tb from Atendimento b INNER JOIN b.tratamento tb WHERE (SELECT count(a) from Atendimento a INNER JOIN a.tratamento.paciente p INNER JOIN a.tratamento t WHERE t.setor = :setor and t.createdAt > :date GROUP BY p) > 35");
            query.setParameter("setor", SetorService.getInstance().findBy("nome", "RPG"));
            query.setParameter("date", date.toDate() );
            
            tratamentos = (List<Tratamento>) query.list();
            
        } catch (Exception e) {
            log.error("Falha ao inicializar o SessionFactory", e);
            JDialog ew = new ErrorMessage("O sistema será finalizado devido ao erro", e);
            ew.setTitle("Falha ao inicializar o SessionFactory");
            ew.setVisible(true);
            System.exit(0);
        }finally {
            if( session != null ) session.close();
        }
        
        return tratamentos;
    }
}
