/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.negocio;

import br.com.fisioforma.db.Util;
import br.com.fisioforma.model.Tratamento;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author allan
 */
public class TratamentoAlert {
    private final Tratamento tratamento;
    private final String message;
    
    public TratamentoAlert ( Tratamento tratamento, String menssage ) {
        this.tratamento = tratamento;
        this.message = menssage;
    }

    public Tratamento getTratamento() {
        return tratamento;
    }
    
    public String getMessage(){
        return message;
    }
    
    public static List<TratamentoAlert> getTratamentos(){
        return TratamentoAlertRPG.getTratamentosAlert(); 
   }
            
}
