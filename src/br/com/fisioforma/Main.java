/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma;

import br.com.fisioforma.model.Usuario;
import br.com.fisioforma.view.LoadingSingleton;
import br.com.fisioforma.view.Login;
import org.apache.log4j.Logger;

/**
 *
 * @author allan
 */
public class Main {
    private static final Logger log = Logger.getLogger(Main.class.getName());
    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) {
        LoadingSingleton.show();
        
        Usuario.createAdminIfNotExists();
        
        new Login().setVisible(true);
    }
}
