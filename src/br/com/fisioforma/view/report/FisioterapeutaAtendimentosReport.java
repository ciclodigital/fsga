/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.view.report;

import br.com.fisioforma.view.*;
import br.com.fisioforma.db.Util;
import br.com.fisioforma.model.Atendimento;
import br.com.fisioforma.model.Fisioterapeuta;
import br.com.fisioforma.model.Tratamento;
import br.com.fisioforma.model.TratamentoStatusEnum;
import br.com.fisioforma.model.service.FisioterapeutaService;
import br.com.fisioforma.model.service.TratamentoService;
import br.com.fisioforma.negocio.TratamentoAlert;
import br.com.fisioforma.swing.ErrorMessage;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import org.hibernate.Query;
import org.hibernate.Session;
import org.jdesktop.swingx.combobox.ListComboBoxModel;

/**
 *
 * @author allan
 */
public class FisioterapeutaAtendimentosReport extends javax.swing.JDialog {
    private java.awt.Frame parent = null;
    private final List<Fisioterapeuta> fisioterapeutas;
    private List<Atendimento> atendimentos = new ArrayList<>();
    
    public FisioterapeutaAtendimentosReport(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        this.parent = parent;
        this.fisioterapeutas = getFisioterapeutas();
        initComponents();
        setLocationRelativeTo(null);
        setTitle("Relatório de Atendimentos por Fisioterapeuta");
        
        table.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent me) {
                JTable table =(JTable) me.getSource();
                Point p = me.getPoint();
                int row = table.rowAtPoint(p);
                if (me.getClickCount() == 2) {
                    new TratamentoForm(null, true, atendimentos.get(table.getSelectedRow()).getTratamento() ).setVisible(true);
                }
            }
        });
        
        listFisioterapeutas.setModel(new ListComboBoxModel<Fisioterapeuta>(fisioterapeutas));
    }
    
    public List<Fisioterapeuta> getFisioterapeutas(){
        try {
            return FisioterapeutaService.getInstance().list();
        } catch (Exception e) {
            return null;
        }
    }

    
    private Vector<String> createTableColumns() {
        Vector<String> columns = new Vector<String>();
        
        columns.add("Id");
        columns.add("TISS");
        columns.add("TIPO");
        columns.add("TRATAMENTO_ANDAMENTO");
        columns.add("PACIENTE");
        columns.add("PERIODO");
        columns.add("HORARIO");
        columns.add("STATUS");
        

        return columns;
    }
    
    
    public Vector<Vector<Object>> toVector( List<Atendimento> atendimentos ){
        Vector<Vector<Object>> vec = new Vector<Vector<Object>>();
        
        for (Atendimento atendimento : atendimentos) {
            Vector<Object> vet = new Vector<Object>();
            
            vet.add( atendimento.getId() );
            vet.add( atendimento.getTratamento().getTiss());
            vet.add( atendimento.getTratamento().getTipo());
            vet.add( atendimento.getTratamento().getStatus());
            vet.add( atendimento.getTratamento().getPaciente().getNome());
            vet.add( atendimento.getSetorHorario().getPeriodo() );
            vet.add( atendimento.getSetorHorario().toString() );
            vet.add( atendimento.getStatus() );
            
            vec.add(vet);
        }
        
        return vec;
    }
    
    public void update_table( List<Atendimento> atendimentos ){
        table.setModel(new DefaultTableModel(toVector(atendimentos), createTableColumns()) {
            public boolean isCellEditable(int row, int column){  
                return false;  
            }
        });
    }    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FisioterapeutaForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FisioterapeutaForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FisioterapeutaForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FisioterapeutaForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FisioterapeutaAtendimentosReport(null, true).setVisible(true);
            }
        });
    }
    
    public List<Atendimento> getAtendimentosBy(Fisioterapeuta fisioterapeuta, Date dateFrom, Date dateTo) {
        Session session = null;
        
        try {
            session = Util.getSessionFactory().openSession();
            Query query = session.getNamedQuery("Atendimento.findByFisioterapeutaAndPeriodo");
            
            query.setParameter("fisioterapeuta", fisioterapeuta);
            query.setParameter("startDate", dateFrom);
            query.setParameter("endDate", dateTo);

            return (List<Atendimento>) query.list();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();
        listFisioterapeutas = new javax.swing.JComboBox<>();
        dateFrom = new datechooser.beans.DateChooserCombo();
        jLabel1 = new javax.swing.JLabel();
        dateTo = new datechooser.beans.DateChooserCombo();
        jLabel2 = new javax.swing.JLabel();
        consultar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Alertas!!!");

        table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(table);

        listFisioterapeutas.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel1.setText("De");

        dateTo.setCalendarPreferredSize(new java.awt.Dimension(400, 300));

        jLabel2.setText("Ate");

        consultar.setText("Consultar");
        consultar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                consultarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 849, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(listFisioterapeutas, javax.swing.GroupLayout.PREFERRED_SIZE, 309, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(dateFrom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel2)
                        .addGap(6, 6, 6)
                        .addComponent(dateTo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(consultar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(listFisioterapeutas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel1))
                    .addComponent(dateFrom, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(dateTo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(9, 9, 9))
                    .addComponent(consultar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 450, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void consultarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_consultarActionPerformed
        List<Atendimento> atendimentos = getAtendimentosBy( (Fisioterapeuta) listFisioterapeutas.getSelectedItem(), dateFrom.getSelectedDate().getTime(), dateTo.getSelectedDate().getTime());        
        this.atendimentos = atendimentos;
        update_table(atendimentos);
    }//GEN-LAST:event_consultarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton consultar;
    private datechooser.beans.DateChooserCombo dateFrom;
    private datechooser.beans.DateChooserCombo dateTo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JComboBox<String> listFisioterapeutas;
    private javax.swing.JTable table;
    // End of variables declaration//GEN-END:variables
}
