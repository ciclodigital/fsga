/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.view.report;

import br.com.fisioforma.db.Util;
import br.com.fisioforma.model.Tratamento;
import br.com.fisioforma.model.TratamentoStatusEnum;
import br.com.fisioforma.model.service.TratamentoService;
import br.com.fisioforma.negocio.TratamentoAlert;
import br.com.fisioforma.swing.ErrorMessage;
import br.com.fisioforma.view.FisioterapeutaForm;
import br.com.fisioforma.view.TratamentoForm;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author allan
 */
public class TratamentosValidatorReport extends javax.swing.JDialog {
    private final List<Tratamento> tratamentos;
    private java.awt.Frame parent = null;
    
    public TratamentosValidatorReport(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        this.tratamentos = getTratamentosImcompletos();
        this.parent = parent;
        initComponents();
        
        setLocationRelativeTo(null);
        setTitle("Relatório Tratamentos Incompletos");
        
        table.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent me) {
                JTable table =(JTable) me.getSource();
                Point p = me.getPoint();
                int row = table.rowAtPoint(p);
                if (me.getClickCount() == 2) {
                    new TratamentoForm(null, true, tratamentos.get(table.getSelectedRow())).setVisible(true);
                }
            }
        });
        
        update_table(tratamentos);
    }
    
    public List<Tratamento> getTratamentosImcompletos(){
        Session session = Util.getSessionFactory().openSession();

        Query query = session.getNamedQuery("Tratamento.findByContinuidade");

        query.setParameter("status", TratamentoStatusEnum.ANDAMENTO);

        List<Tratamento> tratamentos = (List<Tratamento>) query.list();
        List<Tratamento> tratamentosFaltandoAgendamentos = new ArrayList<Tratamento>();
                
        for( Tratamento tratamento : tratamentos ) {
            if( tratamento.temEspacoParaAtendimentos() ) {
                tratamentosFaltandoAgendamentos.add(tratamento);
            }
        }

        session.close();
        
        return tratamentosFaltandoAgendamentos;
    }
    
    private Vector<String> createTableColumns() {
        Vector<String> columns = new Vector<String>();
        
        columns.add("Id");
        columns.add("SETOR");
        columns.add("TISS");
        columns.add("Paciente");
        columns.add("Fisio");
        columns.add("TOTAL");
        columns.add("FEITOS");
        columns.add("AGENDADOS");
        columns.add("Faltas");
        columns.add("Faltas Justisficada");
        columns.add("PENDENTE DE AGENDAMENTO");

        return columns;
    }
    
    
    public Vector<Vector<Object>> tratamentoListToVector( ){
        Vector<Vector<Object>> vec = new Vector<Vector<Object>>();
        
        for (Tratamento tratamento : tratamentos) {
            Vector<Object> vet = new Vector<Object>();
            
            int total = tratamento.getQuantidade();
            long feitos = tratamento.countAtendimentosFeitos();
            long agendados = tratamento.countAtendimentosAgendados();
            long faltas = tratamento.countAtendimentosFaltas();
            long faltas_justificadas = tratamento.countAtendimentosFaltaJustificada();
            long diff = total - feitos - agendados;
            
            vet.add( tratamento.getId() );
            vet.add( tratamento.getSetor().getNome());
            vet.add( tratamento.getTiss() );
            vet.add( tratamento.getPaciente().getNome() );
            if( tratamento.getFisioterapeuta() != null ) {
                vet.add( tratamento.getFisioterapeuta().getNome() );
            } else {
                vet.add( "" );
            }
            vet.add( total );
            vet.add( feitos );
            vet.add( agendados );
            vet.add( faltas );
            vet.add( faltas_justificadas );
            vet.add( diff );
            
            vec.add(vet);
        }
        
        return vec;
    }
    
    public void update_table( List<Tratamento> tratamentosAlert ){
        table.setModel(new DefaultTableModel(tratamentoListToVector( ), createTableColumns()) {
            public boolean isCellEditable(int row, int column){  
                return false;  
            }
        });
    }    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FisioterapeutaForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FisioterapeutaForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FisioterapeutaForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FisioterapeutaForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TratamentosValidatorReport(null, true).setVisible(true);
            }
        });
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Alertas!!!");

        table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(table);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 743, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 492, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable table;
    // End of variables declaration//GEN-END:variables
}
