/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.view;

import br.com.fisioforma.db.IService;
import br.com.fisioforma.db.Util;
import br.com.fisioforma.model.Paciente;
import br.com.fisioforma.model.Tratamento;
import br.com.fisioforma.model.TratamentoStatusEnum;
import br.com.fisioforma.model.TratamentoTipoEnum;
import br.com.fisioforma.model.service.PacienteService;
import br.com.fisioforma.model.service.TratamentoService;
import br.com.fisioforma.swing.ErrorMessage;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author allan
 */
public class TratamentosUsuario extends javax.swing.JDialog {

    private static final Logger log = Logger.getLogger(TratamentosUsuario.class);
    private Vector<String> columns = createTableColumns();
    private IService<Long, Tratamento> service = TratamentoService.getInstance();
    private Paciente paciente;
    private List<JCheckBox> filtersTipo = new ArrayList<JCheckBox>();
    private List<JCheckBox> filtersStatus = new ArrayList<JCheckBox>();
    private List<Tratamento> tratamentos;

    /**
     * Creates new form Tratamentos
     */
    public TratamentosUsuario(javax.swing.JFrame parent, boolean modal, Paciente paciente) {
        super(parent, modal);
        this.paciente = paciente;
        initComponents();
        setLocationRelativeTo(null);
        initFields();
    }

    private void initFields() {
        filtersStatus.add(jCheckBox1);
        filtersStatus.add(jCheckBox2);
        filtersStatus.add(jCheckBox3);
        filtersStatus.add(jCheckBox4);
        filtersStatus.add(jCheckBox5);
        filtersTipo.add(jCheckBox6);
        filtersTipo.add(jCheckBox7);

        setLocationRelativeTo(null);

        if (paciente == null) {
            actionNovoTratamento.setEnabled(false);
            actionNovaEmergencia.setEnabled(false);
        } else {
            table.addMouseListener(new MouseAdapter() {
                public void mousePressed(MouseEvent me) {
                    if (me.getClickCount() == 2) {
                        new TratamentoForm(null, true, getSelectedTratamento()).setVisible(true);

                        Session session = null;

                        try {
                            session = Util.getSessionFactory().openSession();

                            Query query = session.getNamedQuery(Tratamento.FIND_BY_PACIENTE);
                            query.setParameter("paciente", paciente);

                            update_table(query.list());
                        } catch (Exception e) {
                            e.printStackTrace();
                            System.exit(0);
                        } finally {
                            if (session != null) {
                                session.close();
                            }
                        }
                    }
                }
            });
        }
        
        filter();
    }

    private Vector<String> createTableColumns() {
        Vector<String> columns = new Vector<String>();

        columns.add("Id");
        columns.add("Empresa");
        columns.add("TISS");
        columns.add("Paciente");
        columns.add("Quantidade");
        columns.add("Setor");
        columns.add("Medico");
        columns.add("Status");
        columns.add("Tipo");

        return columns;
    }

    public Vector<Vector<Object>> tratamentoListToVector(List<Tratamento> tratamentoList) {
        Vector<Vector<Object>> vec = new Vector<Vector<Object>>();

        for (Tratamento tratamento : tratamentoList) {
            Vector<Object> vet = new Vector<Object>();

            vet.add(tratamento.getId());
            vet.add(tratamento.getPaciente().getEmpresa());
            vet.add(tratamento.getTiss());
            vet.add(tratamento.getPaciente().getNome());
            vet.add(tratamento.getQuantidade());
            vet.add(tratamento.getSetor());
            vet.add(tratamento.getNomeMedico());
            vet.add(tratamento.getStatus() + "");
            vet.add(tratamento.getTipo() + "");

            vec.add(vet);
        }

        return vec;
    }

    public void update_table(List<Tratamento> tratamentoList) {
        table.setModel(new DefaultTableModel(
                tratamentoListToVector(tratamentoList),
                columns
        ) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        });

        table.getColumnModel().getColumn(0).setMaxWidth(50);
        table.doLayout();
    }

    public void filter() {
        Session session = null;

        try {
            session = Util.getSessionFactory().openSession();
            Query query = session.getNamedQuery(Tratamento.FILTRO);

            query.setParameter("paciente", paciente);

            query.setParameterList("tipos", getSelectedTipoTratamento());
            query.setParameterList("status", getSelectedStatusTratamento());
            
            tratamentos = query.list();
            
            update_table(tratamentos);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tratamentoMenu = new javax.swing.JPopupMenu();
        actionEditarTratamento = new javax.swing.JMenuItem();
        actionVerResumo = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        actionVerAtendimentos = new javax.swing.JMenuItem();
        actionNovoMultiploAtendimento = new javax.swing.JMenuItem();
        actionRemover = new javax.swing.JMenuItem();
        jPanel2 = new javax.swing.JPanel();
        actionNovoTratamento = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jCheckBox6 = new javax.swing.JCheckBox();
        jCheckBox7 = new javax.swing.JCheckBox();
        jPanel5 = new javax.swing.JPanel();
        jCheckBox1 = new javax.swing.JCheckBox();
        jCheckBox2 = new javax.swing.JCheckBox();
        jCheckBox3 = new javax.swing.JCheckBox();
        jCheckBox4 = new javax.swing.JCheckBox();
        jCheckBox5 = new javax.swing.JCheckBox();
        jScrollPane2 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();
        actionNovaEmergencia = new javax.swing.JButton();

        actionEditarTratamento.setText("Editar Tratamento");
        actionEditarTratamento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionEditarTratamentoActionPerformed(evt);
            }
        });
        tratamentoMenu.add(actionEditarTratamento);

        actionVerResumo.setText("Ver Resumo");
        actionVerResumo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionVerResumoActionPerformed(evt);
            }
        });
        tratamentoMenu.add(actionVerResumo);
        tratamentoMenu.add(jSeparator1);

        actionVerAtendimentos.setText("Ver Atendimentos");
        actionVerAtendimentos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionVerAtendimentosActionPerformed(evt);
            }
        });
        tratamentoMenu.add(actionVerAtendimentos);

        actionNovoMultiploAtendimento.setText("Criar Multiplos Atendimentos");
        actionNovoMultiploAtendimento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionNovoMultiploAtendimentoActionPerformed(evt);
            }
        });
        tratamentoMenu.add(actionNovoMultiploAtendimento);

        actionRemover.setText("Remover");
        actionRemover.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionRemoverActionPerformed(evt);
            }
        });
        tratamentoMenu.add(actionRemover);

        setMinimumSize(new java.awt.Dimension(800, 600));
        setModal(true);

        actionNovoTratamento.setText("Novo Tratamento");
        actionNovoTratamento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionNovoTratamentoActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Filtros"));

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Tipo de Atendimento"));

        jCheckBox6.setSelected(true);
        jCheckBox6.setText("ROTINA");
        jCheckBox6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jCheckBox6MouseReleased(evt);
            }
        });

        jCheckBox7.setSelected(true);
        jCheckBox7.setText("EMERGENCIAL");
        jCheckBox7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jCheckBox7MouseReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jCheckBox6)
                .addGap(18, 18, 18)
                .addComponent(jCheckBox7)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBox6)
                    .addComponent(jCheckBox7))
                .addContainerGap(16, Short.MAX_VALUE))
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("Status do tratamento"));

        jCheckBox1.setSelected(true);
        jCheckBox1.setText("ANDAMENTO");
        jCheckBox1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jCheckBox1MouseReleased(evt);
            }
        });

        jCheckBox2.setSelected(true);
        jCheckBox2.setText("ALTA");
        jCheckBox2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jCheckBox2MouseReleased(evt);
            }
        });

        jCheckBox3.setSelected(true);
        jCheckBox3.setText("CANCELADO");
        jCheckBox3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jCheckBox3MouseReleased(evt);
            }
        });

        jCheckBox4.setSelected(true);
        jCheckBox4.setText("DESISTENCIA");
        jCheckBox4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jCheckBox4MouseReleased(evt);
            }
        });

        jCheckBox5.setSelected(true);
        jCheckBox5.setText("CONCLUIDO");
        jCheckBox5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jCheckBox5MouseReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jCheckBox1)
                .addGap(18, 18, 18)
                .addComponent(jCheckBox2)
                .addGap(18, 18, 18)
                .addComponent(jCheckBox3)
                .addGap(18, 18, 18)
                .addComponent(jCheckBox4)
                .addGap(18, 18, 18)
                .addComponent(jCheckBox5)
                .addContainerGap(112, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBox1)
                    .addComponent(jCheckBox2)
                    .addComponent(jCheckBox3)
                    .addComponent(jCheckBox4)
                    .addComponent(jCheckBox5))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        table.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        table.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tableMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tableMouseReleased(evt);
            }
        });
        jScrollPane2.setViewportView(table);

        actionNovaEmergencia.setText("Novo Tratamento Emergêncial");
        actionNovaEmergencia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionNovaEmergenciaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 817, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(actionNovoTratamento)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(actionNovaEmergencia)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(actionNovoTratamento)
                    .addComponent(actionNovaEmergencia))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 403, Short.MAX_VALUE)
                .addContainerGap())
        );

        getContentPane().add(jPanel2, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void actionNovoTratamentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionNovoTratamentoActionPerformed
        new TratamentoForm(null, true, paciente).setVisible(true);

        Session session = null;

        try {
            session = Util.getSessionFactory().openSession();

            Query query = session.getNamedQuery(Tratamento.FIND_BY_PACIENTE);
            query.setParameter("paciente", paciente);

            update_table(query.list());
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }//GEN-LAST:event_actionNovoTratamentoActionPerformed

    private void actionNovaEmergenciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionNovaEmergenciaActionPerformed
        Tratamento tratamento = new Tratamento();
        tratamento.setPaciente(paciente);
        tratamento.setTipo(TratamentoTipoEnum.EMERGENCIAL);

        new TratamentoForm(null, true, tratamento).setVisible(true);

        Session session = null;

        try {
            session = Util.getSessionFactory().openSession();

            Query query = session.getNamedQuery(Tratamento.FIND_BY_PACIENTE);
            query.setParameter("paciente", paciente);

            update_table(query.list());
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }//GEN-LAST:event_actionNovaEmergenciaActionPerformed

    private void jCheckBox6MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jCheckBox6MouseReleased
        filter();
    }//GEN-LAST:event_jCheckBox6MouseReleased

    private void jCheckBox7MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jCheckBox7MouseReleased
        filter();
    }//GEN-LAST:event_jCheckBox7MouseReleased

    private void jCheckBox1MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jCheckBox1MouseReleased
        filter();
    }//GEN-LAST:event_jCheckBox1MouseReleased

    private void jCheckBox2MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jCheckBox2MouseReleased
        filter();
    }//GEN-LAST:event_jCheckBox2MouseReleased

    private void jCheckBox3MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jCheckBox3MouseReleased
        filter();
    }//GEN-LAST:event_jCheckBox3MouseReleased

    private void jCheckBox4MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jCheckBox4MouseReleased
        filter();
    }//GEN-LAST:event_jCheckBox4MouseReleased

    private void jCheckBox5MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jCheckBox5MouseReleased
        filter();
    }//GEN-LAST:event_jCheckBox5MouseReleased

    private void actionEditarTratamentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionEditarTratamentoActionPerformed
        new TratamentoForm(null, true, getSelectedTratamento()).setVisible(true);

        Session session = null;

        try {
            session = Util.getSessionFactory().openSession();

            Query query = session.getNamedQuery(Tratamento.FIND_BY_PACIENTE);
            query.setParameter("paciente", paciente);

            update_table(query.list());
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }//GEN-LAST:event_actionEditarTratamentoActionPerformed

    private void tableMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableMousePressed
        if (evt.isPopupTrigger()) {
            tratamentoMenu.show(
                    evt.getComponent(), evt.getX(), evt.getY()
            );
        }

        int r = table.rowAtPoint(evt.getPoint());
        if (r >= 0 && r < table.getRowCount()) {
            table.setRowSelectionInterval(r, r);
        } else {
            table.clearSelection();
        }
    }//GEN-LAST:event_tableMousePressed

    private void actionVerAtendimentosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionVerAtendimentosActionPerformed
        new Atendimentos(null, true, getSelectedTratamento()).setVisible(true);
    }//GEN-LAST:event_actionVerAtendimentosActionPerformed

    private void actionNovoMultiploAtendimentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionNovoMultiploAtendimentoActionPerformed
        new AtendimentosForm(null, true, getSelectedTratamento()).setVisible(true);
    }//GEN-LAST:event_actionNovoMultiploAtendimentoActionPerformed

    private void actionVerResumoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionVerResumoActionPerformed
        new TratamentoResumo(null, true, getSelectedTratamento()).setVisible(true);
    }//GEN-LAST:event_actionVerResumoActionPerformed

    private void tableMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableMouseReleased
        tableMousePressed(evt);
    }//GEN-LAST:event_tableMouseReleased

    private void actionRemoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionRemoverActionPerformed
        Tratamento tratamento = getSelectedTratamento();
        
        int dialogResult = JOptionPane.showConfirmDialog(null, "Deseja remover este tratamento ("+ tratamento.getTiss()+")?","Warning", JOptionPane.YES_NO_OPTION);
        if(dialogResult == JOptionPane.YES_OPTION){
            if( TratamentoService.getInstance().delete(tratamento) ) {
                JOptionPane.showMessageDialog( this , "Tratamento removido com sucesso");
                filter();
            } else {
                JOptionPane.showMessageDialog( this , "Tivemos algum problema ao tentar remover este tratamento, entre em contato com o Administrador.");
            }
        }
        
    }//GEN-LAST:event_actionRemoverActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new TratamentosUsuario(null, false, PacienteService.getInstance().find(1)).setVisible(true);
                } catch (Exception e) {
                    log.error(e.getLocalizedMessage().toString(), e);
                    JDialog ew = new ErrorMessage("O sistema será finalizado devido ao erro", e);
                    ew.setTitle(e.getLocalizedMessage().toString());
                    ew.setVisible(true);
                    System.exit(0);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem actionEditarTratamento;
    private javax.swing.JButton actionNovaEmergencia;
    private javax.swing.JMenuItem actionNovoMultiploAtendimento;
    private javax.swing.JButton actionNovoTratamento;
    private javax.swing.JMenuItem actionRemover;
    private javax.swing.JMenuItem actionVerAtendimentos;
    private javax.swing.JMenuItem actionVerResumo;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JCheckBox jCheckBox2;
    private javax.swing.JCheckBox jCheckBox3;
    private javax.swing.JCheckBox jCheckBox4;
    private javax.swing.JCheckBox jCheckBox5;
    private javax.swing.JCheckBox jCheckBox6;
    private javax.swing.JCheckBox jCheckBox7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JTable table;
    private javax.swing.JPopupMenu tratamentoMenu;
    // End of variables declaration//GEN-END:variables

    private Tratamento getSelectedTratamento() {        
        return tratamentos.get(table.getSelectedRow());
    }

    private List<TratamentoStatusEnum> getSelectedStatusTratamento() {
        List<TratamentoStatusEnum> status = new ArrayList<TratamentoStatusEnum>();

        for (JCheckBox o : filtersStatus) {
            if (o.isSelected()) {
                status.add(TratamentoStatusEnum.valueOf(o.getText()));
            }
        }
        
        // Se não haver nem um status selecionado. utiliza todos
        if( status.size() == 0 ) {
            for (JCheckBox o : filtersStatus) {
                status.add(TratamentoStatusEnum.valueOf(o.getText()));
            }
        }

        return status;
    }

    private List<TratamentoTipoEnum> getSelectedTipoTratamento() {
        List<TratamentoTipoEnum> tipo = new ArrayList<TratamentoTipoEnum>();

        for (JCheckBox o : filtersTipo) {
            if (o.isSelected()) {
                tipo.add(TratamentoTipoEnum.valueOf(o.getText()));
            }
        }
        
        // Se não haver nem um tipo selecionado. utiliza todos
        if( tipo.size() == 0 ) {
            for (JCheckBox o : filtersTipo) {
                tipo.add(TratamentoTipoEnum.valueOf(o.getText()));
            }
        }

        return tipo;
    }
}
