/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.view;

import br.com.fisioforma.db.Util;
import br.com.fisioforma.model.Atendimento;
import br.com.fisioforma.model.Fisioterapeuta;
import br.com.fisioforma.model.SetorHorario;
import br.com.fisioforma.model.Tratamento;
import br.com.fisioforma.model.TratamentoStatusEnum;
import br.com.fisioforma.model.service.AtendimentoService;
import br.com.fisioforma.model.service.TratamentoService;
import br.com.fisioforma.swing.ErrorMessage;
import br.com.fisioforma.swing.Icons;
import br.com.fisioforma.swing.JButtonDate;
import br.com.fisioforma.swing.calendar.AbstractDatePickerDateValidate;
import br.com.fisioforma.swing.calendar.AbstractSelectedChangeListener;
import br.com.fisioforma.swing.calendar.DatePicker;
import br.com.fisioforma.swing.calendar.DatePickerEventType;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.ComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.ListModel;
import javax.validation.ConstraintViolation;
import javax.validation.ValidationException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.jdesktop.swingx.combobox.ListComboBoxModel;
import org.joda.time.LocalDate;

/**
 *
 * @author allan
 */
public class AtendimentosForm extends javax.swing.JDialog {
    private Tratamento tratamento;
    private Calendar cal = Calendar.getInstance();
    private List<SetorHorario> setorHorarios;
    private java.awt.Frame parent;
    private Set<Date> daysToDisable = new LinkedHashSet<Date>();
    private List<Atendimento> atendimentos = new ArrayList<Atendimento>();
    
    /**
     * Creates new form AtendimentoForm
     */
    public AtendimentosForm(java.awt.Frame parent, boolean modal, Tratamento tratamento) {
        super(parent, modal);
        this.parent = parent;
        this.tratamento = tratamento;
        initComponents();
        setLocationRelativeTo(null);
        initFields();
    }
    
    public void initFields(){
        nomeSetor.setText( tratamento.getSetor().getNome() );
        quantidadeDeAtendimentosDisponiveis.setText( tratamento.atendimentosDisponiveis() + "" );
        fisioterapeuta.setModel(getListComboBoxModel());
        
        periodoDeAtendimento.setText( "de " + tratamento.getInicio().toString() + " até " + tratamento.getFim().toString() );
        
        updateHorarios();
        
        MouseAdapter removeDate = new MouseAdapter(){

            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e); //To change body of generated methods, choose Tools | Templates.
                
                JButtonDate button = (JButtonDate) e.getComponent();
                
                datePicker.removeDate( button.getDate() );
                
                resumoDiasAtendimento.remove(button);
                resumoDiasAtendimento.repaint(50L);
                resumoDiasAtendimento.doLayout();
            }
        };
        
        datePicker.setMinDate( new LocalDate(tratamento.getInicio()) );
        datePicker.setMaxDate( new LocalDate(tratamento.getFim()) );
       
        datePicker.addSelectedListenings(new AbstractSelectedChangeListener() {
            private int count = 0;
            
            @Override
            public void changeSelectedDates(DatePickerEventType evt, DatePicker datePicker, Date selected ) {
                boolean atendimentosLimitados = getSelectedFisioterapeuta().isLimitedAtendimentoByHoraAndHorario(selected, getSelectedHorario());

                resumoDiasAtendimento.removeAll();
                
                Atendimento at = new Atendimento(
                                getSelectedFisioterapeuta(),
                                selected,
                                tratamento,
                                getSelectedHorario()
                            );
                
                if( evt == DatePickerEventType.ADD ) {
                    if( atendimentosLimitados ) {
                        JOptionPane.showMessageDialog(null, "Limite de atendimento por fisioterapeuta por data/horario é limitado à " + Fisioterapeuta.LIMITE_ATENDIMENTOS_POR_HORARIO);
                    } else {
                        atendimentos.add(at);
                    }  
                }
                
                if( evt == DatePickerEventType.REMOVE || atendimentosLimitados ) {
                    if( atendimentos.contains(at) ) {
                        atendimentos.remove(at);
                    }
                }
                
                for( LocalDate d : datePicker.getDatesSelected() ) {
                    JButtonDate btn = new JButtonDate(d.toString(), Icons.CLOSE);
                    
                    btn.setDate(d.toDate());
                    
                    resumoDiasAtendimento.add(btn);
                    
                    btn.addMouseListener(removeDate);
                }
                
                quantidadeDeAtendimentosDisponiveis.setText(
                    tratamento.atendimentosDisponiveis() - atendimentos.size()  + ""
                );
                                    
                actionSalvarEFechar.setEnabled(datePicker.getDatesSelected().size() > 0);
                
                resumoDiasAtendimento.doLayout();
            }
            
            @Override
            public void changeCurrentDate(DatePickerEventType evt, DatePicker datePicker, Date current){
                updateDaysToDisable();
            }
        });
        
        datePicker.addValidateDates(new AbstractDatePickerDateValidate() {
            public boolean validate(LocalDate day, LocalDate current) {
                return !daysToDisable.contains(day.toDate());
            }
        });
    }
    
    private void updateDaysToDisable() {
        Session session = null;
        
        if( horarios.getSelectedIndex() < 0 ) {
            horarios.setSelectedIndex(0);
        }
                        
        try {
            daysToDisable = new LinkedHashSet<Date>();
           
            session = Util.getSessionFactory().openSession();

            Query query = session.createQuery("SELECT a FROM Atendimento a WHERE a.horario = :horario AND a.fisioterapeuta = :fisioterapeuta AND a.tratamento.status = :statusTratamento AND a.status in (:status) AND a.data BETWEEN :start AND :end");
            query.setParameter("fisioterapeuta", getSelectedFisioterapeuta());
            query.setParameter("statusTratamento", TratamentoStatusEnum.ANDAMENTO);
            query.setParameter("start", datePicker.getCurrentDate().withDayOfMonth(1).toDate());
            query.setParameter("end", datePicker.getCurrentDate().plusMonths(1).withDayOfMonth(1).minusDays(1).toDate());
            query.setParameter("horario", getSelectedHorario());
            query.setParameterList("status", Atendimento.STATUS_ATENDIMENTO);
            
            ArrayList<Atendimento> atendimentos =(ArrayList<Atendimento>) query.list();

            HashMap<Date, Integer> dates = new HashMap<Date, Integer>();
            
            for( Atendimento at : atendimentos ) {
                if( dates.get(at.getData()) == null ) {
                    dates.put( at.getData(), 1 );
                } else {
                    dates.put( at.getData(), dates.get(at.getData()) + 1 );
                }
            }
            
            for(Map.Entry<Date, Integer> entry : dates.entrySet()) {
                Date key = entry.getKey();
                Integer value = (Integer) entry.getValue();

                if( value >= Fisioterapeuta.LIMITE_ATENDIMENTOS_POR_HORARIO ) {
                    daysToDisable.add(key);
                }
            }
            
            /**
             * Um paciente não pode ter de um atendimento do mesmo horário.
             */
            Query queryPaciente = session.getNamedQuery(Atendimento.FIND_BY_PACIENTE_HORARIO_AND_STATUS);
            
            queryPaciente.setParameter("paciente", tratamento.getPaciente());
            queryPaciente.setParameter("horario", getSelectedHorario());
            queryPaciente.setParameterList("status", Atendimento.STATUS_ATENDIMENTO);
            
            ArrayList<Atendimento> atendimentosPaciente =(ArrayList<Atendimento>) queryPaciente.list();
            
            for( Atendimento at : atendimentosPaciente ) {
                daysToDisable.add(at.getData());
            }
        } catch (Exception e) {
            e.printStackTrace();
            new ErrorMessage(e).setVisible(true);
        } finally {
            if( session != null ) session.close();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        datePicker = new br.com.fisioforma.swing.calendar.DatePicker(tratamento.atendimentosDisponiveis());
        jScrollPane1 = new javax.swing.JScrollPane();
        horarios = new javax.swing.JList();
        jLabel1 = new javax.swing.JLabel();
        fisioterapeuta = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        nomeSetor = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        quantidadeDeAtendimentosDisponiveis = new javax.swing.JLabel();
        actionSalvarEFechar = new javax.swing.JButton();
        actionCancelar = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        resumoDiasAtendimento = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jLabel4 = new javax.swing.JLabel();
        periodoDeAtendimento = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        datePicker.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        horarios.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        horarios.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        horarios.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                horariosValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(horarios);

        jLabel1.setText("Fisioterapeuta:");

        fisioterapeuta.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        fisioterapeuta.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                fisioterapeutaItemStateChanged(evt);
            }
        });

        jLabel2.setText("Setor:");

        nomeSetor.setText("-");

        jLabel3.setText("Quantidade Atendimentos Disponíveis:");

        quantidadeDeAtendimentosDisponiveis.setText("-");

        actionSalvarEFechar.setText("Salvar e Fechar");
        actionSalvarEFechar.setEnabled(false);
        actionSalvarEFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionSalvarEFecharActionPerformed(evt);
            }
        });

        actionCancelar.setText("Cancelar");
        actionCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionCancelarActionPerformed(evt);
            }
        });

        resumoDiasAtendimento.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT ));
        resumoDiasAtendimento.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        resumoDiasAtendimento.setMaximumSize(new java.awt.Dimension(874, 155));
        resumoDiasAtendimento.add(jScrollPane2);

        jScrollPane3.setViewportView(resumoDiasAtendimento);

        jLabel4.setText("Período de atendimento disponível:");

        periodoDeAtendimento.setText("-");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(actionCancelar)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(actionSalvarEFechar))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(quantidadeDeAtendimentosDisponiveis))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(nomeSetor)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(fisioterapeuta, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(datePicker, javax.swing.GroupLayout.PREFERRED_SIZE, 593, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 790, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(periodoDeAtendimento)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(nomeSetor)
                    .addComponent(jLabel2)
                    .addComponent(jLabel1)
                    .addComponent(fisioterapeuta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(quantidadeDeAtendimentosDisponiveis))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(periodoDeAtendimento))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1)
                    .addComponent(datePicker, javax.swing.GroupLayout.PREFERRED_SIZE, 365, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(actionSalvarEFechar)
                    .addComponent(actionCancelar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void actionCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionCancelarActionPerformed
        dispose();
    }//GEN-LAST:event_actionCancelarActionPerformed

    private void actionSalvarEFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionSalvarEFecharActionPerformed
        try {
            
            Set<ConstraintViolation<Tratamento>> valdiations = TratamentoService.getInstance().validate(tratamento);
            
            if ( valdiations.size() > 0 ) {                
                throw new ValidationException( TratamentoService.getInstance().getFormatedValidationMessages(valdiations) );
            }
            
            // Só adiciona o fisioterapeuta ao tratamento se ele ainda não
            // estiver setado.
            if( tratamento.getFisioterapeuta() != getSelectedFisioterapeuta() ) {
                tratamento.setFisioterapeuta( getSelectedFisioterapeuta() );
                TratamentoService.getInstance().saveOrUpdate(tratamento);
            }
            
            for( Atendimento atendimento : atendimentos ) {
                AtendimentoService.getInstance().saveOrUpdate(atendimento);
            }
            
            dispose();
            JOptionPane.showMessageDialog( parent , "Tratamento salvo com sucesso!");
            
            
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }//GEN-LAST:event_actionSalvarEFecharActionPerformed

    private void fisioterapeutaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_fisioterapeutaItemStateChanged
        if( evt.getStateChange() == java.awt.event.ItemEvent.SELECTED ) {
            try {
                datePicker.reset();
                resumoDiasAtendimento.removeAll();
                resumoDiasAtendimento.repaint(50L);
                resumoDiasAtendimento.doLayout();

                updateHorarios();

                quantidadeDeAtendimentosDisponiveis.setText( tratamento.atendimentosDisponiveis() - datePicker.getDatesSelected().size() + "" );
            } catch  (Exception e){
                e.printStackTrace();
            }
        }
    }//GEN-LAST:event_fisioterapeutaItemStateChanged

    private void horariosValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_horariosValueChanged
        updateDaysToDisable();
        datePicker.restarLayout();
    }//GEN-LAST:event_horariosValueChanged

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FisioterapeutaForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FisioterapeutaForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FisioterapeutaForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FisioterapeutaForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new AtendimentosForm(null, true, TratamentoService.getInstance().find(1)).setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                } 
            }
        });
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton actionCancelar;
    private javax.swing.JButton actionSalvarEFechar;
    private br.com.fisioforma.swing.calendar.DatePicker datePicker;
    private javax.swing.JComboBox fisioterapeuta;
    private javax.swing.JList horarios;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel nomeSetor;
    private javax.swing.JLabel periodoDeAtendimento;
    private javax.swing.JLabel quantidadeDeAtendimentosDisponiveis;
    private javax.swing.JPanel resumoDiasAtendimento;
    // End of variables declaration//GEN-END:variables

    private Fisioterapeuta getSelectedFisioterapeuta() {
        return (Fisioterapeuta) fisioterapeuta.getSelectedItem();
    }

    private void updateHorarios() {
        try {
            Fisioterapeuta fisioterapeuta = getSelectedFisioterapeuta();
            List<SetorHorario> setorHorarios = fisioterapeuta.getHorarios();
            horarios.setListData( setorHorarios.toArray() );
            horarios.setSelectedIndex(0);
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(parent, e.getCause().getLocalizedMessage());
        }        
    }
    
    private SetorHorario getSelectedHorario() {
        return (SetorHorario) horarios.getModel().getElementAt( horarios.getSelectedIndex() );
    }

    private ComboBoxModel getListComboBoxModel() {
        Session session = null;
        
        try {
            session = Util.getSessionFactory().openSession();
            Query query = session.getNamedQuery("Fisioterapeuta.findAllbySetorAndAtivo");
            query.setParameter("setor", tratamento.getSetor());

            return new ListComboBoxModel(
                new ArrayList<Fisioterapeuta>(
                    query.list()
                )
            );
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        
        return new ListComboBoxModel(new ArrayList<Fisioterapeuta>());
    }
}


