/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.view;

import com.toedter.calendar.DateUtil;
import com.toedter.calendar.IDateEvaluator;
import java.awt.Color;
import java.util.Date;

/**
 *
 * @author allan
 */
public class DisableWeekendEvaluator implements IDateEvaluator {
    private DateUtil dateUtil = new DateUtil();
    
    @Override
    public boolean isSpecial(Date date) {
        return false;
    }

    @Override
    public Color getSpecialForegroundColor() {
        return null;
    }

    @Override
    public Color getSpecialBackroundColor() {
        return null;
    }

    @Override
    public String getSpecialTooltip() {
        return null;
    }

    @Override
    public boolean isInvalid(Date date) {
        return true;
    }

    @Override
    public Color getInvalidForegroundColor() {
        return Color.gray;
    }

    @Override
    public Color getInvalidBackroundColor() {
        return null;
    }

    @Override
    public String getInvalidTooltip() {
        return "Dia indisponível para atendimento";
    }
    
}
