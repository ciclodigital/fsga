/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.view;

import br.com.fisioforma.db.IService;
import br.com.fisioforma.db.Util;
import br.com.fisioforma.model.Empresa;
import br.com.fisioforma.model.Paciente;
import br.com.fisioforma.model.Tratamento;
import br.com.fisioforma.model.TratamentoTipoEnum;
import br.com.fisioforma.model.service.EmpresaService;
import br.com.fisioforma.model.service.PacienteService;
import br.com.fisioforma.swing.ErrorMessage;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author allan
 */
public class Pacientes extends javax.swing.JPanel {

    private static final Logger log = Logger.getLogger(Pacientes.class);
    private Vector<String> columns = createTableColumns();
    private IService<Long, Paciente> service = PacienteService.getInstance();

    /**
     * Creates new form Pacientes
     */
    public Pacientes() {
        initComponents();

        try {
            update_table(PacienteService.getInstance().list());
            jListEmpresas.setListData( EmpresaService.getInstance().list().toArray() );
        } catch (Exception e) {
            log.error(e.getLocalizedMessage().toString(), e);
            JDialog ew = new ErrorMessage("O sistema será finalizado devido ao erro", e);
            ew.setTitle(e.getLocalizedMessage().toString());
            ew.setVisible(true);
            System.exit(0);
        }
    }

    private Vector<String> createTableColumns() {
        Vector<String> columns = new Vector<String>();

        columns.add("Id");
        columns.add("Nome");
        columns.add("Genero");
        columns.add("Matrícula");
        columns.add("Cargo");
        columns.add("Departamento");
        columns.add("Empresa");

        return columns;
    }

    public Vector<Vector<Object>> pacienteListToVector(List<Paciente> pacienteList) {
        Vector<Vector<Object>> vec = new Vector<Vector<Object>>();

        for (Paciente paciente : pacienteList) {
            Vector<Object> vet = new Vector<Object>();

            vet.add(paciente.getId());
            vet.add(paciente.getNome());
            vet.add(paciente.getSexo());
            vet.add(paciente.getMatricula());
            vet.add(paciente.getCargo());
            vet.add(paciente.getDepartamento());
            vet.add(paciente.getEmpresa().getNome().toUpperCase());

            vec.add(vet);
        }

        return vec;
    }

    public void update_table(List<Paciente> pacienteList) {
        table.setModel(new DefaultTableModel(
                pacienteListToVector(pacienteList),
                columns
        ) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        });

        table.getColumnModel().getColumn(0).setMaxWidth(30);
        table.getColumnModel().getColumn(2).setMinWidth(100);
        table.getColumnModel().getColumn(2).setMaxWidth(100);
        table.getColumnModel().getColumn(3).setMinWidth(100);
        table.getColumnModel().getColumn(3).setMaxWidth(100);
        table.getColumnModel().getColumn(4).setMinWidth(150);
        table.getColumnModel().getColumn(4).setMaxWidth(150);
        table.getColumnModel().getColumn(5).setMinWidth(150);
        table.getColumnModel().getColumn(5).setMaxWidth(150);
        table.getColumnModel().getColumn(6).setMinWidth(100);
        table.getColumnModel().getColumn(6).setMaxWidth(100);
        table.doLayout();
    }

    public void filtrar() {
        Session session = null;
        List<Empresa> empresas = getSelectedEmpresas();

        try {
            session = Util.getSessionFactory().openSession();
            Query query;

            if (empresas.size() > 0) {
                query = session.getNamedQuery("Paciente.pesquisaWithEmpresa");
                query.setParameterList("empresas", empresas);
            } else {
                query = session.getNamedQuery("Paciente.pesquisa");
            }

            query.setString("pesquisa", "%" + filtroPesquisa.getText() + "%");

            update_table(query.list());
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    public Paciente getSelectedPaciente(){
        Paciente paciente = null;
        
        try {
            paciente = PacienteService.getInstance().find( this.getSelectedPacienteId() );
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
        
        return paciente;
    }
    
    public int getSelectedPacienteId(){
        return (int) table.getValueAt(table.getSelectedRow(), 0);
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pacienteMenuPopup = new javax.swing.JPopupMenu();
        actionNovoTratamentoMenu = new javax.swing.JMenuItem();
        actionListarTratamentosMenu = new javax.swing.JMenuItem();
        actionNovoTratamentoEmerciancal = new javax.swing.JMenuItem();
        actionListarAtendimentos = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        actionEditarUsuarioMenu = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        actionRemoverUsuario = new javax.swing.JMenuItem();
        actionNovaPaciente = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        filtroPesquisa = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jListEmpresas = new javax.swing.JList();
        jScrollPane2 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();

        actionNovoTratamentoMenu.setText("Novo Tratamento");
        actionNovoTratamentoMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionNovoTratamentoMenuActionPerformed(evt);
            }
        });
        pacienteMenuPopup.add(actionNovoTratamentoMenu);

        actionListarTratamentosMenu.setText("Listar Tratamentos");
        actionListarTratamentosMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionListarTratamentosMenuActionPerformed(evt);
            }
        });
        pacienteMenuPopup.add(actionListarTratamentosMenu);

        actionNovoTratamentoEmerciancal.setText("Nova Emergência");
        actionNovoTratamentoEmerciancal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionNovoTratamentoEmerciancalActionPerformed(evt);
            }
        });
        pacienteMenuPopup.add(actionNovoTratamentoEmerciancal);

        actionListarAtendimentos.setText("Ver Atendimentos");
        actionListarAtendimentos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionListarAtendimentosActionPerformed(evt);
            }
        });
        pacienteMenuPopup.add(actionListarAtendimentos);
        pacienteMenuPopup.add(jSeparator1);

        actionEditarUsuarioMenu.setText("Editar");
        actionEditarUsuarioMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionEditarUsuarioMenuActionPerformed(evt);
            }
        });
        pacienteMenuPopup.add(actionEditarUsuarioMenu);
        pacienteMenuPopup.add(jSeparator2);

        actionRemoverUsuario.setText("Remover Paciente");
        actionRemoverUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionRemoverUsuarioActionPerformed(evt);
            }
        });
        pacienteMenuPopup.add(actionRemoverUsuario);

        actionNovaPaciente.setText("Nova Paciente");
        actionNovaPaciente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionNovaPacienteActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Filtros"));

        jLabel1.setText("Pesquisa:");

        filtroPesquisa.setToolTipText("Este campo permite que você pesquise  por parte do nome ou email ou cpf ou matrícula nas EMPRESAS que selecionar.");
        filtroPesquisa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                filtroPesquisaKeyReleased(evt);
            }
        });

        jListEmpresas.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jListEmpresasValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(jListEmpresas);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(filtroPesquisa)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(filtroPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );

        table.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tableMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tableMouseReleased(evt);
            }
        });
        jScrollPane2.setViewportView(table);
        table.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent me) {
                JTable table =(JTable) me.getSource();
                if (me.getClickCount() == 2) {
                    int id = (int) table.getValueAt(table.getSelectedRow(), 0);

                    try {
                        new PacienteForm(null, true, service.find(id)).setVisible(true);
                        update_table( service.list() );
                    } catch (Exception e){
                        e.printStackTrace();
                        System.exit(0);
                    }
                }
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(actionNovaPaciente)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 956, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(actionNovaPaciente)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 407, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void actionNovaPacienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionNovaPacienteActionPerformed
        try {
            new PacienteForm(null, true).setVisible(true);
            update_table(service.list());
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
    }//GEN-LAST:event_actionNovaPacienteActionPerformed

    private void tableMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableMousePressed

        if (evt.isPopupTrigger()) {
            log.info("Popup is trigger on Pacientes");
            pacienteMenuPopup.show(
                evt.getComponent(), evt.getX(), evt.getY()
            );
        }

        int r = table.rowAtPoint(evt.getPoint());
        if (r >= 0 && r < table.getRowCount()) {
            table.setRowSelectionInterval(r, r);
        } else {
            table.clearSelection();
        }
    }//GEN-LAST:event_tableMousePressed

    private void actionListarTratamentosMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionListarTratamentosMenuActionPerformed
        int id = (int) table.getValueAt(table.getSelectedRow(), 0);

        try {
            Paciente paciente = PacienteService.getInstance().find(id);
            new TratamentosUsuario(null, true, paciente).setVisible(true);
            update_table(service.list());
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
    }//GEN-LAST:event_actionListarTratamentosMenuActionPerformed

    private void actionEditarUsuarioMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionEditarUsuarioMenuActionPerformed
        int id = (int) table.getValueAt(table.getSelectedRow(), 0);

        try {
            Paciente paciente = PacienteService.getInstance().find(id);
            new PacienteForm(null, true, paciente).setVisible(true);
            update_table(service.list());
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
    }//GEN-LAST:event_actionEditarUsuarioMenuActionPerformed

    private void actionNovoTratamentoMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionNovoTratamentoMenuActionPerformed
        int id = (int) table.getValueAt(table.getSelectedRow(), 0);

        try {
            Paciente paciente = PacienteService.getInstance().find(id);
            new TratamentoForm(null, true, paciente).setVisible(true);
            update_table(service.list());
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
    }//GEN-LAST:event_actionNovoTratamentoMenuActionPerformed

    private void actionNovoTratamentoEmerciancalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionNovoTratamentoEmerciancalActionPerformed
        int id = (int) table.getValueAt(table.getSelectedRow(), 0);

        try {
            Paciente paciente = PacienteService.getInstance().find(id);
            
            new TratamentoForm(null, true, Tratamento.buildEmergencia(paciente)).setVisible(true);
            update_table(service.list());
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
    }//GEN-LAST:event_actionNovoTratamentoEmerciancalActionPerformed

    private void actionListarAtendimentosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionListarAtendimentosActionPerformed

        int id = (int) table.getValueAt(table.getSelectedRow(), 0);

        try {
            Paciente paciente = PacienteService.getInstance().find(id);
            new Atendimentos(null, true, paciente).setVisible(true);
            update_table(service.list());
        } catch (Exception e) {
            Util.fatalErrorFlow(log, e);
        }
    }//GEN-LAST:event_actionListarAtendimentosActionPerformed

    private void tableMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableMouseReleased
        tableMousePressed(evt);
    }//GEN-LAST:event_tableMouseReleased

    private void filtroPesquisaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_filtroPesquisaKeyReleased
        filtrar();
    }//GEN-LAST:event_filtroPesquisaKeyReleased

    private void jListEmpresasValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jListEmpresasValueChanged
        if (!evt.getValueIsAdjusting()) {
            filtrar();
        }
    }//GEN-LAST:event_jListEmpresasValueChanged

    private void actionRemoverUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionRemoverUsuarioActionPerformed
        Paciente paciente = getSelectedPaciente();
        
        int dialogResult = JOptionPane.showConfirmDialog(null, "Ao remover um cliente não será possível recuperar seus dados futuramente. Perdendo todos os seus dados.","Warning", JOptionPane.YES_NO_OPTION);
        if(dialogResult == JOptionPane.YES_OPTION){
            if( paciente.destroy() ) {
                JOptionPane.showConfirmDialog(null, "Paciente removido com sucesso!", "Sucesso.", JOptionPane.OK_OPTION);
                filtrar();
            }
        }
    }//GEN-LAST:event_actionRemoverUsuarioActionPerformed

    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
    /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FisioterapeutaForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FisioterapeutaForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FisioterapeutaForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FisioterapeutaForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    javax.swing.JFrame j = new javax.swing.JFrame();
                    j.setExtendedState(java.awt.Frame.MAXIMIZED_BOTH);
                    j.add(new Pacientes());
                    j.setLocationRelativeTo(null);
                    j.setVisible(true);

                } catch (Exception e) {
                    log.error(e.getLocalizedMessage().toString(), e);
                    JDialog ew = new ErrorMessage("O sistema será finalizado devido ao erro", e);
                    ew.setTitle(e.getLocalizedMessage().toString());
                    ew.setVisible(true);
                    System.exit(0);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem actionEditarUsuarioMenu;
    private javax.swing.JMenuItem actionListarAtendimentos;
    private javax.swing.JMenuItem actionListarTratamentosMenu;
    private javax.swing.JButton actionNovaPaciente;
    private javax.swing.JMenuItem actionNovoTratamentoEmerciancal;
    private javax.swing.JMenuItem actionNovoTratamentoMenu;
    private javax.swing.JMenuItem actionRemoverUsuario;
    private javax.swing.JTextField filtroPesquisa;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JList jListEmpresas;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu pacienteMenuPopup;
    private javax.swing.JTable table;
    // End of variables declaration//GEN-END:variables

    private void restart() {
        Session session = null;

        try {
            session = Util.getSessionFactory().openSession();
            update_table((List<Paciente>) session.getNamedQuery(Paciente.FIND_ALL).list());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    private List<Empresa> getSelectedEmpresas() {
        List<Empresa> empresas = new ArrayList<Empresa>();

        for( int index : jListEmpresas.getSelectedIndices() ) {
            empresas.add( (Empresa) jListEmpresas.getModel().getElementAt(index) );
        }

        return empresas;
    }
}
