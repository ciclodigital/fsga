/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.view;

/**
 *
 * @author allan
 */
public final class LoadingSingleton {
    private static final Loading loading = new Loading();
    
    public static void show(){
        loading.setVisible(true);
    }
    
    public static void hide(){
        loading.setVisible(false);
    }
}
