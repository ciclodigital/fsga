/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.view;

import br.com.fisioforma.db.IService;
import br.com.fisioforma.db.Util;
import br.com.fisioforma.model.Recurso;
import br.com.fisioforma.model.service.RecursoService;
import br.com.fisioforma.swing.ErrorMessage;
import br.com.fisioforma.util.AbstractErrorMessage;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Vector;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author allan
 */
public class Recursos extends javax.swing.JPanel {

    private static final Logger log = Logger.getLogger(Recursos.class);
    private Vector<String> columns = createTableColumns();
    private IService<Long, Recurso> service = RecursoService.getInstance();

    /**
     * Creates new form Recursos
     */
    public Recursos() {
        initComponents();
        initFields();

        new AbstractErrorMessage() {

            @Override
            public void execute() throws Exception {
                update_table(service.list());
            }
        };
    }

    private void initFields() {
        table.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent me) {
                JTable table = (JTable) me.getSource();
                if (me.getClickCount() == 2) {
                    int id = (int) table.getValueAt(table.getSelectedRow(), 0);

                    try {
                        new RecursoForm(null, true, service.find(id)).setVisible(true);
                        update_table(service.list());
                    } catch (Exception e) {
                        e.printStackTrace();
                        System.exit(0);
                    }
                }
            }
        });
    }

    private Vector<String> createTableColumns() {
        Vector<String> columns = new Vector<String>();

        columns.add("Id");
        columns.add("Sigla");
        columns.add("Descrição");

        return columns;
    }

    public Vector<Vector<Object>> recursoListToVector(List<Recurso> recursoList) {
        Vector<Vector<Object>> vec = new Vector<Vector<Object>>();

        for (Recurso recurso : recursoList) {
            Vector<Object> vet = new Vector<Object>();

            vet.add(recurso.getId());
            vet.add(recurso.getSigla());
            vet.add(recurso.getDescricao());

            vec.add(vet);
        }

        return vec;
    }

    public void update_table(List<Recurso> recursoList) {
        table.setModel(new DefaultTableModel(
                recursoListToVector(recursoList),
                createTableColumns()
        ) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        });

        table.getColumnModel().getColumn(0).setMaxWidth(30);
        table.getColumnModel().getColumn(1).setMaxWidth(100);
        table.doLayout();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        actionNovaRecurso = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        filtroNome = new javax.swing.JTextField();
        actionFiltrar = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();

        actionNovaRecurso.setText("Nova Recurso");
        actionNovaRecurso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionNovaRecursoActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Filtros"));

        jLabel1.setText("Nome:");

        actionFiltrar.setText("Filtrar");
        actionFiltrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionFiltrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(filtroNome)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(actionFiltrar)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(filtroNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(actionFiltrar))
                .addGap(0, 8, Short.MAX_VALUE))
        );

        jScrollPane2.setViewportView(table);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(actionNovaRecurso)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 552, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(actionNovaRecurso)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void actionNovaRecursoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionNovaRecursoActionPerformed
        try {
            new RecursoForm(null, true).setVisible(true);
            update_table(service.list());
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
    }//GEN-LAST:event_actionNovaRecursoActionPerformed

    private void actionFiltrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionFiltrarActionPerformed
        Session session = null;

        try {
            session = Util.getSessionFactory().openSession();
            Query query = session.getNamedQuery("Recurso.likeBySigla").setString("sigla", "%" + filtroNome.getText() + "%");

            update_table(query.list());
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }//GEN-LAST:event_actionFiltrarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
    /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FisioterapeutaForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FisioterapeutaForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FisioterapeutaForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FisioterapeutaForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    javax.swing.JFrame j = new javax.swing.JFrame();
                    j.setExtendedState(java.awt.Frame.MAXIMIZED_BOTH);
                    j.add(new Recursos());
                    j.setLocationRelativeTo(null);
                    j.setVisible(true);

                } catch (Exception e) {
                    log.error(e.getLocalizedMessage().toString(), e);
                    JDialog ew = new ErrorMessage("O sistema será finalizado devido ao erro", e);
                    ew.setTitle(e.getLocalizedMessage().toString());
                    ew.setVisible(true);
                    System.exit(0);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton actionFiltrar;
    private javax.swing.JButton actionNovaRecurso;
    private javax.swing.JTextField filtroNome;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable table;
    // End of variables declaration//GEN-END:variables
}
