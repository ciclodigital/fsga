/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.view;

import br.com.fisioforma.db.Util;
import br.com.fisioforma.model.Atendimento;
import br.com.fisioforma.model.AtendimentoStatusEnum;
import br.com.fisioforma.model.Paciente;
import br.com.fisioforma.model.Tratamento;
import br.com.fisioforma.model.service.AtendimentoService;
import br.com.fisioforma.model.service.PacienteService;
import br.com.fisioforma.model.service.TratamentoService;
import br.com.fisioforma.swing.ErrorMessage;
import br.com.fisioforma.util.AbstractErrorMessage;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author allan
 */
public class AtendimentosEspecial extends javax.swing.JDialog {
    private Paciente paciente;
    private Tratamento tratamento;
    private List<Atendimento> atendimentos;
    private Atendimento atendimento;

    /**
     * Creates new form Atendimento
     */
    public AtendimentosEspecial(java.awt.Frame parent, boolean modal, Paciente paciente) {
        super(parent, modal);
        this.paciente = paciente;
        initComponents();
        
        relistAtendimentos();
    }
    
    /**
     * Creates new form Atendimento
     */
    public AtendimentosEspecial(java.awt.Frame parent, boolean modal, Tratamento tratamento) {
        super(parent, modal);
        this.tratamento = tratamento;
        initComponents();
        setTitle("Atendimentos do Tratamento (tiss: "+ tratamento.getTiss() +") - Status: "  + tratamento.getStatus());
        setLocationRelativeTo(null);
        
        relistAtendimentos();
    }
    
    /**
     * Creates new form Atendimento
     */
    public AtendimentosEspecial(java.awt.Frame parent, boolean modal, ArrayList<Atendimento> atendimentos ) {
        super(parent, modal);
        initComponents();
        setTitle("Atendimentos Pendentes");
        setLocationRelativeTo(null);
        
        Session session = null;
        
        update_table(atendimentos);
    } 
    
    private Vector<String> createTableColumns() {
        Vector<String> columns = new Vector<String>();
        
        columns.add("Id");
        columns.add("Fisioterapeuta");
        columns.add("Paciente");
        columns.add("Data");
        columns.add("Horário");
        columns.add("Setor");
        columns.add("Status");

        return columns;
    }
    
    public Vector<Vector<Object>> tratamentoListToVector( List<br.com.fisioforma.model.Atendimento> atendimentoList ){
        Vector<Vector<Object>> vec = new Vector<Vector<Object>>();
        
        for (br.com.fisioforma.model.Atendimento atendimento : atendimentoList) {
            Vector<Object> vet = new Vector<Object>();
            
            vet.add( atendimento.getId() );
            vet.add( atendimento.getFisioterapeuta() );
            vet.add( atendimento.getTratamento().getPaciente().getNome() );
            vet.add( atendimento.getData() );
            vet.add( atendimento.getSetorHorario() );
            vet.add( atendimento.getTratamento().getSetor().getNome() );
            vet.add( atendimento.getStatus() );

            vec.add(vet);
        }
        
        return vec;
    }
    
    public void update_table( List<br.com.fisioforma.model.Atendimento> atendimentoList ){
        this.atendimentos = atendimentoList;
        
        table.setModel(new DefaultTableModel(
            tratamentoListToVector( atendimentoList ),
            createTableColumns()
        ) {
            public boolean isCellEditable(int row, int column){  
                return false;  
            }
        });
        
        table.getColumnModel().getColumn(0).setMaxWidth(100);
        table.doLayout();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        menu = new javax.swing.JPopupMenu();
        actionMarcarComoFaltaEReagendar = new javax.swing.JMenuItem();
        actionMarcarComoPresenca = new javax.swing.JMenuItem();
        actionMarcarComoFalta = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        actionVerTratamento = new javax.swing.JMenuItem();
        actionVerPaciente = new javax.swing.JMenuItem();
        actionVerAtendimento = new javax.swing.JMenuItem();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();

        actionMarcarComoFaltaEReagendar.setText("Marcar como Falta e Re-agendar");
        actionMarcarComoFaltaEReagendar.setActionCommand("Marcar como Falta e Re-agendar (justificada)");
        actionMarcarComoFaltaEReagendar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionMarcarComoFaltaEReagendarActionPerformed(evt);
            }
        });
        menu.add(actionMarcarComoFaltaEReagendar);

        actionMarcarComoPresenca.setText("Marcar Como Presente");
        actionMarcarComoPresenca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionMarcarComoPresencaActionPerformed(evt);
            }
        });
        menu.add(actionMarcarComoPresenca);

        actionMarcarComoFalta.setText("Marcar Como Falta");
        actionMarcarComoFalta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionMarcarComoFaltaActionPerformed(evt);
            }
        });
        menu.add(actionMarcarComoFalta);
        menu.add(jSeparator1);

        actionVerTratamento.setText("Ver Tratamento");
        actionVerTratamento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionVerTratamentoActionPerformed(evt);
            }
        });
        menu.add(actionVerTratamento);

        actionVerPaciente.setText("Ver Paciente");
        actionVerPaciente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionVerPacienteActionPerformed(evt);
            }
        });
        menu.add(actionVerPaciente);

        actionVerAtendimento.setText("Ver Atendimento");
        actionVerAtendimento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionVerAtendimentoActionPerformed(evt);
            }
        });
        menu.add(actionVerAtendimento);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4", "Título 5", "Título 6"
            }
        ));
        table.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tableMousePressed(evt);
            }
        });
        jScrollPane1.setViewportView(table);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 812, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 363, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tableMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableMousePressed
        //if (evt.isPopupTrigger()) {
            menu.show(
                evt.getComponent(), evt.getX(), evt.getY()
            );
        //}

        int r = table.rowAtPoint(evt.getPoint());
        if (r >= 0 && r < table.getRowCount()) {
            table.setRowSelectionInterval(r, r);
            atendimento = atendimentos.get(r);
        } else {
            table.clearSelection();
        }
    }//GEN-LAST:event_tableMousePressed

    private void actionMarcarComoFaltaEReagendarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionMarcarComoFaltaEReagendarActionPerformed
        new ReagendarAtendimentoForm(null, true, atendimento).setVisible(true);
    }//GEN-LAST:event_actionMarcarComoFaltaEReagendarActionPerformed

    private void actionMarcarComoPresencaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionMarcarComoPresencaActionPerformed

        Session session = null;

        try {
            session = Util.getSessionFactory().openSession();

            atendimento.compareceu();
            AtendimentoService.getInstance().saveOrUpdate(atendimento);

            if( atendimento.getTratamento().countAtendimentosFeitos() == atendimento.getTratamento().getQuantidade() ) {
                new TratamentoConcluidoForm(null, true, TratamentoService.getInstance().find(atendimento.getTratamento().getId())).setVisible(true);
            }
        } catch (Exception e) {
            JDialog ew = new ErrorMessage("O sistema será finalizado devido ao erro", e);
            ew.setTitle(e.getLocalizedMessage().toString());
            ew.setVisible(true);
            System.exit(0);
        } finally {
            if (session != null) {
                session.close();
            }
        }

    }//GEN-LAST:event_actionMarcarComoPresencaActionPerformed

    private void actionMarcarComoFaltaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionMarcarComoFaltaActionPerformed
        new AbstractErrorMessage() {

            @Override
            public void execute() throws Exception {
                atendimento.faltou();

                AtendimentoService.getInstance().saveOrUpdate(atendimento);
            }
        };
    }//GEN-LAST:event_actionMarcarComoFaltaActionPerformed

    private void actionVerTratamentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionVerTratamentoActionPerformed
        new TratamentoForm(null, true, atendimento.getTratamento() ).setVisible(true);
    }//GEN-LAST:event_actionVerTratamentoActionPerformed

    private void actionVerPacienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionVerPacienteActionPerformed
        new PacienteForm(null, true, atendimento.getTratamento().getPaciente()).setVisible(true);
    }//GEN-LAST:event_actionVerPacienteActionPerformed

    private void actionVerAtendimentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionVerAtendimentoActionPerformed
        new AtendimentoForm(null, true, atendimento).setVisible(true);
    }//GEN-LAST:event_actionVerAtendimentoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AtendimentosEspecial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AtendimentosEspecial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AtendimentosEspecial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AtendimentosEspecial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    AtendimentosEspecial dialog = new AtendimentosEspecial(new javax.swing.JFrame(), true, PacienteService.getInstance().find(1) );
                    dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                        @Override
                        public void windowClosing(java.awt.event.WindowEvent e) {
                            System.exit(0);
                        }
                    });
                dialog.setVisible(true);
                } catch (Exception e) {}
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem actionMarcarComoFalta;
    private javax.swing.JMenuItem actionMarcarComoFaltaEReagendar;
    private javax.swing.JMenuItem actionMarcarComoPresenca;
    private javax.swing.JMenuItem actionVerAtendimento;
    private javax.swing.JMenuItem actionVerPaciente;
    private javax.swing.JMenuItem actionVerTratamento;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu menu;
    private javax.swing.JTable table;
    // End of variables declaration//GEN-END:variables

    private Atendimento getSelectedAtendimento() {       
        return atendimentos.get(table.getSelectedRow());
    }

    private void relistAtendimentos() {
        Session session = null;
        
        try {
            session = Util.getSessionFactory().openSession();
            Query query = null;
            
            if( this.paciente == null ) {
                query = session.getNamedQuery(br.com.fisioforma.model.Atendimento.FIND_BY_TRATAMENTO);
                query.setParameter("tratamento", tratamento);
            } else {
                query = session.getNamedQuery(Atendimento.FIND_BY_PACIENTE);
                query.setParameter("paciente", paciente);
            }

            update_table( (List<Atendimento>) query.list() );
        } catch (Exception e) {
            new ErrorMessage(e).setVisible(true);
            System.exit(0);
        } finally {
            if( session != null ) session.close();
        }
    }
}
