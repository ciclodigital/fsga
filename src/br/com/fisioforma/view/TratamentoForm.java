/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.view;

import br.com.fisioforma.db.Util;
import br.com.fisioforma.model.Atendimento;
import br.com.fisioforma.model.AtendimentoStatusEnum;
import br.com.fisioforma.model.Paciente;
import br.com.fisioforma.model.Regiao;
import br.com.fisioforma.model.Recurso;
import br.com.fisioforma.model.Procedimento;
import br.com.fisioforma.model.Setor;
import br.com.fisioforma.model.Tratamento;
import br.com.fisioforma.model.TratamentoStatusEnum;
import br.com.fisioforma.model.TratamentoTipoEnum;
import br.com.fisioforma.model.service.PacienteService;
import br.com.fisioforma.model.service.SetorService;
import br.com.fisioforma.model.service.TratamentoService;
import br.com.fisioforma.swing.ErrorMessage;
import br.com.fisioforma.swing.ListModel;
import br.com.fisioforma.util.AbstractErrorMessage;
import br.com.fisioforma.util.StringUtil;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.validation.ConstraintViolation;
import javax.validation.ValidationException;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.jdesktop.swingx.combobox.ListComboBoxModel;

/**
 *
 * @author allan
 */
public class TratamentoForm extends javax.swing.JDialog {

    private static final Logger log = Logger.getLogger(TratamentoForm.class);
    private Tratamento tratamento = null;
    private List<Regiao> regioesList = new ArrayList<Regiao>();
    private List<Recurso> recursosList = new ArrayList<Recurso>();
    private List<Procedimento> procedimentosList = new ArrayList<Procedimento>();
    private List<Setor> setores;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    public TratamentoForm(javax.swing.JFrame parent, boolean modal, Paciente paciente) {
        super(parent, modal);
        this.tratamento = new Tratamento();
        this.tratamento.setPaciente(paciente);
        initComponents();
        setLocationRelativeTo(null);
        initFields();
        setTitle("Novo Tratamento - " + tratamento.getPaciente().getNome());
        initUI();
    }

    public TratamentoForm(javax.swing.JFrame parent, boolean modal, Tratamento tratamento) {
        super(parent, modal);
        this.tratamento = tratamento;
        initComponents();
        setLocationRelativeTo(null);
        initFields();
        setTitle("Editar Tratamento - " + tratamento.getPaciente().getNome());
        initUI();
    }

    public Tratamento getTratamento() {
        return tratamento;
    }

    private void initFields() {
        
        if( tratamento.getStatus() == TratamentoStatusEnum.CANCELADO ) {
            actionCancelarTratamento.setText("Descancelar");
        }

        updatePeriodo();

        try {
            setores = SetorService.getInstance().list();

            if (setores.size() == 0) {
                JOptionPane.showMessageDialog(this, "Para cadastrar um tratamento. É obrigatório a criação de ao menos um setor.");
                log.warn("Tentativa de criação de tratamento sem a existência de ao menos um setor");
                dispose();
            }
        } catch (Exception e) {
            log.error(e.getLocalizedMessage().toString(), e);
            JDialog ew = new ErrorMessage("O sistema será finalizado devido ao erro", e);
            ew.setTitle(e.getLocalizedMessage().toString());
            ew.setVisible(true);
            System.exit(0);
        }

        nomeDoPaciente.setText(tratamento.getPaciente().getNome().toUpperCase());
        regiaoList.setModel(new ListModel<Regiao>(regioesDisponiveis()));
        recursoList.setModel(new ListModel<Recurso>(recursosDisponiveis()));
        procedimentoList.setModel(new ListModel<Procedimento>(procedimentosDisponiveis()));
        tratamentoRegiaoList.setModel(new ListModel<Regiao>(tratamento.getRegioes()));
        tratamentoRecursoList.setModel(new ListModel<Recurso>(tratamento.getRecursos()));
        tratamentoProcedimentoList.setModel(new ListModel<Procedimento>(tratamento.getProcedimentos()));
        tiss.setText(tratamento.getTiss());
        medico.setText(tratamento.getNomeMedico());
        diagnostico.setText(tratamento.getDiagnostico());
        observacao.setText(tratamento.getObservacao());
        sessoes.setSelectedItem(tratamento.getQuantidade() + "");
        setor.setModel(
                new ListComboBoxModel(setores)
        );

        if (tratamento.getSetor() == null) {
            setor.setSelectedIndex(0);
        } else {
            setor.setSelectedItem(tratamento.getSetor());
        }

        createAddListSelectionListener(regiaoList, actionAddRegioes);
        createAddListSelectionListener(tratamentoRegiaoList, actionRemoveRegioes);
        createAddListSelectionListener(recursoList, actionAddRecursos);
        createAddListSelectionListener(tratamentoRecursoList, actionRemoveRecursos);
        createAddListSelectionProcedimentosListener(procedimentoList, actionAddProcedimentos, tratamento.getPaciente().getEmpresa().getLimiteProcedimentos());
        createAddListSelectionListener(tratamentoProcedimentoList, actionRemoveProcedimentos);
    }

    private List<Regiao> regioesDisponiveis() {
        Session session = null;
        try {
            session = Util.getSessionFactory().openSession();
            Query query;

            if (tratamento.getRegioes() != null && tratamento.getRegioes().size() > 0) {
                query = session.getNamedQuery("Regiao.notIn");
                query.setParameterList("regioes", tratamento.getRegioesIds());
            } else {
                query = session.getNamedQuery("Regiao.findAll");
            }

            regioesList = query.list();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return regioesList;
    }

    private List<Recurso> recursosDisponiveis() {
        new AbstractErrorMessage() {

            @Override
            public void execute() throws Exception {
                Session session = null;

                try {
                    session = Util.getSessionFactory().openSession();
                    Query query;

                    if (tratamento.getRecursos() != null && tratamento.getRecursos().size() > 0) {
                        query = session.getNamedQuery("Recurso.notIn");
                        query.setParameterList("recursos", tratamento.getRecursosIds());
                    } else {
                        query = session.getNamedQuery("Recurso.findAll");
                    }

                    recursosList = query.list();
                } catch (Exception e) {
                    throw new Exception(e);
                } finally {
                    if (session != null) {
                        session.close();
                    }
                }
            }
        };

        return recursosList;
    }

    private List<Procedimento> procedimentosDisponiveis() {
        new AbstractErrorMessage() {

            @Override
            public void execute() throws Exception {
                Session session = null;

                try {
                    session = Util.getSessionFactory().openSession();
                    Query query;

                    if (tratamento.getProcedimentos() != null && tratamento.getProcedimentos().size() > 0) {
                        query = session.getNamedQuery("Procedimento.notIn");
                        query.setParameterList("procedimentos", tratamento.getProcedimentosIds());
                        query.setParameter("empresa", tratamento.getPaciente().getEmpresa());
                    } else {
                        query = session.getNamedQuery("Procedimento.findByEmpresa");
                        query.setParameter("empresa", tratamento.getPaciente().getEmpresa());
                    }

                    procedimentosList = query.list();
                } catch (Exception e) {
                    throw new Exception(e);
                } finally {
                    if (session != null) {
                        session.close();
                    }
                }
            }
        };

        return procedimentosList;
    }

    public boolean salvar() {
        try {
            tratamento.setTiss(tiss.getText());
            tratamento.setDiagnostico(diagnostico.getText());
            tratamento.setNomeMedico(medico.getText());
            tratamento.setObservacao(observacao.getText());
            tratamento.setSetor((Setor) setor.getSelectedItem());
            tratamento.setQuantidade(
                    StringUtil.stringToInteger(
                            sessoes.getSelectedItem().toString()
                    )
            );
            tratamento.setInicio(dateFormat.parse(de.getText()));
            tratamento.setFim(dateFormat.parse(ate.getText()));

            Set<ConstraintViolation<Tratamento>> valdiations = TratamentoService.getInstance().validate(tratamento);

            if (valdiations.size() > 0) {
                throw new ValidationException(TratamentoService.getInstance().getFormatedValidationMessages(valdiations));
            }

            if (TratamentoService.getInstance().saveOrUpdate(tratamento) != null) {
                dispose();
                JOptionPane.showMessageDialog(this, "Tratamento salvo com sucesso!");
            }
        } catch (ValidationException e) {
            JOptionPane.showMessageDialog(this, e.getMessage());
            return false;
        } catch (Exception e) {
            new ErrorMessage(e).setVisible(true);
            return false;
        }

        return true;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        actionSalvarFechar = new javax.swing.JButton();
        actionCancelar = new javax.swing.JButton();
        salvarECriarTratamento = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        nomeDoPaciente = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        tiss = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        sessoes = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        setor = new javax.swing.JComboBox();
        jLabel5 = new javax.swing.JLabel();
        medico = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        diagnostico = new javax.swing.JTextArea();
        jPanel4 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        observacao = new javax.swing.JTextArea();
        jLabel10 = new javax.swing.JLabel();
        de = new javax.swing.JFormattedTextField();
        jLabel11 = new javax.swing.JLabel();
        ate = new javax.swing.JFormattedTextField();
        jPanel5 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        actionRemoveRegioes = new javax.swing.JButton();
        actionAddRegioes = new javax.swing.JButton();
        jScrollPane9 = new javax.swing.JScrollPane();
        regiaoList = new javax.swing.JList();
        jScrollPane10 = new javax.swing.JScrollPane();
        tratamentoRegiaoList = new javax.swing.JList();
        jPanel10 = new javax.swing.JPanel();
        actionRemoveRecursos = new javax.swing.JButton();
        actionAddRecursos = new javax.swing.JButton();
        jScrollPane11 = new javax.swing.JScrollPane();
        recursoList = new javax.swing.JList();
        jScrollPane12 = new javax.swing.JScrollPane();
        tratamentoRecursoList = new javax.swing.JList();
        jPanel11 = new javax.swing.JPanel();
        actionRemoveProcedimentos = new javax.swing.JButton();
        actionAddProcedimentos = new javax.swing.JButton();
        jScrollPane15 = new javax.swing.JScrollPane();
        procedimentoList = new javax.swing.JList();
        jScrollPane16 = new javax.swing.JScrollPane();
        tratamentoProcedimentoList = new javax.swing.JList();
        actionCancelarTratamento = new javax.swing.JButton();
        tratamentoStatus1 = new br.com.fisioforma.swing.TratamentoStatus();
        tratamentoTipo1 = new br.com.fisioforma.swing.TratamentoTipo();
        actionAtendimentos = new javax.swing.JButton();
        labelFisioterapeuta = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        labelEmpresa = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        actionVerResumo = new javax.swing.JButton();
        actionDesistir = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Nova Tratamento");
        setResizable(false);

        actionSalvarFechar.setText("Salvar e Fechar");
        actionSalvarFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionSalvarFecharActionPerformed(evt);
            }
        });

        actionCancelar.setText("Cancelar");
        actionCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionCancelarActionPerformed(evt);
            }
        });

        salvarECriarTratamento.setText("Salvar e Criar Atendimento");
        salvarECriarTratamento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salvarECriarTratamentoActionPerformed(evt);
            }
        });

        jLabel3.setText("Paciente:");

        nomeDoPaciente.setText("-");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Informações da Guia"));

        jLabel1.setText("Número da TISS*:");

        jLabel2.setText("Numero de Sessões*:");

        sessoes.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "5", "10", "15", "20" }));
        sessoes.setSelectedItem("10");

        jLabel4.setText("Setor de Tratamento*:");

        jLabel5.setText("Médico*:");

        jPanel2.setLayout(new java.awt.GridLayout(1, 0));

        jLabel6.setText("Indicação Clínica (Diagnóstico)*:");

        diagnostico.setColumns(20);
        diagnostico.setLineWrap(true);
        diagnostico.setRows(5);
        diagnostico.setMargin(new java.awt.Insets(10, 10, 10, 10));
        jScrollPane1.setViewportView(diagnostico);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 557, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.add(jPanel3);

        jLabel7.setText("Observações:");

        observacao.setColumns(20);
        observacao.setLineWrap(true);
        observacao.setRows(5);
        observacao.setMargin(new java.awt.Insets(10, 10, 10, 10));
        jScrollPane2.setViewportView(observacao);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 557, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel2.add(jPanel4);

        jLabel10.setText(" Período de Atendimento:* de");

        try {
            de.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel11.setText("até");

        try {
            ate.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        ate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ateActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tiss, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(medico, javax.swing.GroupLayout.PREFERRED_SIZE, 284, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sessoes, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(setor, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(de, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ate, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(tiss, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(sessoes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(setor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(medico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(de, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11)
                    .addComponent(ate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel5.setLayout(new java.awt.GridLayout(1, 0));

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder("Regioes*"));

        actionRemoveRegioes.setText("<<");
        actionRemoveRegioes.setEnabled(false);
        actionRemoveRegioes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionRemoveRegioesActionPerformed(evt);
            }
        });

        actionAddRegioes.setText(">>");
        actionAddRegioes.setEnabled(false);
        actionAddRegioes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionAddRegioesActionPerformed(evt);
            }
        });

        jScrollPane9.setViewportView(regiaoList);

        jScrollPane10.setViewportView(tratamentoRegiaoList);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane9, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(actionRemoveRegioes)
                    .addComponent(actionAddRegioes))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane10, javax.swing.GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane9, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane10, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(55, 55, 55)
                        .addComponent(actionAddRegioes)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(actionRemoveRegioes)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel5.add(jPanel6);

        jPanel10.setBorder(javax.swing.BorderFactory.createTitledBorder("Recursos*"));

        actionRemoveRecursos.setText("<<");
        actionRemoveRecursos.setEnabled(false);
        actionRemoveRecursos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionRemoveRecursosActionPerformed(evt);
            }
        });

        actionAddRecursos.setText(">>");
        actionAddRecursos.setEnabled(false);
        actionAddRecursos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionAddRecursosActionPerformed(evt);
            }
        });

        jScrollPane11.setViewportView(recursoList);

        jScrollPane12.setViewportView(tratamentoRecursoList);

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane11, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(actionRemoveRecursos)
                    .addComponent(actionAddRecursos))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane12, javax.swing.GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane11, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane12, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGap(55, 55, 55)
                        .addComponent(actionAddRecursos)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(actionRemoveRecursos)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel5.add(jPanel10);

        jPanel11.setBorder(javax.swing.BorderFactory.createTitledBorder("Patologias*"));
        jPanel11.setToolTipText("Só exibirá os procedimentos relativos a EMPRESA associada ao Paciente e ao Procedimento."); // NOI18N

        actionRemoveProcedimentos.setText("<<");
        actionRemoveProcedimentos.setEnabled(false);
        actionRemoveProcedimentos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionRemoveProcedimentosActionPerformed(evt);
            }
        });

        actionAddProcedimentos.setText(">>");
        actionAddProcedimentos.setEnabled(false);
        actionAddProcedimentos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionAddProcedimentosActionPerformed(evt);
            }
        });

        procedimentoList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane15.setViewportView(procedimentoList);

        jScrollPane16.setViewportView(tratamentoProcedimentoList);

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane15, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(actionRemoveProcedimentos)
                    .addComponent(actionAddProcedimentos))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane16, javax.swing.GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane15, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane16, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGap(55, 55, 55)
                        .addComponent(actionAddProcedimentos)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(actionRemoveProcedimentos)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel5.add(jPanel11);

        actionCancelarTratamento.setText("Cancelar");
        actionCancelarTratamento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionCancelarTratamentoActionPerformed(evt);
            }
        });
        // Se o tratamento ainda nao tiver sido persistido
        // o botao deve ser desabilitado
        actionCancelarTratamento.setEnabled(
            tratamento.getId() != null
        );

        tratamentoStatus1.setTratamento(tratamento);

        tratamentoTipo1.setTratamento(tratamento);

        actionAtendimentos.setText("Atendimentos");
        actionAtendimentos.setEnabled(tratamento.getAtendimentos().size() > 0 );
        actionAtendimentos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionAtendimentosActionPerformed(evt);
            }
        });

        labelFisioterapeuta.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        labelFisioterapeuta.setText("-");

        jLabel9.setText("Fisioterapeuta Responsável:");

        labelEmpresa.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        labelEmpresa.setText("-");

        jLabel8.setText("Empresa:");

        actionVerResumo.setText("Ver Resumo");
        actionVerResumo.setEnabled(false);
        actionVerResumo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionVerResumoActionPerformed(evt);
            }
        });

        actionDesistir.setText("Desistir");
        actionDesistir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionDesistirActionPerformed(evt);
            }
        });

        jButton1.setText("Remover");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(actionCancelarTratamento)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(actionDesistir)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(actionAtendimentos)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(actionCancelar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(actionSalvarFechar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(salvarECriarTratamento))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(nomeDoPaciente)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(actionVerResumo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelEmpresa)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(labelFisioterapeuta)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tratamentoTipo1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tratamentoStatus1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel3)
                    .addComponent(nomeDoPaciente)
                    .addComponent(tratamentoStatus1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tratamentoTipo1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelFisioterapeuta)
                    .addComponent(jLabel9)
                    .addComponent(labelEmpresa)
                    .addComponent(jLabel8)
                    .addComponent(actionVerResumo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(actionAtendimentos)
                        .addComponent(jButton1))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(actionSalvarFechar)
                        .addComponent(actionCancelar)
                        .addComponent(salvarECriarTratamento)
                        .addComponent(actionCancelarTratamento)
                        .addComponent(actionDesistir)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void actionCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionCancelarActionPerformed
        dispose();
    }//GEN-LAST:event_actionCancelarActionPerformed

    private void actionSalvarFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionSalvarFecharActionPerformed
        if (salvar()) {
            dispose();
        }
    }//GEN-LAST:event_actionSalvarFecharActionPerformed

    private void actionAddRegioesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionAddRegioesActionPerformed
        javax.swing.ListModel model = regiaoList.getModel();

        // Get all the selected items using the indices
        for (int index : regiaoList.getSelectedIndices()) {
            tratamento.getRegioes().add(
                    (Regiao) model.getElementAt(index)
            );
        }

        regiaoList.setModel(new ListModel<Regiao>(regioesDisponiveis()));
        tratamentoRegiaoList.setModel(new ListModel<Regiao>(tratamento.getRegioes()));
        actionAddRegioes.setEnabled(false);
    }//GEN-LAST:event_actionAddRegioesActionPerformed

    private void actionRemoveRegioesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionRemoveRegioesActionPerformed
        javax.swing.ListModel model = tratamentoRegiaoList.getModel();

        // Get all the selected items using the indices
        for (int index : tratamentoRegiaoList.getSelectedIndices()) {
            tratamento.getRegioes().remove(
                    (Regiao) model.getElementAt(index)
            );
        }

        regiaoList.setModel(new ListModel<Regiao>(regioesDisponiveis()));
        tratamentoRegiaoList.setModel(new ListModel<Regiao>(tratamento.getRegioes()));
        actionRemoveRegioes.setEnabled(false);
    }//GEN-LAST:event_actionRemoveRegioesActionPerformed

    private void actionAddRecursosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionAddRecursosActionPerformed
        javax.swing.ListModel model = recursoList.getModel();

        // Get all the selected items using the indices
        for (int index : recursoList.getSelectedIndices()) {
            tratamento.getRecursos().add(
                    (Recurso) model.getElementAt(index)
            );
        }

        recursoList.setModel(new ListModel<Recurso>(recursosDisponiveis()));
        tratamentoRecursoList.setModel(new ListModel<Recurso>(tratamento.getRecursos()));
        actionAddRecursos.setEnabled(false);
    }//GEN-LAST:event_actionAddRecursosActionPerformed

    private void actionRemoveRecursosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionRemoveRecursosActionPerformed
        javax.swing.ListModel model = tratamentoRecursoList.getModel();

        // Get all the selected items using the indices
        for (int index : tratamentoRecursoList.getSelectedIndices()) {
            tratamento.getRecursos().remove(
                    (Recurso) model.getElementAt(index)
            );
        }

        recursoList.setModel(new ListModel<Recurso>(recursosDisponiveis()));
        tratamentoRecursoList.setModel(new ListModel<Recurso>(tratamento.getRecursos()));
        actionRemoveRecursos.setEnabled(false);
    }//GEN-LAST:event_actionRemoveRecursosActionPerformed

    private void actionAddProcedimentosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionAddProcedimentosActionPerformed
        javax.swing.ListModel model = procedimentoList.getModel();

        // Get all the selected items using the indices
        for (int index : procedimentoList.getSelectedIndices()) {
            tratamento.getProcedimentos().add(
                    (Procedimento) model.getElementAt(index)
            );
        }

        procedimentoList.setModel(new ListModel<Procedimento>(procedimentosDisponiveis()));
        tratamentoProcedimentoList.setModel(new ListModel<Procedimento>(tratamento.getProcedimentos()));
        actionAddProcedimentos.setEnabled(false);
    }//GEN-LAST:event_actionAddProcedimentosActionPerformed

    private void actionRemoveProcedimentosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionRemoveProcedimentosActionPerformed
        javax.swing.ListModel model = tratamentoProcedimentoList.getModel();

        // Get all the selected items using the indices
        for (int index : tratamentoProcedimentoList.getSelectedIndices()) {
            tratamento.getProcedimentos().remove(
                    (Procedimento) model.getElementAt(index)
            );
        }

        procedimentoList.setModel(new ListModel<Procedimento>(procedimentosDisponiveis()));
        tratamentoProcedimentoList.setModel(new ListModel<Procedimento>(tratamento.getProcedimentos()));
        actionRemoveProcedimentos.setEnabled(false);
    }//GEN-LAST:event_actionRemoveProcedimentosActionPerformed

    private void actionCancelarTratamentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionCancelarTratamentoActionPerformed
        if( tratamento.getStatus() == TratamentoStatusEnum.CANCELADO ) {
            tratamento.setStatus(TratamentoStatusEnum.ANDAMENTO);
            salvar();
            dispose();
            new TratamentoForm(null, true, tratamento).setVisible(true);
        } else {
            new CancelarTratamentoForm(null, true, tratamento, this).setVisible(true);
        }
    }//GEN-LAST:event_actionCancelarTratamentoActionPerformed

    private void salvarECriarTratamentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salvarECriarTratamentoActionPerformed
        try {
            if (salvar()) {
                dispose();
                new AtendimentosForm(null, true, tratamento).setVisible(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
    }//GEN-LAST:event_salvarECriarTratamentoActionPerformed

    private void actionAtendimentosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionAtendimentosActionPerformed
        new Atendimentos(null, true, tratamento).setVisible(true);
    }//GEN-LAST:event_actionAtendimentosActionPerformed

    private void actionVerResumoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionVerResumoActionPerformed
        new TratamentoResumo(null, true, tratamento).setVisible(true);
    }//GEN-LAST:event_actionVerResumoActionPerformed

    private void actionDesistirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionDesistirActionPerformed
        int confirm = JOptionPane.showConfirmDialog(null, "Realmente deseja confirmar a desistência deste tratamento?", "Warning", JOptionPane.OK_CANCEL_OPTION);

        if (confirm == JOptionPane.YES_OPTION) {
            tratamento.setStatus(TratamentoStatusEnum.DESISTENCIA);

            Session session = null;

            try {
                session = Util.getSessionFactory().openSession();

                session.update("Tratamento", tratamento);
                
                for( Atendimento atendimeno : tratamento.getAtendimentos() ) {
                    if( atendimeno.getStatus() == AtendimentoStatusEnum.AGENDADO ) {
                        atendimeno.destroy();
                    }
                }

                initFields();
                initUI();
            } catch (Exception e) {
                e.printStackTrace();
                System.exit(0);
            } finally {
                if (session != null) {
                    session.close();
                }
            }
            
            
        }
    }//GEN-LAST:event_actionDesistirActionPerformed

    private void ateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ateActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ateActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        int dialogResult = JOptionPane.showConfirmDialog(null, "Deseja remover este tratamento ("+ tratamento.getTiss()+")?","Warning", JOptionPane.YES_NO_OPTION);
        if(dialogResult == JOptionPane.YES_OPTION){
            
            if( TratamentoService.getInstance().delete(tratamento) ) {
                dispose();
                JOptionPane.showMessageDialog( this , "Tratamento removido com sucesso");
            } else {
                JOptionPane.showMessageDialog( this , "Tivemos algum problema ao tentar remover este tratamento, entre em contato com o Administrador.");
            }
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new TratamentoForm(null, false, PacienteService.getInstance().find(1)).setVisible(true);
                } catch (Exception e) {
                    log.error(e.getLocalizedMessage().toString(), e);
                    JDialog ew = new ErrorMessage("O sistema será finalizado devido ao erro", e);
                    ew.setTitle(e.getLocalizedMessage().toString());
                    ew.setVisible(true);
                    System.exit(0);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton actionAddProcedimentos;
    private javax.swing.JButton actionAddRecursos;
    private javax.swing.JButton actionAddRegioes;
    private javax.swing.JButton actionAtendimentos;
    private javax.swing.JButton actionCancelar;
    private javax.swing.JButton actionCancelarTratamento;
    private javax.swing.JButton actionDesistir;
    private javax.swing.JButton actionRemoveProcedimentos;
    private javax.swing.JButton actionRemoveRecursos;
    private javax.swing.JButton actionRemoveRegioes;
    private javax.swing.JButton actionSalvarFechar;
    private javax.swing.JButton actionVerResumo;
    private javax.swing.JFormattedTextField ate;
    private javax.swing.JFormattedTextField de;
    private javax.swing.JTextArea diagnostico;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane12;
    private javax.swing.JScrollPane jScrollPane15;
    private javax.swing.JScrollPane jScrollPane16;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JLabel labelEmpresa;
    private javax.swing.JLabel labelFisioterapeuta;
    private javax.swing.JTextField medico;
    private javax.swing.JLabel nomeDoPaciente;
    private javax.swing.JTextArea observacao;
    private javax.swing.JList procedimentoList;
    private javax.swing.JList recursoList;
    private javax.swing.JList regiaoList;
    private javax.swing.JButton salvarECriarTratamento;
    private javax.swing.JComboBox sessoes;
    private javax.swing.JComboBox setor;
    private javax.swing.JTextField tiss;
    private javax.swing.JList tratamentoProcedimentoList;
    private javax.swing.JList tratamentoRecursoList;
    private javax.swing.JList tratamentoRegiaoList;
    private br.com.fisioforma.swing.TratamentoStatus tratamentoStatus1;
    private br.com.fisioforma.swing.TratamentoTipo tratamentoTipo1;
    // End of variables declaration//GEN-END:variables

    private void initUI() {
        labelEmpresa.setText(tratamento.getPaciente().getEmpresa().getNome().toUpperCase());

        // Se o tratamento ja exitir que algum atendimento for criado
        // já existirá um fisioterapeuta responsável.
        if (tratamento.getFisioterapeuta() != null) {
            labelFisioterapeuta.setText(tratamento.getFisioterapeuta().getNome());
        }

        // se o tratamento tiver algum atendimento cadastrado,
        // não será mais possível altear o setor
        if (tratamento.getAtendimentos().size() > 0) {
            setor.setEnabled(false);
            de.setEnabled(false);
            ate.setEnabled(false);
        }

        if (tratamento.getId() != null) {
            actionVerResumo.setEnabled(true);
        }

        if (tratamento.getStatus() != TratamentoStatusEnum.ANDAMENTO) {
            nomeDoPaciente.setEnabled(false);
            regiaoList.setEnabled(false);
            recursoList.setEnabled(false);
            procedimentoList.setEnabled(false);
            tratamentoRegiaoList.setEnabled(false);
            tratamentoRecursoList.setEnabled(false);
            tratamentoProcedimentoList.setEnabled(false);
            tiss.setEnabled(false);
            medico.setEnabled(false);
            diagnostico.setEnabled(false);
            observacao.setEnabled(false);
            sessoes.setEnabled(false);
            setor.setEnabled(false);
            actionAddProcedimentos.setEnabled(false);
            actionAddRecursos.setEnabled(false);
            actionAddRegioes.setEnabled(false);
            actionCancelarTratamento.setEnabled(false);
            actionRemoveProcedimentos.setEnabled(false);
            actionRemoveRecursos.setEnabled(false);
            actionRemoveRegioes.setEnabled(false);
            actionSalvarFechar.setEnabled(false);
            actionDesistir.setEnabled(false);
            salvarECriarTratamento.setEnabled(false);
        }

        if (tratamento.getTipo() == TratamentoTipoEnum.EMERGENCIAL) {
            sessoes.setModel(new javax.swing.DefaultComboBoxModel(new String[]{"1"}));
            sessoes.setEnabled(false);
            actionCancelarTratamento.setEnabled(false);
            salvarECriarTratamento.setEnabled(true);
            tiss.setEnabled(false);
            
        }
        
        if (tratamento.getStatus() == TratamentoStatusEnum.CANCELADO) {
            actionCancelarTratamento.setEnabled(true);
        }
    }

    private void createAddListSelectionListener(
            javax.swing.JList list,
            javax.swing.JButton action
    ) {
        list.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent evt) {
                if (!evt.getValueIsAdjusting()) {
                    int[] selectedIx = list.getSelectedIndices();

                    if (selectedIx.length > 0) {
                        action.setEnabled(true);
                    }
                }
            }
        });
    }

    private void updatePeriodo() {
        de.setText(dateFormat.format(tratamento.getInicio()));
        ate.setText(dateFormat.format(tratamento.getFim()));
    }

    private void createAddListSelectionProcedimentosListener(JList procedimentoList, JButton actionAddProcedimentos, int limiteProcedimentos) {
        procedimentoList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent evt) {
                if (!evt.getValueIsAdjusting()) {
                    int[] selectedIx = procedimentoList.getSelectedIndices();

                    if (selectedIx.length > 0 && tratamentoProcedimentoList.getModel().getSize() < limiteProcedimentos) {
                        actionAddProcedimentos.setEnabled(true);
                    }
                }
            }
        });
    }
}
