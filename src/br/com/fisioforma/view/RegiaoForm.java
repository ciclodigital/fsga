/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.view;

import br.com.fisioforma.model.Regiao;
import br.com.fisioforma.model.service.RegiaoService;
import java.text.ParseException;
import java.util.Set;
import javax.swing.JOptionPane;
import javax.validation.ConstraintViolation;
import javax.validation.ValidationException;

/**
 *
 * @author allan
 */
public class RegiaoForm extends javax.swing.JDialog {
    private Regiao regiao = null;
    
    /**
     * Creates new form RegiaoForm
     */
    public RegiaoForm(javax.swing.JFrame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setLocationRelativeTo(null);
        this.regiao = new Regiao();
    }
    
    public RegiaoForm(javax.swing.JFrame parent, boolean modal, Regiao regiao){
        super(parent, modal);
        initComponents();
        setLocationRelativeTo(null);
        setTitle("Editar Região - " + regiao.getNome());
        this.regiao = regiao;
        nome.setText(regiao.getNome());
        descricao.setText(regiao.getDescricao());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        nome = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        descricao = new javax.swing.JTextArea();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        actionSalvarFechar = new javax.swing.JButton();
        actionCancelar = new javax.swing.JButton();
        actionRemover = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Nova Região");

        descricao.setColumns(20);
        descricao.setLineWrap(true);
        descricao.setRows(5);
        jScrollPane1.setViewportView(descricao);

        jLabel1.setText("Nome*:");

        jLabel2.setText("Descrição:");

        actionSalvarFechar.setText("Salvar e Fechar");
        actionSalvarFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionSalvarFecharActionPerformed(evt);
            }
        });

        actionCancelar.setText("Cancelar");
        actionCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionCancelarActionPerformed(evt);
            }
        });

        actionRemover.setText("Remover");
        actionRemover.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionRemoverActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 21, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 270, Short.MAX_VALUE)
                            .addComponent(nome)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(actionRemover)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(actionCancelar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(actionSalvarFechar)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(actionSalvarFechar)
                    .addComponent(actionCancelar)
                    .addComponent(actionRemover))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void actionCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionCancelarActionPerformed
        dispose();
    }//GEN-LAST:event_actionCancelarActionPerformed

    private void actionSalvarFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionSalvarFecharActionPerformed
        regiao.setNome( nome.getText() );
        regiao.setDescricao( descricao.getText() );
        
        try {
            RegiaoService rs = new RegiaoService();
            
            Set<ConstraintViolation<Regiao>> valdiations = rs.validate(regiao);
            
            if ( valdiations.size() > 0 ) {                
                throw new ValidationException( rs.getFormatedValidationMessages(valdiations) );
            }
            
            if ( rs.saveOrUpdate(regiao) != null ) {
                dispose();
                JOptionPane.showMessageDialog( this , "Região salvo com sucesso!");
            }
            
        } catch (Exception e ) {
            JOptionPane.showMessageDialog(this, e.getLocalizedMessage());
        }
    }//GEN-LAST:event_actionSalvarFecharActionPerformed

    private void actionRemoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionRemoverActionPerformed
        if( regiao.anyTratamento() ) {
            JOptionPane.showMessageDialog( this , "Este região nao pode ser removido. Pois possui tratamentos atrelados à ele.");
        } else {
            int dialogResult = JOptionPane.showConfirmDialog(null, "Deseja remover esta região ("+ regiao.getNome() +")?","Warning", JOptionPane.YES_NO_OPTION);
            if(dialogResult == JOptionPane.YES_OPTION){
                if( regiao.destroy() ) {
                    dispose();
                    JOptionPane.showMessageDialog( this , "Região removido com sucesso");
                } else {
                    JOptionPane.showMessageDialog( this , "Tivemos algum problema ao tentar remover esta região, entre em contato com o Administrador.");
                }
            }
        }
    }//GEN-LAST:event_actionRemoverActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RegiaoForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RegiaoForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RegiaoForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RegiaoForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RegiaoForm(null, false).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton actionCancelar;
    private javax.swing.JButton actionRemover;
    private javax.swing.JButton actionSalvarFechar;
    private javax.swing.JTextArea descricao;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField nome;
    // End of variables declaration//GEN-END:variables
}
