/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.pdf;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author cdanner
 */
public class TableHeaderSingleton {

// Static variable that contains the instance of the method
    private static TableHeaderSingleton instance;
    private static List<String> tableHeaderList;

    static {
        // Class initialization operations
    }

    // Private builder. Suppress the default public constructor
    protected TableHeaderSingleton() {
    }

    // Method public static single access to the object
    public static TableHeaderSingleton getInstance() {
        if (instance == null) {
            populateTableHeaderList();
            initializesInstance();
            //The value is returned to who is asking
        }
        return instance;
        // Returns the object instance
    }

    /*
     * This method is synchronized to avoid that due to competition are created more than
     * one instance
     */
    private static synchronized void initializesInstance() {
        if (instance == null) {
            instance = new TableHeaderSingleton();
        }
    }

    private static void populateTableHeaderList() {
        tableHeaderList = new ArrayList<>();
        tableHeaderList.add("GUIA TISS");
        tableHeaderList.add("PACIENTE");
        tableHeaderList.add("CÓDIGO CIEFAS");
        tableHeaderList.add("DESCRIÇÃO DO TRATAMENTO");
        tableHeaderList.add("VALOR UNITÁRIO (R$)");
        tableHeaderList.add("QT.");
        tableHeaderList.add("VALOR TOTAL (R$)");
    }

    public List<String> getTableHeaderList() {
        return tableHeaderList;
    }
}