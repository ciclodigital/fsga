/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.pdf;

import com.itextpdf.text.Font;
/**
 *
 * @author CRISTIAN
 */
public class TextType {

    private String textLine;
    private Integer textAlignment;
    private Font textFont;

    public TextType() {
    }

    public TextType(String textLine, Integer textAlignment, Font textFont) {
        this.textLine = textLine;
        this.textAlignment = textAlignment;
        this.textFont = textFont;
    }

    public String getTextLine() {
        return textLine;
    }

    public void setTextLine(String textLine) {
        this.textLine = textLine;
    }

    public Integer getTextAlignment() {
        return textAlignment;
    }

    public void setTextAlignment(Integer textAlignment) {
        this.textAlignment = textAlignment;
    }

    public Font getTextFont() {
        return textFont;
    }

    public void setTextFont(Font textFont) {
        this.textFont = textFont;
    }
}
