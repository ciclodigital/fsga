/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.pdf;

/**
 *
 * @author CRISTIAN
 */
public class PrintLine {

    private LineTypeEnum lineTypeEnum;
    private Object lineTypeObject;

    public PrintLine() {
    }

    public PrintLine(LineTypeEnum lineTypeEnum, Object lineTypeObject) {
        this.lineTypeEnum = lineTypeEnum;
        this.lineTypeObject = lineTypeObject;
    }

    public LineTypeEnum getLineTypeEnum() {
        return lineTypeEnum;
    }

    public void setLineTypeEnum(LineTypeEnum lineTypeEnum) {
        this.lineTypeEnum = lineTypeEnum;
    }

    public Object getLineTypeObject() {
        return lineTypeObject;
    }

    public void setLineTypeObject(Object lineTypeObject) {
        this.lineTypeObject = lineTypeObject;
    }
}
