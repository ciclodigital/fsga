/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.pdf;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

/**
 *
 * @author CRISTIAN
 */
public class AbstractSwingWorker extends SwingWorker<Object, Object> {

    protected boolean mSucess = false;
    protected ArrayList<ErrosEnum> mErrorsList;
    protected Callback mCallback;
    protected JDialog mJDialog;

    AbstractSwingWorker() {
        // TODO Auto-generated constructor stub
        this.mErrorsList = new ArrayList<>();
    }

    public Callback getCallback() {
        return mCallback;
    }

    public void setCallback(Callback callback) {
        this.mCallback = callback;
    }

    public ArrayList<ErrosEnum> getErrorsList() {
        return mErrorsList;
    }

    public boolean theListHasError(ErrosEnum errosEnum) {
        for (int i = 0; i < mErrorsList.size(); i++) {
            if (mErrorsList.get(i) == errosEnum) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected Object doInBackground() throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected void done() {
        // TODO Auto-generated method stub
        super.done();
        if (mCallback != null) {
            if (mSucess) {
                mCallback.onSucess();
            } else {
                mCallback.onError();
            }
        }
    }
}
