/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.pdf;

import java.util.ArrayList;
/**
 *
 * @author CRISTIAN
 */
public class TableType {

    private ArrayList<ArrayList<TextType>> tableTypeList;

    public TableType() {
    }

    public TableType(ArrayList<ArrayList<TextType>> tableTypeList) {
        this.tableTypeList = tableTypeList;
    }

    public ArrayList<ArrayList<TextType>> getTableTypeList() {
        return tableTypeList;
    }

    public void setTableTypeList(ArrayList<ArrayList<TextType>> tableTypeList) {
        this.tableTypeList = tableTypeList;
    }
}
