/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.pdf;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author cdanner
 */
public class HeaderSingleton {

    // Static variable that contains the instance of the method
    private static HeaderSingleton instance;
    private static List<String> headerList;

    static {
        // Class initialization operations
    }

    // Private builder. Suppress the default public constructor
    protected HeaderSingleton() {
    }

    // Method public static single access to the object
    public static HeaderSingleton getInstance() {
        if (instance == null) {
            populateHeaderList();
            initializesInstance();
            //The value is returned to who is asking
        }
        return instance;
        // Returns the object instance
    }

    /*
     * This method is synchronized to avoid that due to competition are created more than
     * one instance
     */
    private static synchronized void initializesInstance() {
        if (instance == null) {
            instance = new HeaderSingleton();
        }
    }

    private static void populateHeaderList() {
        headerList = new ArrayList<>();
        headerList.add("Fundação de Assistência e Previdência Social do BNDES - FAPES");
        headerList.add("RELAÇÕES DE SERVIÇOS PRESTADOS");
        headerList.add("FISIOFORMA SERVIÇOS DE FISIOTERAPIA - EDSERJ");
    }

    public List<String> getHeaderList() {
        return headerList;
    }
}