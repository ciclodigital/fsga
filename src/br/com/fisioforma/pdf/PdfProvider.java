/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.pdf;

import java.io.FileOutputStream;
import java.util.ArrayList;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

/**
 *
 * @author CRISTIAN
 */
public class PdfProvider implements IPdfProvider {
    
    private String path;
    private ArrayList<PrintLine> linesToPrint = new ArrayList<PrintLine>();
    private boolean mIsPortrait;

    private final String LOGO = "img/firefioforma-relatorio.jpg";
    private final Font FONT_IMAGE = new Font(Font.FontFamily.HELVETICA, 12, Font.NORMAL, GrayColor.GRAYWHITE);

    public PdfProvider(){}
    
    public PdfProvider(String path, ArrayList<PrintLine> printLinesList, boolean isPortrait) {
        this.linesToPrint = printLinesList;
        this.path = path;
        this.mIsPortrait = isPortrait;
    }
    
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public ArrayList<PrintLine> getLinesToPrint() {
        return linesToPrint;
    }

    public void setLinesToPrint(ArrayList<PrintLine> linesToPrint) {
        this.linesToPrint = linesToPrint;
    }

    /**
     * @return the mIsPortrait
     */
    public boolean ismIsPortrait() {
        return mIsPortrait;
    }

    /**
     * @param mIsPortrait the mIsPortrait to set
     */
    public void setmIsPortrait(boolean mIsPortrait) {
        this.mIsPortrait = mIsPortrait;
    }
    
    /**
     * Inner class to add a table as header and footer.
     */
    class HeaderFooterPageEvent extends PdfPageEventHelper {

        private boolean mIsPortrait;

        public HeaderFooterPageEvent(boolean isPortrait) {
            this.mIsPortrait = isPortrait;
        }

        public void onStartPage(PdfWriter writer, Document document) {
        }

        public void onEndPage(PdfWriter writer, Document document) {
            if (mIsPortrait) {
                ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_RIGHT,
                        new Phrase(String.valueOf(document.getPageNumber())), 550, 30, 0);
            } else {
                ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_RIGHT,
                        new Phrase(String.valueOf(document.getPageNumber())), 800, 30, 0);
            }
        }
    }

    public boolean execute() throws Exception {
        boolean mSucess = false;

        try {
            if (linesToPrint == null) {
                throw new Exception("Empty list.");
            }

            Document document = null;
            if (ismIsPortrait()){
                document = new Document(); // orientation portrait
            }else{
                document = new Document(PageSize.A4_LANDSCAPE.rotate()); // orientation landscape
            }    
                   
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(path));
            
            HeaderFooterPageEvent event = new HeaderFooterPageEvent(ismIsPortrait()); // Portrait or not (landscape)
	    writer.setPageEvent(event);
            
            document.open();

            // Line break to give space between the image the top.
            document.add(new Paragraph(""));

            PdfContentByte cb = writer.getDirectContentUnder();

            Image img = Image.getInstance(LOGO);
            img.scaleToFit(100, 100);
            document.add(img);

            // Loop to populate the PDF according to the parameter that was passed in the list.
            for (PrintLine printLine : linesToPrint) {
                Paragraph paragraph = null;
                PdfPCell cell = null;
                switch (printLine.getLineTypeEnum()) {
                    case TEXT:
                        TextType textType = (TextType) printLine.getLineTypeObject();

                        if (textType.getTextFont() != null) {
                            paragraph = new Paragraph(textType.getTextLine(), textType.getTextFont());
                        } else {
                            paragraph = new Paragraph(textType.getTextLine());
                        }

                        if (textType.getTextAlignment() != null) {
                            paragraph.setAlignment(textType.getTextAlignment());
                        }
                        document.add(paragraph);
                        break;

                    case TABLE:
                        TableType tableType = (TableType) printLine.getLineTypeObject();

                        /**
                        * tableType.getTableTypeList().get(0) --> HEADER (Number Columns)
                        * tableType.getTableTypeList().get(1) --> ROWS
                        * 
                        * A quantidade de linhas deve ser múltipla de acordo com o tamanho do header.
                        * Ex.: Se o tamanho de header (Number Columns) é igual a 6, a quantidade de linhas deve ser 6, 12, 18, 24...60
                        * */
                        // Get the list size to zero (header).
                        PdfPTable table = new PdfPTable(tableType.getTableTypeList().get(0).size());

                        for (int i = 0; i < tableType.getTableTypeList().get(0).size(); i++) {

                            if (tableType.getTableTypeList().get(0).get(i).getTextFont() != null) {
                                cell = new PdfPCell(new Phrase(tableType.getTableTypeList().get(0).get(i).getTextLine(), tableType.getTableTypeList().get(0).get(i).getTextFont()));
                            } else {
                                cell = new PdfPCell(new Phrase(tableType.getTableTypeList().get(0).get(i).getTextLine()));
                            }

                            if (tableType.getTableTypeList().get(0).get(i).getTextAlignment() != null) {
                                cell.setHorizontalAlignment(tableType.getTableTypeList().get(0).get(i).getTextAlignment());
                            }

                            table.addCell(cell);
                        }
                        table.setHeaderRows(1);

                        for (int i = 0; i < tableType.getTableTypeList().get(1).size(); i++) {

                            if (tableType.getTableTypeList().get(1).get(i).getTextFont() != null) {
                                cell = new PdfPCell(new Phrase(tableType.getTableTypeList().get(1).get(i).getTextLine(), tableType.getTableTypeList().get(1).get(i).getTextFont()));
                            } else {
                                cell = new PdfPCell(new Phrase(tableType.getTableTypeList().get(1).get(i).getTextLine()));
                            }

                            if (tableType.getTableTypeList().get(1).get(i).getTextAlignment() != null) {
                                cell.setHorizontalAlignment(tableType.getTableTypeList().get(1).get(i).getTextAlignment());
                            }

                            table.addCell(cell);
                        }

                        document.add(table);

                        break;

                    // TODO Implementar o gráfico (chart) e a imagem (image)
                }
            }

            document.close();

            mSucess = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mSucess;
    }
}
