/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.pdf;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author cdanner
 */
public class DepartamentoSingleton {

    // Static variable that contains the instance of the method
    private static DepartamentoSingleton instance;
    private static List<String> headerList;

    static {
        // Class initialization operations
    }

    // Private builder. Suppress the default public constructor
    protected DepartamentoSingleton() {
    }

    // Method public static single access to the object
    public static DepartamentoSingleton getInstance() {
        if (instance == null) {
            populateHeaderList();
            initializesInstance();
            //The value is returned to who is asking
        }
        return instance;
        // Returns the object instance
    }

    /*
     * This method is synchronized to avoid that due to competition are created more than
     * one instance
     */
    private static synchronized void initializesInstance() {
        if (instance == null) {
            instance = new DepartamentoSingleton();
        }
    }

    private static void populateHeaderList() {
        headerList = new ArrayList<>();
        headerList.add("Atividade");
        headerList.add("Capacidade");
        headerList.add("Demanda");
        headerList.add("% Demanda/Capacidade");
        headerList.add("Atendimentos");
        headerList.add("% Ocupação");
    }

    public List<String> getHeaderList() {
        return headerList;
    }
}
