/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.pdf;

/**
 *
 * @author CRISTIAN
 */
public class ImageType {

    private String path;
    private Float width;
    private Float heigth;

    public ImageType() {
    }

    public ImageType(String path, Float width, Float heigth) {
        this.path = path;
        this.width = width;
        this.heigth = heigth;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Float getWidth() {
        return width;
    }

    public void setWidth(Float width) {
        this.width = width;
    }

    public Float getHeigth() {
        return heigth;
    }

    public void setHeigth(Float heigth) {
        this.heigth = heigth;
    }
}
