/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author allan
 */
public class Config {
    private static Properties prop = new Properties();
    
    // Carrega as configurações do sistema.
    static {
        InputStream input = null;
        
        try {
            input = new FileInputStream("config.properties");
            prop.load(input);
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    public static String get(String name){
        return prop.getProperty(name);
    }
    
    public static void set(String name, String value) {
        prop.setProperty(name, value);
    }
}
