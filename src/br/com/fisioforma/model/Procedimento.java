/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.model;

import br.com.fisioforma.db.Util;
import br.com.fisioforma.model.service.ProcedimentoService;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author allan
 */
@Entity
@Table(name = "procedimentos", uniqueConstraints=
    @UniqueConstraint(
        columnNames = {"nome", "ciefas", "empresa_id"}
    )
)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Procedimento.findAll", query = "SELECT p FROM Procedimento p"),
    @NamedQuery(name = "Procedimento.findById", query = "SELECT p FROM Procedimento p WHERE p.id = :id"),
    @NamedQuery(name = "Procedimento.findByEmpresa", query = "SELECT p FROM Procedimento p WHERE p.empresa = :empresa"),
    @NamedQuery(name = "Procedimento.findByNome", query = "SELECT p FROM Procedimento p WHERE p.nome = :nome"),
    @NamedQuery(name = "Procedimento.likeByNome", query = "SELECT p FROM Procedimento p WHERE p.nome like :nome"),
    @NamedQuery(name = "Procedimento.findByCiefas", query = "SELECT p FROM Procedimento p WHERE p.ciefas = :ciefas"),
    @NamedQuery(name = "Procedimento.findByValor", query = "SELECT p FROM Procedimento p WHERE p.valor = :valor"),
    @NamedQuery(name = "Procedimento.notIn", query = "SELECT p FROM Procedimento p WHERE p.id not in (:procedimentos) and p.empresa = :empresa")
})
public class Procedimento implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @NotNull
    @NotEmpty(message = "O nome é obrigatório.")
    @Size(max = 64, message = "O tramanho máximo do nome é de 64 caracteres.")
    @Column(name = "nome")
    private String nome;
    
    @Lob
    @Size(max = 65535)
    @Column(name = "descricao")
    private String descricao;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "ciefas")
    @Min(value=1, message="O ciefas não deve ficar em branco.")
    private int ciefas;
    
    @Min(value=1, message = "O valor do procedimento deve ser maior ou igual que R$ 1,00") //@Max(value=?)   //if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull(message = "O valor é obrigatório")
    @Column(name = "valor")
    private BigDecimal valor = new BigDecimal(0.00);
    
    @JoinColumn(name = "empresa_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @NotNull(message = "A empresa é obrigatória para criar um procedimento.")
    private Empresa empresa;

    public Procedimento() {
    }

    public Procedimento(Integer id) {
        this.id = id;
    }

    public Procedimento(Integer id, String nome, int ciefas, BigDecimal valor) {
        this.id = id;
        this.nome = nome;
        this.ciefas = ciefas;
        this.valor = valor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getCiefas() {
        return ciefas;
    }

    public void setCiefas(int ciefas) {
        this.ciefas = ciefas;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }
    
    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }
    
    public Empresa getEmpresa(){
        return empresa;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Procedimento)) {
            return false;
        }
        Procedimento other = (Procedimento) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getCiefas() + "";
    }
    
    public boolean destroy(){
        return ProcedimentoService.getInstance().delete(this);
    }
    
    public boolean anyTratamento(){
        Session session = null;
        
        try {
            session = Util.getSessionFactory().openSession();
            
            Query query = session.createQuery("SELECT COUNT(*) from Tratamento t LEFT JOIN t.procedimentos r WHERE r.id = :procedimento_id");
                  query.setParameter("procedimento_id", getId());

            List<Object> list = query.list();
            long count = (long) list.get(0);
            return count > 0;
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        } finally {
            if (session != null) { session.close(); }
        }
        
        return false;
    }
}
