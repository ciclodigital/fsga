/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.model;

import br.com.fisioforma.db.Util;
import br.com.fisioforma.model.service.AtendimentoService;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author allan
 */
@Entity
@Table(name = "atendimentos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Atendimento.findAll", query = "SELECT a FROM Atendimento a"),
    @NamedQuery(name = "Atendimento.findById", query = "SELECT a FROM Atendimento a WHERE a.id = :id"),
    @NamedQuery(name = "Atendimento.findByFisioterapeuta", query = "SELECT a FROM Atendimento a WHERE a.fisioterapeuta = :fisioterapeuta"),
    @NamedQuery(name = "Atendimento.findByFisioterapeutaAndPeriodo", query = "SELECT a FROM Atendimento a WHERE a.fisioterapeuta = :fisioterapeuta and a.data BETWEEN :startDate and :endDate"),
    @NamedQuery(name = "Atendimento.findByData", query = "SELECT a FROM Atendimento a WHERE a.data = :data"),
    @NamedQuery(name = "Atendimento.findByCompareceu", query = "SELECT a FROM Atendimento a WHERE a.status = :compareceu"),
    @NamedQuery(name = "Atendimento.findByPaciente", query = "SELECT a FROM Atendimento a WHERE a.tratamento.paciente = :paciente"),
    @NamedQuery(name = "Atendimento.findByFisioterapeutaSetorHorarioAndData", query = "SELECT a FROM Atendimento a WHERE a.fisioterapeuta = :fisioterapeuta and a.horario = :horario and a.data = :data"),
    @NamedQuery(name = "Atendimento.findByFisioterapeutaSetorHorarioAndBetweenData", query = "SELECT a FROM Atendimento a WHERE a.fisioterapeuta = :fisioterapeuta and a.horario = :horario and a.data BETWEEN :startDate and :endDate"),
    @NamedQuery(name = "Atendimento.findBySetorHorarioAndData", query = "select a from Atendimento a where a.tratamento.setor = :setor and a.data = :data and a.horario.inicio = :inicio"),
    @NamedQuery(name = "Atendimento.findForDiario", query = "select a from Atendimento a where a.tratamento.setor = :setor and a.data = :data and a.horario.inicio = :inicio and a.tratamento.status != 'CANCELADO' and a.tratamento.status != 'ALTA'"),
    @NamedQuery(name = "Atendimento.findByTratamento", query = "select a from Atendimento a where a.tratamento = :tratamento"),
    @NamedQuery(name = "Atendimento.countAtendimentoByTratamentoAndStatus", query = "SELECT count(a) FROM Atendimento a where a.tratamento = :tratamento and a.status in (:status)"),
    @NamedQuery(name = "Atendimento.findAtendimentoByTratamentoAndStatus", query = "SELECT a FROM Atendimento a where a.tratamento = :tratamento and a.status in (:status)"),
    @NamedQuery(name = "Atendimento.report", query="SELECT a FROM Atendimento a where a.fisioterapeuta in (:fisioterapeutas) and a.tratamento.status in (:stautsTratamento) AND a.status in (:status) AND a.data BETWEEN :start AND :end AND a.horario in (:horarios) AND a.tratamento.paciente.empresa in (:empresas)"),
    @NamedQuery(name = "Atendimento.ReportAtendimentoPorTipo", query="SELECT a FROM Atendimento a WHERE a.data BETWEEN :start AND :end AND a.tratamento.paciente.empresa in (:empresas) AND a.status in (:status) AND a.tratamento.setor in (:setores) AND a.tratamento.tipo in (:tiposTratamento)"),
    @NamedQuery(name = "Atendimento.CountBy", query="SELECT count(*) FROM Atendimento a WHERE a.data BETWEEN :start AND :end AND a.tratamento.paciente.empresa in (:empresas) AND a.status in (:status) AND a.tratamento.setor in (:setores) AND a.tratamento.tipo in (:tiposTratamento) AND a.tratamento.status in (:statusTratamento)"),
    @NamedQuery(name = "Atendimento.findByPacienteHorarioAndStatus", query = "select a from Atendimento a where a.tratamento.paciente = :paciente and a.horario = :horario and a.status in (:status)"),
    @NamedQuery(name = "Atendimento.findByFisioDataHorarioStatusAndTratametoStatus", query="SELECT a FROM Atendimento a where a.fisioterapeuta = :fisioterapeuta and a.tratamento.status = :statusTratamento and a.data BETWEEN :start and :end and a.horario = :horario and a.status in (:status)"),
    @NamedQuery(name = "Atendimento.CountGroupByRecurso", query = "SELECT r.sigla, count(r.sigla) FROM Atendimento a LEFT JOIN a.tratamento.recursos r WHERE a.data BETWEEN :start AND :end AND a.tratamento.paciente.empresa in (:empresas) GROUP BY r.sigla")
})
public class Atendimento implements Serializable {
    public static final String FIND_ALL = "Atendimento.findAll";
    public static final String FIND_BY_PACIENTE = "Atendimento.findByPaciente";
    public static final String FIND_BY_FISIOTERAPEUTA_SETOR_AND_DATA = "Atendimento.findByFisioterapeutaSetorHorarioAndData";
    public static final String FIND_BY_FISIOTERAPEUTA_SETOR_AND_BETWEEN_DATA = "Atendimento.findByFisioterapeutaSetorHorarioAndBetweenData";
    public static final String FIND_BY_SETOR_HORARIO_AND_DATA = "Atendimento.findBySetorHorarioAndData";
    public static final String FIND_FOR_DIARIO = "Atendimento.findForDiario";
    public static final String FIND_BY_TRATAMENTO = "Atendimento.findByTratamento";
    public static final String COUNT_ATENDIMENTO_BY_TRATAMENTO_STATUS = "Atendimento.countAtendimentoByTratamentoAndStatus";
    public static final String FIND_ATENDIMENTO_BY_TRATAMENTO_STATUS = "Atendimento.findAtendimentoByTratamentoAndStatus";
    public static final String FIND_BY_FISIO_DATA_HORARIO_STATUS_AND_TRATAMENTOSTATUS = "Atendimento.findByFisioDataHorarioStatusAndTratametoStatus";
    public static final String FIND_BY_PACIENTE_HORARIO_AND_STATUS = "Atendimento.findByPacienteHorarioAndStatus";
    public static final String REPORT = "Atendimento.report";
    public static final String REPORT_ATENDIMENTO_POR_TIPO = "Atendimento.ReportAtendimentoPorTipo";
    public static final String COUNT_BY = "Atendimento.CountBy";
    
    public static final AtendimentoStatusEnum[] STATUS_ATENDIMENTO = { AtendimentoStatusEnum.AGENDADO, AtendimentoStatusEnum.COMPARECEU };
    public static final AtendimentoStatusEnum[] STATUS_FALTOSO     = { AtendimentoStatusEnum.FALTA   , AtendimentoStatusEnum.FALTA_JUSTIFICADA };
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "fisioterapeuta_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    private Fisioterapeuta fisioterapeuta;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "data")
    @Temporal(TemporalType.DATE)
    private Date data;
    
    @Lob
    @Size(max = 65535)
    @Column(name = "justificativa")
    private String justificativa;
    
    @JoinColumn(name = "setor_horario_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private SetorHorario horario;
        
    @JoinColumn(name = "tratamento_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Tratamento tratamento;
    
    @Basic(optional = false)
    @NotNull(message = "O Status do tratamento é obrigatório")
    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private AtendimentoStatusEnum status = AtendimentoStatusEnum.AGENDADO;

    public Atendimento() {
    }

    public Atendimento(Integer id) {
        this.id = id;
    }

    public Atendimento(Integer id, Fisioterapeuta fisioterapeuta, Date data) {
        this.id = id;
        this.fisioterapeuta = fisioterapeuta;
        this.data = data;
    }
    
    public Atendimento(Fisioterapeuta fisioterapeuta, Date data, Tratamento tratamento, SetorHorario horario) {
        this.fisioterapeuta = fisioterapeuta;
        this.data = data;
        this.tratamento = tratamento;
        this.horario = horario;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Fisioterapeuta getFisioterapeuta() {
        return fisioterapeuta;
    }

    public void setFisioterapeuta(Fisioterapeuta fisioterapeuta) {
        this.fisioterapeuta = fisioterapeuta;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }
    
    public void setStatus(AtendimentoStatusEnum status){
        this.status = status;
    }
    
    public AtendimentoStatusEnum getStatus(){
        return status;
    }

    public String getJustificativa() {
        return justificativa;
    }

    public void setJustificativa(String justificativa) {
        this.justificativa = justificativa;
    }

    public SetorHorario getSetorHorario() {
        return horario;
    }

    public void setSetorHorario(SetorHorario horario) {
        this.horario = horario;
    }

    public Tratamento getTratamento() {
        return tratamento;
    }

    public void setTratamento(Tratamento tratamento) {
        this.tratamento = tratamento;
    }
    
    public void compareceu(){
        status = AtendimentoStatusEnum.COMPARECEU;
    }
    
    public void faltou(){
        status = AtendimentoStatusEnum.FALTA;
    }
    
    public void faltouEJustificou(String justificativa){
        status = AtendimentoStatusEnum.FALTA_JUSTIFICADA;
        this.justificativa = justificativa;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Atendimento)) {
            return false;
        }
        Atendimento other = (Atendimento) object;
//        
        // validate by id
        if( this.id != null && this.id == other.id ) {
            return false;
        }
        
        //validate by unique value
        if( this.tratamento.getId() == other.tratamento.getId() &&
            this.fisioterapeuta.getId() == other.fisioterapeuta.getId() &&
            this.horario.equals(other.horario) &&
            this.data.equals(other.horario)
          ) {
            return false;
        }
        
        return true;
    }

    @Override
    public String toString() {
        return "br.com.fisioforma.model.Atendimentos[ id=" + id + " ]";
    }
    
    public static long countBy (
            List<Empresa> empresas,
            List<AtendimentoStatusEnum> status,
            List<TratamentoTipoEnum> tipoTratamento,
            List<Setor> setores,
            Date start, Date end) {
        
        long count = 0;
        
        Session session = null;
        
        try {
            session = Util.getSessionFactory().openSession();
            
            Query query = session.getNamedQuery(Atendimento.COUNT_BY);
            
            query.setParameter("start", start);
            query.setParameter("end"  , end  );
            query.setParameterList("empresas", empresas);
            query.setParameterList("status", status);
            query.setParameterList("setores", setores);
            query.setParameterList("tiposTratamento", tipoTratamento);
            query.setParameterList("statusTratamento", TratamentoStatusEnum.asList());
            
            List<Object> list = query.list();
            
            count = (long) list.get(0);
            
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        } finally {
            if( session == null ) { session.close(); } 
        }
        
        return count;
    }
    
    public static long countBy (
            List<Empresa> empresas,
            List<AtendimentoStatusEnum> status,
            List<TratamentoTipoEnum> tipoTratamento,
            List<TratamentoStatusEnum> statusTratamento,
            List<Setor> setores,
            Date start, Date end) {
        
        long count = 0;
        
        Session session = null;
        
        try {
            session = Util.getSessionFactory().openSession();
            
            Query query = session.getNamedQuery(Atendimento.COUNT_BY);
            
            query.setParameter("start", start);
            query.setParameter("end"  , end  );
            query.setParameterList("empresas", empresas);
            query.setParameterList("status", status);
            query.setParameterList("setores", setores);
            query.setParameterList("tiposTratamento", tipoTratamento);
            query.setParameterList("statusTratamento", statusTratamento);
            
            List<Object> list = query.list();
            
            count = (long) list.get(0);
            
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        } finally {
            if( session == null ) { session.close(); } 
        }
        
        return count;
    }
    
    public static List<String[]> metricsBySetorWithSetorAtendimentos(List<Empresa> empresasSelecionas, Map<Setor, Integer> setorAtendimentos, Map<Setor, Integer> setoresDemanda, Date inicio, Date fim) {
        List<String[]> lists = new ArrayList<String[]>();
        DecimalFormat df = new DecimalFormat("#.##");
        
        try {
            double[] totals = { 0, 0, 0, 0 };
            
            for( Map.Entry entry : setoresDemanda.entrySet() ) {
                Setor setor = (Setor) entry.getKey();
                int demanda = setorAtendimentos.get(entry.getKey());
                int capacidade = (int) entry.getValue();
                int atendimentos = (int) Atendimento.countBy(empresasSelecionas, Arrays.asList(Atendimento.STATUS_ATENDIMENTO), TratamentoTipoEnum.asList(), Arrays.asList(setor), inicio, fim);
                
                double demandaCapacidade = ( ( demanda * 1f ) / ( capacidade * 1f ) ) * 100f;
                double ocupacao = ( ( atendimentos * 1f ) / ( capacidade * 1f ) ) * 100f;
                
                String[] row = { setor.getNome(), capacidade+"", demanda+"", df.format(demandaCapacidade)+"%", atendimentos+"", df.format(ocupacao)+"%" };
                totals[0] += capacidade;
                totals[1] += demanda;
                totals[3] += atendimentos;
                lists.add(row);
            }
            
            double demandaCapacidade = totals[1] / totals[0] * 100f;
            double ocupacao = totals[3] / totals[0] * 100f;
            
            String[] row = { "Totalização", totals[0]+"", totals[1]+"", df.format(demandaCapacidade)+"%", totals[3]+"", df.format(ocupacao)+"%" };
            lists.add(row);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        
        return lists;
    }
    
    public static List<String[]> metricsBySetor(List<Empresa> empresasSelecionas, Map<Setor, Integer> setoresDemanda, Date inicio, Date fim) {
        List<String[]> lists = new ArrayList<String[]>();
        DecimalFormat df = new DecimalFormat("#.##");
        
        try {
            double[] totals = { 0, 0, 0, 0 };
            
            for( Map.Entry entry : setoresDemanda.entrySet() ) {
                Setor setor = (Setor) entry.getKey();
                int demanda = (int) Atendimento.countBy(empresasSelecionas, AtendimentoStatusEnum.asList(), TratamentoTipoEnum.asList(), Arrays.asList(setor), inicio, fim);
                int capacidade = (int) entry.getValue();
                int atendimentos = (int) Atendimento.countBy(empresasSelecionas, Arrays.asList(Atendimento.STATUS_ATENDIMENTO), TratamentoTipoEnum.asList(), Arrays.asList(setor), inicio, fim);
                
                double demandaCapacidade = ( ( demanda * 1f ) / ( capacidade * 1f ) ) * 100f;
                double ocupacao = ( ( atendimentos * 1f ) / ( capacidade * 1f ) ) * 100f;
                
                String[] row = { setor.getNome(), capacidade+"", demanda+"", df.format(demandaCapacidade)+"%", atendimentos+"", df.format(ocupacao)+"%" };
                totals[0] += capacidade;
                totals[1] += demanda;
                totals[3] += atendimentos;
                lists.add(row);
            }
            
            double demandaCapacidade = totals[1] / totals[0] * 100f;
            double ocupacao = totals[3] / totals[0] * 100f;
            
            String[] row = { "Totalização", totals[0]+"", totals[1]+"", df.format(demandaCapacidade)+"%", totals[3]+"", df.format(ocupacao)+"%" };
            lists.add(row);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        
        return lists;
    }

    public String getToolTip() {
        return getTratamento().toToolTip() + " - Justificativa: " + getJustificativa();
    }
    
    public boolean destroy(){       
        return AtendimentoService.getInstance().delete(this);
    }
    
    public static List<Object[]> countGroupByRecurso(
       List<Empresa> empresas,
       Date start,
       Date end
    ){
        List<Object[]> group = new ArrayList<Object[]>();
        
        Session session = null;
        
        try {
            session = Util.getSessionFactory().openSession();
            Query query = session.getNamedQuery("Atendimento.CountGroupByRecurso");
            
            query.setParameterList("empresas", empresas);
            query.setParameter("start", start);
            query.setParameter("end"  , end  );
            
            group = query.list();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        } finally {
            if( session == null ) { session.close(); } 
        }
        
        return group;
    }
}
