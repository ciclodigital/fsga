/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.model;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author allan
 */
public enum TratamentoTipoEnum {
    // Este tipo de tratamento ocorrer como padrão
    ROTINA,
    
    // Este tipo de tratamento ocorre somente em casos de emergência.
    EMERGENCIAL;
    
    public static List<TratamentoTipoEnum> asList() {
        return Arrays.asList( values() );
    }
}
