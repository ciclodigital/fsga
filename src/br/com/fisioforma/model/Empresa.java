/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.model;

import br.com.fisioforma.util.StoreFile;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.validator.Size;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author allan
 */
@Entity
@Table(name = "empresas")
@NamedQueries({
    @NamedQuery(name = "Empresa.findAll", query = "SELECT e FROM Empresa e"),
    @NamedQuery(name = "Empresa.findById", query = "SELECT e FROM Empresa e WHERE e.id = :id"),
    @NamedQuery(name = "Empresa.findByNome", query = "SELECT e FROM Empresa e WHERE e.nome = :nome"),
    @NamedQuery(name = "Empresa.findByNomes", query = "SELECT e FROM Empresa e WHERE e.nome in (:nomes)"),
    @NamedQuery(name = "Empresa.likeByNome", query = "SELECT e FROM Empresa e WHERE e.nome like :nome")})
public class Empresa implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @NotEmpty(message = "O nome da empresa é obrigatório.")
    @Column(name = "nome", unique=true)
    private String nome;
    
    @Basic(optional = true)
    @Column(name = "cnpj", unique=true)
    private String cnpj;
    
    @Lob
    @Size(max = 65535)
    @Column(name = "descricao")
    private String descricao;
    
    @Size(min = 1, message = "O valor mínimo de quantidade de procedimentos por empresa é 1.")
    @NotNull(message = "O limite de procedimentos por empresa é obrigatório.")
    @Column(name="limite_procedimentos")
    private int limiteProcedimentos = 1;
    
    @OneToMany(mappedBy = "empresa")
    private List<Paciente> pacientes;
    
    public Empresa() {
    }

    public Empresa(Integer id) {
        this.id = id;
    }

    public Empresa(Integer id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        Integer oldId = this.id;
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        String oldNome = this.nome;
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        String oldDescricao = this.descricao;
        this.descricao = descricao;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Empresa)) {
            return false;
        }
        Empresa other = (Empresa) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getNome().toUpperCase();
    }

    @XmlTransient
    public List<Paciente> getpacientes() {
        return pacientes;
    }

    public void setpacientes(List<Paciente> pacientes) {
        this.pacientes = pacientes;
    }

    /**
     * @return the limiteProcedimentos
     */
    public int getLimiteProcedimentos() {
        return limiteProcedimentos;
    }

    /**
     * @param limiteProcedimentos the limiteProcedimentos to set
     */
    public void setLimiteProcedimentos(int limiteProcedimentos) {
        if( limiteProcedimentos >= 1 ) {
            this.limiteProcedimentos = limiteProcedimentos;
        }
    }

    /**
     * @return the cnpj
     */
    public String getCnpj() {
        return cnpj;
    }

    /**
     * @param cnpj the cnpj to set
     */
    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }
    
    @Transient
    public boolean createLogo(String source){
        String target = "empresa-" + getId().toString();
        return StoreFile.store(source, target);
    }
    
    @Transient
    public String getLogoPath(){
        return StoreFile.getStorePath("empresa-" + getId().toString());
    }
}
