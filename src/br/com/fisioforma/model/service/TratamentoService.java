/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.model.service;

import br.com.fisioforma.db.Service;
import br.com.fisioforma.model.Tratamento;
import org.dom4j.util.SingletonStrategy;

/**
 *
 * @author allan
 */
public class TratamentoService extends Service<Long, Tratamento> {    
    public TratamentoService() {
        super( Tratamento.class );
    }
    
    private static TratamentoService instance;
    public static TratamentoService getInstance(){
        if( instance == null ) instance = new TratamentoService();
        
        return instance;
    }
}
