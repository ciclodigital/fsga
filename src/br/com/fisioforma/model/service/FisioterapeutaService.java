/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.model.service;

import br.com.fisioforma.db.Service;
import br.com.fisioforma.model.Fisioterapeuta;

/**
 *
 * @author allan
 */
public class FisioterapeutaService extends Service<Long, Fisioterapeuta> {
    public FisioterapeutaService(){
        super(Fisioterapeuta.class);
    }
    
    private static FisioterapeutaService instance;
    public static FisioterapeutaService getInstance(){
        if( instance == null ) instance = new FisioterapeutaService();
        
        return instance;
    }
}
