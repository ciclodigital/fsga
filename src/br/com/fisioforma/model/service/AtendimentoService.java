/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.model.service;

import br.com.fisioforma.db.AbstractServiceQuery;
import br.com.fisioforma.db.IService;
import br.com.fisioforma.db.Service;
import br.com.fisioforma.db.Util;
import br.com.fisioforma.model.Atendimento;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author allan
 */
public class AtendimentoService extends Service<Long, Atendimento>  {
    public AtendimentoService(){
        super(Atendimento.class);
    }
    
    private static AtendimentoService instance;
    public static AtendimentoService getInstance(){
        if( instance == null ) instance = new AtendimentoService();
        
        return instance;
    }
}
