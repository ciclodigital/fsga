/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.model.service;

import br.com.fisioforma.db.Service;
import br.com.fisioforma.model.Usuario;

/**
 *
 * @author allan
 */
public class UsuarioService extends Service<Long, Usuario> {
    public UsuarioService(){
        super(Usuario.class);
    }
    
    private static UsuarioService instance;
    public static UsuarioService getInstance(){
        if( instance == null ) instance = new UsuarioService();
        
        return instance;
    }
}
