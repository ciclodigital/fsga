/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.model.service;

import br.com.fisioforma.db.Service;
import br.com.fisioforma.model.Empresa;

/**
 *
 * @author allan
 */
public class EmpresaService extends Service<Long, Empresa> {    
    public EmpresaService(){
        super(Empresa.class);
    }
    
    private static EmpresaService instance;
    public static EmpresaService getInstance(){
        if( instance == null ) instance = new EmpresaService();
        
        return instance;
    }
}
