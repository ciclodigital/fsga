/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.model.service;

import br.com.fisioforma.db.Service;
import br.com.fisioforma.model.Regiao;

/**
 *
 * @author allan
 */
public class RegiaoService extends Service<Long, Regiao> {
    public RegiaoService () {
        super(Regiao.class);
        
    }
    
    private static RegiaoService instance;
    public static RegiaoService getInstance(){
        if( instance == null ) instance = new RegiaoService();
        
        return instance;
    }
}
