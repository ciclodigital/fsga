/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.model.service;

import br.com.fisioforma.db.Service;
import br.com.fisioforma.model.Recurso;

/**
 *
 * @author allan
 */
public class RecursoService extends Service<Long, Recurso> {
    public RecursoService(){
        super(Recurso.class);
    }
    
    private static RecursoService instance;
    public static RecursoService getInstance(){
        if( instance == null ) instance = new RecursoService();
        
        return instance;
    }
}
