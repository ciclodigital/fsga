/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.model.service;

import br.com.fisioforma.db.Service;
import br.com.fisioforma.model.Procedimento;

/**
 *
 * @author allan
 */
public class ProcedimentoService extends Service<Long, Procedimento> {
    public ProcedimentoService(){
        super(Procedimento.class);
    }
    
    private static ProcedimentoService instance;
    public static ProcedimentoService getInstance(){
        if( instance == null ) instance = new ProcedimentoService();
        
        return instance;
    }
}
