/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.model.service;

import br.com.fisioforma.db.Service;
import br.com.fisioforma.model.Satisfacao;

/**
 *
 * @author allan
 */
public class SatisfacaoService extends Service<Long, Satisfacao> {
    public SatisfacaoService(){
        super(Satisfacao.class);
    }
    
    private static SatisfacaoService instance;
    public static SatisfacaoService getInstance(){
        if( instance == null ) instance = new SatisfacaoService();
        
        return instance;
    }
}
