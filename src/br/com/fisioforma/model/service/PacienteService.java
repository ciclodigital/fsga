/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.model.service;

import br.com.fisioforma.db.Service;
import br.com.fisioforma.model.Paciente;

/**
 *
 * @author allan
 */
public class PacienteService extends Service<Long, Paciente> {
    public PacienteService(){
        super(Paciente.class);
        
    }
    
    private static PacienteService instance;
    public static PacienteService getInstance(){
        if( instance == null ) instance = new PacienteService();
        
        return instance;
    }
}
