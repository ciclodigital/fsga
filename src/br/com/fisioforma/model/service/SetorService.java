/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.model.service;

import br.com.fisioforma.db.Service;
import br.com.fisioforma.model.Setor;

/**
 *
 * @author allan
 */
public class SetorService extends Service<Long, Setor> {
    public SetorService(){
        super(Setor.class);
        
    }
    
    private static SetorService instance;
    public static SetorService getInstance(){
        if( instance == null ) instance = new SetorService();
        
        return instance;
    }
}
