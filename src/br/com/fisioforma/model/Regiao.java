/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.model;

import br.com.fisioforma.db.Util;
import br.com.fisioforma.model.service.RecursoService;
import br.com.fisioforma.model.service.RegiaoService;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author allan
 */
@Entity
@Table(name = "regioes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Regiao.findAll", query = "SELECT r FROM Regiao r"),
    @NamedQuery(name = "Regiao.findById", query = "SELECT r FROM Regiao r WHERE r.id = :id"),
    @NamedQuery(name = "Regiao.findByNome", query = "SELECT r FROM Regiao r WHERE r.nome = :nome"),
    @NamedQuery(name = "Regiao.likeByNome", query = "SELECT r FROM Regiao r WHERE LOWER(r.nome) like LOWER(:nome)"),
    @NamedQuery(name = "Regiao.notIn", query = "SELECT r FROM Regiao r where r.id not in (:regioes)")
})
public class Regiao implements Serializable {
    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 64)
    @Column(name = "nome", unique=true)
    private String nome;
    
    @Lob
    @Size(max = 65535)
    @Column(name = "descricao")
    private String descricao;

    public Regiao() {
    }

    public Regiao(Integer id) {
        this.id = id;
    }

    public Regiao(Integer id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        Integer oldId = this.id;
        this.id = id;
        changeSupport.firePropertyChange("id", oldId, id);
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        String oldNome = this.nome;
        this.nome = nome;
        changeSupport.firePropertyChange("nome", oldNome, nome);
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        String oldDescricao = this.descricao;
        this.descricao = descricao;
        changeSupport.firePropertyChange("descricao", oldDescricao, descricao);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Regiao)) {
            return false;
        }
        Regiao other = (Regiao) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nome;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
    public boolean destroy(){
        return RegiaoService.getInstance().delete(this);
    }
    
    public boolean anyTratamento(){
        Session session = null;
        
        try {
            session = Util.getSessionFactory().openSession();
            
            Query query = session.createQuery("SELECT COUNT(*) from Tratamento t LEFT JOIN t.regioes r WHERE r.id = :regiao_id");
                  query.setParameter("regiao_id", getId());

            List<Object> list = query.list();
            long count = (long) list.get(0);
            return count > 0;
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        } finally {
            if (session != null) { session.close(); }
        }
        
        return false;
    }
}
