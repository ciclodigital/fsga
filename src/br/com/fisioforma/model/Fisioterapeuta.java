/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.model;

import br.com.fisioforma.db.Util;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author allan
 */
@Entity
@Table(name = "fisioterapeutas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Fisioterapeuta.findAll", query = "SELECT f FROM Fisioterapeuta f"),
    @NamedQuery(name = "Fisioterapeuta.findAllAtivos", query = "SELECT f FROM Fisioterapeuta f WHERE f.ativo = true"),
    @NamedQuery(name = "Fisioterapeuta.findById", query = "SELECT f FROM Fisioterapeuta f WHERE f.id = :id"),
    @NamedQuery(name = "Fisioterapeuta.findByNome", query = "SELECT f FROM Fisioterapeuta f WHERE f.nome = :nome"),
    @NamedQuery(name = "Fisioterapeuta.likeByNome", query = "SELECT f FROM Fisioterapeuta f WHERE f.nome like :nome"),
    @NamedQuery(name = "Fisioterapeuta.findByCrefito", query = "SELECT f FROM Fisioterapeuta f WHERE f.crefito = :crefito"),
    @NamedQuery(name = "Fisioterapeuta.findByCpf", query = "SELECT f FROM Fisioterapeuta f WHERE f.cpf = :cpf"),
    @NamedQuery(name = "Fisioterapeuta.findBySetor", query = "SELECT f FROM Fisioterapeuta f WHERE f.setor = :setor"),
    @NamedQuery(name = "Fisioterapeuta.countBySetorAndAtivo", query = "SELECT count(f) FROM Fisioterapeuta f WHERE f.setor = :setor and f.ativo = :ativo"),
    @NamedQuery(name = "Fisioterapeuta.findAllbySetorAndAtivo", query = "SELECT f FROM Fisioterapeuta f WHERE f.setor = :setor and f.ativo = true"),
    @NamedQuery(name = "Fisioterapeuta.countBySetorAtivoAndPeriodo", query = "SELECT count(f) FROM Fisioterapeuta f WHERE f.setor = :setor and f.ativo = :ativo and f.periodo = :periodo")
})
public class Fisioterapeuta implements Serializable {
    public static final int LIMITE_ATENDIMENTOS_POR_HORARIO = 4;
    
    public static final String BY_SETOR = "Fisioterapeuta.findBySetor";
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @NotEmpty(message = "O nome é obrigatório")
    @Size(max = 128)
    @Column(name = "nome", unique = true)
    private String nome;
    
    @Basic(optional = false)
    @NotEmpty(message = "O crefito é obrigatório")
    @Size(max = 45)
    @Column(name = "crefito", unique = true)
    private String crefito;
    
    @Basic(optional = false)
    @NotEmpty(message = "O cpf é obrigatório")
    @Size(min = 11, max = 11)
    @Column(name = "cpf", unique=true)
    private String cpf;
    
    @JoinColumn(name = "setor_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Setor setor;
    
    @Basic(optional = false)
    @NotNull(message = "Deve ser definido o período de atendimento (MANHA ou TARDE).")
    @Column(name = "periodo") //, columnDefinition = "enum('MANHA','TARDE')")
    @Enumerated(EnumType.STRING)
    private AtendimentoPeriodoEnum periodo = null;
    
    @Basic(optional = false)
    @NotNull(message = "Deve marcar o fisioterapeuta como ativo ou inativo")
    @Column(name = "ativo")
    private boolean ativo = true;
    
    public Fisioterapeuta() {
    }

    public Fisioterapeuta(Integer id) {
        this.id = id;
    }

    public Fisioterapeuta(Integer id, String nome, String cpf) {
        this.id = id;
        this.nome = nome;
        this.cpf = cpf;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCrefito() {
        return crefito;
    }

    public void setCrefito(String crefito) {
        this.crefito = crefito;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
    
    public AtendimentoPeriodoEnum getPeriodo(){
        return periodo;
    }
    
    public void setPeriodo(AtendimentoPeriodoEnum periodo){
        this.periodo = periodo;
    }
    
    public Setor getSetor() {
        return setor;
    }

    public void setSetor(Setor setor) {
        this.setor = setor;
    }
    
    public boolean getAtivo(){
        return ativo;
    }
    
    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Fisioterapeuta)) {
            return false;
        }
        Fisioterapeuta other = (Fisioterapeuta) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)) ) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getNome();
    }

    @Transactional
    public List<SetorHorario> getHorarios() throws Exception {
        Session session = null;
        List<SetorHorario> horarios = null;
        
        try {
            session = Util.getSessionFactory().openSession();
            Query query = session.createQuery("select distinct h from SetorHorario h where h.setor = :setor and h.periodo = :periodo");
            
            query.setParameter("setor", setor);
            query.setParameter("periodo", periodo);

            horarios = query.list();
        } catch (Exception e) {
            
        } finally {
            if( session != null ) session.close();
        }
        
        return horarios;
    }

    public boolean isLimitedAtendimentoByHoraAndHorario(Date data, SetorHorario horario) {
        Session session = null;
        
        try {
            session = Util.getSessionFactory().openSession();
            /**
             * Pegar Todos os atendimentos de um fisioterapeuta de um horário
             * que não estejam marcados como falta ou falta justificada e
             * que não contenha o atendimento atual.
             */
            Query query = session.createQuery("SELECT count(a) FROM Atendimento a "
                    + "where a.fisioterapeuta = :fisioterapeuta and "
                    + "a.tratamento.status = :statusTratamento and "
                    + "a.data = :data and " 
                    + "a.horario = :horario and "
                    + "a.status in (:status)");

            query.setParameter("fisioterapeuta", this);
            query.setParameter("statusTratamento", TratamentoStatusEnum.ANDAMENTO);
            query.setParameter("data", data);
            query.setParameter("horario", horario);
            query.setParameterList("status", Atendimento.STATUS_ATENDIMENTO);
            
            long count = ((long) query.uniqueResult());
            
            return count >= Fisioterapeuta.LIMITE_ATENDIMENTOS_POR_HORARIO;
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        
        return false;
    }
    
    public static long countBySetorAndAtivo(Setor setor, boolean ativo) {
        Session session = null;
        
        try {
            session = Util.getSessionFactory().openSession();
            Query query = session.getNamedQuery("Fisioterapeuta.countBySetorAndAtivo");
            query.setParameter("setor", setor);
            query.setParameter("ativo", ativo);
            
            return (long) query.uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        } finally {
            if( session != null ) session.close();
        }
        
        return 0;
    }
    
    public static long countBySetorAtivoAndPeriodo(Setor setor, boolean ativo, AtendimentoPeriodoEnum periodo) {
        Session session = null;
        
        try {
            session = Util.getSessionFactory().openSession();
            Query query = session.getNamedQuery("Fisioterapeuta.countBySetorAtivoAndPeriodo");
            query.setParameter("setor", setor);
            query.setParameter("ativo", ativo);
            query.setParameter("periodo", periodo);
            
            return (long) query.uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        } finally {
            if( session != null ) session.close();
        }
        
        return 0;
    }
    
}
