/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.model;

import java.awt.Color;
import java.util.Map;

/**
 *
 * @author allan
 */
public final class TratamentoTipoColor {
    private TratamentoTipoColor(){}
    
    private static Map<TratamentoTipoEnum, Color> colors;
    
    static {
        colors.put(TratamentoTipoEnum.ROTINA, Color.green);
        colors.put(TratamentoTipoEnum.EMERGENCIAL, Color.red);
    }
    
    public static Color getColor( TratamentoTipoEnum tipo ){
        return colors.get(tipo);
    }
}
