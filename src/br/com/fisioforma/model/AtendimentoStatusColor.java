/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.model;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author allan
 */
public class AtendimentoStatusColor {
    static private Map<AtendimentoStatusEnum, Color> colors = new HashMap<AtendimentoStatusEnum, Color>();
    
    static {
        colors.put(AtendimentoStatusEnum.AGENDADO, new Color(107,107,255));
        colors.put(AtendimentoStatusEnum.COMPARECEU, new Color(107,255,107));
        colors.put(AtendimentoStatusEnum.FALTA, new Color(255,107,107));
        colors.put(AtendimentoStatusEnum.FALTA_JUSTIFICADA, new Color(255,180,107));
    }
    
    public static Color getColor( AtendimentoStatusEnum status ){
        return colors.get(status);
    }
    
    private AtendimentoStatusColor(){};
}
