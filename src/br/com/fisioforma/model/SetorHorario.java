/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.model;

import br.com.fisioforma.exception.HorarioException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.joda.time.LocalTime;

/**
 *
 * @author allan
 */
@Entity
@Table(name = "setor_horarios", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"fim", "inicio", "setor_id"}),
    @UniqueConstraint(columnNames = {"setor_id", "inicio"}),
    @UniqueConstraint(columnNames = {"fim", "setor_id"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SetorHorario.findAll", query = "SELECT s FROM SetorHorario s"),
    @NamedQuery(name = "SetorHorario.findById", query = "SELECT s FROM SetorHorario s WHERE s.id = :id"),
    @NamedQuery(name = "SetorHorario.findByInicio", query = "SELECT s FROM SetorHorario s WHERE s.inicio = :inicio"),
    @NamedQuery(name = "SetorHorario.findByFim", query = "SELECT s FROM SetorHorario s WHERE s.fim = :fim"),
    @NamedQuery(name = "SetorHorario.countBySetor", query = "SELECT count(s) FROM SetorHorario s WHERE s.setor = :setor"),
    @NamedQuery(name = "SetorHorario.countBySetorAndPeriodo", query = "SELECT count(s) FROM SetorHorario s WHERE s.setor = :setor and s.periodo = :periodo")
})
public class SetorHorario implements Serializable, Comparable {
    private static final long serialVersionUID = 1L;
    
    @Transient
    private DateFormat dateFormat = new SimpleDateFormat("HH:mm");
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "inicio", nullable = false)
    @Temporal(TemporalType.TIME)
    private Date inicio;
    
    @Column(name = "fim")
    @Temporal(TemporalType.TIME)
    private Date fim;
    
    @JoinColumn(name = "setor_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Setor setor;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "horario")
    private List<Atendimento> atendimentos;
    
    @Basic(optional = false)
    @NotNull(message = "Deve ser definido o período de atendimento (MANHA ou TARDE).")
    @Column(name = "periodo") //, columnDefinition = "enum('MANHA','TARDE')")
    @Enumerated(EnumType.STRING)
    private AtendimentoPeriodoEnum periodo = null;

    public SetorHorario() {
    }

    public SetorHorario(Integer id) {
        this.id = id;
    }

    public SetorHorario(Integer id, Date inicio) {
        this.id = id;
        this.inicio = inicio;
    }
    
    public SetorHorario( String inicio, String fim ) throws ParseException, HorarioException {
        this.inicio = dateFormat.parse(inicio);
        this.fim = dateFormat.parse(fim);
        
        if( this.inicio.after(this.fim) ) {
            throw new HorarioException("Fim deve ser maior que início.");
        }
    }
    
    public SetorHorario( Setor setor, String inicio, String fim ) throws ParseException, HorarioException {
        this.setor = setor;
        this.inicio = dateFormat.parse(inicio);
        this.fim = dateFormat.parse(fim);
        
        if( this.inicio.after(this.fim) ) {
            throw new HorarioException("Fim deve ser maior que início.");
        }
    }
    
    public SetorHorario( Setor setor, Date inicio, Date fim ) throws HorarioException {
        this.setor = setor;
        this.inicio = inicio;
        this.fim = fim;
        
        if( this.inicio.after(this.fim) ) {
            throw new HorarioException("Fim deve ser maior que início.");
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getInicio() {
        return inicio;
    }
    
    public long getInicioTime(){
        return new LocalTime(getInicio()).getMillisOfDay();
    }

    public void setInicio(Date inicio) throws HorarioException  {
        if( inicio.after(this.fim) ) {
            throw new HorarioException("Início deve ser menor que fim.");
        }
        
        this.inicio = inicio;
    }

    public Date getFim() {
        return fim;
    }
    
    public long getFimTime(){
        return new LocalTime(getFim()).getMillisOfDay();
    }

    public void setFim(Date fim) throws HorarioException {
        if( fim.before(this.inicio) ) {
            throw new HorarioException("Fim deve ser maior que início.");
        }
        
        this.fim = fim;
    }

    public Setor getSetor() {
        return setor;
    }

    public void setSetor(Setor setor) {
        this.setor = setor;
    }
    
    public AtendimentoPeriodoEnum getPeriodo(){
        return periodo;
    }
    
    public void setPeriodo(AtendimentoPeriodoEnum periodo){
        this.periodo = periodo;
    }
    
    @XmlTransient
    public List<Atendimento> getAtendimentos() {
        return atendimentos;
    }

    public void setAtendimentos(List<Atendimento> atendimentos) {
        this.atendimentos = atendimentos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SetorHorario)) {
            return false;
        }
        
        SetorHorario other = (SetorHorario) object;
        if (
            (this.id != null && !this.id.equals(other.id)) ||
            !(
                (this.setor.equals(other.setor)) &&
                (this.inicio.equals(other.inicio)) &&
                (this.fim.equals(other.fim))
            )
        ) {
            System.out.println("SetorHorario:equals");
            return false;
        }
        
        return true;
    }

    @Override
    public String toString() {
        return dateFormat.format(inicio) + " - " + dateFormat.format(fim);
    }

    @Override
    public int compareTo(Object o) {
        SetorHorario other = (SetorHorario) o;
        
        if( this.inicio.before(other.inicio) ) {
            return -1;
        }
        
        if( this.inicio.before(other.inicio) ) {
            return 1;
        }
        
        return 0;
    }
}
