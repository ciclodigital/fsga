/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.model;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author allan
 */
public enum AtendimentoStatusEnum {
    // O paciente agendou
    AGENDADO,
    
    // O paciente compareceu ao agendamento
    COMPARECEU,
    
    // O paciente faltou
    FALTA,
    
    // O paciente justificou a falta
    FALTA_JUSTIFICADA;

    public static List<AtendimentoStatusEnum> asList() {
        return Arrays.asList( values() );
    }
}
