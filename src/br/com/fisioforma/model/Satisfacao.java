/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.model;

import br.com.fisioforma.db.Util;
import br.com.fisioforma.model.service.SatisfacaoService;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author allan
 */
@Entity
@Table(name = "satisfacoes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Satisfacao.findAll", query = "SELECT s FROM Satisfacao s"),
    @NamedQuery(name = "Satisfacao.findById", query = "SELECT s FROM Satisfacao s WHERE s.id = :id"),
    @NamedQuery(name = "Satisfacao.findByAtendimento", query = "SELECT s FROM Satisfacao s WHERE s.atendimento = :atendimento"),
    @NamedQuery(name = "Satisfacao.findByRecepcao", query = "SELECT s FROM Satisfacao s WHERE s.recepcao = :recepcao"),
    @NamedQuery(name = "Satisfacao.groupByAtendimentoAndDate", query = "SELECT s.atendimento, count(s.atendimento) FROM Satisfacao s WHERE s.tratamento.updatedAt BETWEEN :start AND :end AND s.tratamento.paciente.empresa in (:empresas) GROUP BY s.atendimento ORDER BY s.atendimento ASC")
})
public class Satisfacao implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @NotNull(message = "A Definição da qualidade do atendimento é obrigatório.")
    @Column(name = "atendimento") //, columnDefinition = "ENUM('SUPEROU_AS_ESPECTATIVAS', 'SATISFEITO', 'PARCIALMENTE_SATISFEITO', 'INSATISFEITO')")
    @Enumerated(EnumType.STRING)
    private SatisfacaoEnum atendimento;
    
    @Basic(optional = false)
    @NotNull(message = "A Definição da qualidade da recepção é obrigatório.")
    @Column(name = "recepcao") //, columnDefinition = "ENUM('SUPEROU_AS_ESPECTATIVAS', 'SATISFEITO', 'PARCIALMENTE_SATISFEITO', 'INSATISFEITO')")
    @Enumerated(EnumType.STRING)
    private SatisfacaoEnum recepcao;
    
    @Lob
    @Column(name = "comentario")
    private String comentario;
    
    @NotNull
    @JoinColumn(name = "tratamento_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Tratamento tratamento;

    public Satisfacao() {
    }

    public Satisfacao(Integer id) {
        this.id = id;
    }

    public Satisfacao(Integer id, SatisfacaoEnum atendimento, SatisfacaoEnum recepcao, String comentario) {
        this.id = id;
        this.atendimento = atendimento;
        this.recepcao = recepcao;
        this.comentario = comentario;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SatisfacaoEnum getAtendimento() {
        return atendimento;
    }

    public void setAtendimento(SatisfacaoEnum atendimento) {
        this.atendimento = atendimento;
    }

    public SatisfacaoEnum getRecepcao() {
        return recepcao;
    }

    public void setRecepcao(SatisfacaoEnum recepcao) {
        this.recepcao = recepcao;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Tratamento getTratamento() {
        return tratamento;
    }

    public void setTratamento(Tratamento tratamento) {
        this.tratamento = tratamento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Satisfacao)) {
            return false;
        }
        Satisfacao other = (Satisfacao) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.fisioforma.model.Satisfacoes[ id=" + id + " ]";
    }
    
    public static List<Object[]> groupByAtendimentoAndDate(List<Empresa> empresas, Date start, Date end){
        List<Object[]> group = new ArrayList<Object[]>();
        
        Session session = null;
        
        try {
            session = Util.getSessionFactory().openSession();
            Query query = session.getNamedQuery("Satisfacao.groupByAtendimentoAndDate");
            
            query.setParameterList("empresas", empresas);
            query.setParameter("start", start);
            query.setParameter("end"  , end  );
            
            group = query.list();            
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        } finally {
            if( session == null ) { session.close(); } 
        }
        
        return group;
    }
    
    public boolean destroy(){
        return SatisfacaoService.getInstance().delete(this);
    }
    
}
