/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.model;

import br.com.fisioforma.db.Util;
import br.com.fisioforma.exception.HorarioException;
import br.com.fisioforma.model.service.SetorService;
import br.com.fisioforma.swing.ErrorMessage;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author allan
 */
@Entity
@Table(name = "setores")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Setor.findAll", query = "SELECT s FROM Setor s"),
    @NamedQuery(name = "Setor.findById", query = "SELECT s FROM Setor s WHERE s.id = :id"),
    @NamedQuery(name = "Setor.findByNome", query = "SELECT s FROM Setor s WHERE s.nome = :nome"),
    @NamedQuery(name = "Setor.findByNomes", query = "SELECT s FROM Setor s WHERE s.nome in (:nomes)"),
    @NamedQuery(name = "Setor.likeByNome", query = "SELECT s FROM Setor s WHERE s.nome like :nome")
})
public class Setor implements Serializable {
    private static final long serialVersionUID = 1L;
    public  static final String ALL = "Setor.findAll";
        
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 64, message = "O nome é obrigatório.")
    @Column(name = "nome", nullable = false, length = 64, unique=true)
    private String nome;
    
    @Lob
    @Size(max = 65535)
    @Column(name = "descricao", length = 65535)
    private String descricao;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "setor", fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<SetorHorario> horarios = new LinkedHashSet<SetorHorario>();
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "setor", fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<Fisioterapeuta> fisioterapeutas = new LinkedHashSet<Fisioterapeuta>();

    public Setor() {
    }

    public Setor(Integer id) {
        this.id = id;
    }

    public Setor(Integer id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @XmlTransient
    public Set<SetorHorario> getHorarios() {
        return horarios;
    }
    
    @XmlTransient
    public List<String> getHorariosAsListString(){
        List<String> list = new ArrayList<String>();
        
        for( SetorHorario sh : horarios ) list.add( sh.toString() );
        
        return list;
    }

    public void setHorarios(Set<SetorHorario> horarios) {
        this.horarios = horarios;
    }
    
    public void addHorario(SetorHorario sh ) throws HorarioException {
        for( SetorHorario h : horarios ) {
            // o inicio pode ocorrer depois do fim ou antes
            // se o inicio ocorrer antes do fim, então o fim deste deve ocorrer antes do inicio
            if( !( (h.getFimTime() < sh.getInicioTime() ) || ( h.getInicioTime() > sh.getFimTime() ) ) ) {
                throw new HorarioException("Intervalo de horário já utilizado.");
            }
        }
        
        horarios.add(sh);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Setor)) {
            return false;
        }
        Setor other = (Setor) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getNome();
    }

    @XmlTransient
    public Set<Fisioterapeuta> getFisioterapeutas() {
        return fisioterapeutas;
    }

    public void setFisioterapeutas(Set<Fisioterapeuta> fisioterapeutas) {
        this.fisioterapeutas = fisioterapeutas;
    }
    
    public long countHorarios() {
        Session session = null;
        
        try {
            session = Util.getSessionFactory().openSession();
            Query query = session.getNamedQuery("SetorHorario.countBySetor").setParameter("setor", this);
            
            return (long) query.uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        } finally {
            if( session != null ) session.close();
        }
        
        return 0;
    }

    
    public long countHorarios(AtendimentoPeriodoEnum periodo) {
        Session session = null;
        
        try {
            session = Util.getSessionFactory().openSession();
            Query query = session.getNamedQuery("SetorHorario.countBySetorAndPeriodo");
            
            query.setParameter("setor", this);
            query.setParameter("periodo", periodo);
            
            return (long) query.uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        } finally {
            if( session != null ) session.close();
        }
        
        return 0;
    }

}
