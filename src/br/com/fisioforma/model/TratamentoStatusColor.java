/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.model;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author allan
 */
public final class TratamentoStatusColor {
    
    static private Map<TratamentoStatusEnum, Color> colors = new HashMap<TratamentoStatusEnum, Color>();
    
    static {
        colors.put(TratamentoStatusEnum.ANDAMENTO, new Color(107,107,255));
        colors.put(TratamentoStatusEnum.CANCELADO, new Color(255,107,107));
        colors.put(TratamentoStatusEnum.CONCLUIDO, new Color(107,255,107));
        colors.put(TratamentoStatusEnum.DESISTENCIA, new Color(255,180,107));
        colors.put(TratamentoStatusEnum.ALTA, new Color(107,255,107));
    }
    
    public static Color getColor( TratamentoStatusEnum status ){
        return colors.get(status);
    }
    
    private TratamentoStatusColor(){};
}
