/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.model;

/**
 *
 * @author allan
 */
public enum SexoEnum {
    MASCULINO("Masculino"), FEMININO("Feminino");

    private String n;

    SexoEnum(String n)  {
        this.n = n;
    }

    @Override
    public String toString() {
        return this.n;
    }
}
