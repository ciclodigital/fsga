/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.model;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author allan
 */
public enum TratamentoStatusEnum {
    // Tratamento em andamento
    ANDAMENTO,
    
    // O paciente finalizou todos os atendimentos e teve alta
    ALTA,
    
    // Cancelado por tempo  ou pela quantidade de atendimentos muito alta
    CANCELADO,
    
    // O paciente desistiu
    DESISTENCIA,
    
    // O paciente finalizou todos os atendimentos
    CONCLUIDO;

    public static List<TratamentoStatusEnum> asList() {
        return Arrays.asList(values());
    }
}
