/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.model;

/**
 *
 * @author allan
 */
public enum SatisfacaoEnum {
    SUPEROU_AS_ESPECTATIVAS("Superou as Expectativas"),
    SATISFEITO("Satisfeito"),
    PARCIALMENTE_SATISFEITO("Parcialmente Satisfeito"),
    INSATISFEITO("Insatisfeito");

    private String n;

    SatisfacaoEnum(String n) {
        this.n = n;
    }

    @Override
    public String toString() {
        return this.n;
    }
}
