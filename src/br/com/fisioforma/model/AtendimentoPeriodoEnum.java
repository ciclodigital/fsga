/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.model;

/**
 * Periodos disponiveis para os horariodos dos setores para atendimento
 * 
 * @author allan
 */
public enum AtendimentoPeriodoEnum {
    // Atendimentos que irao ocorrer dentro do periodo da manha
    MANHA,
    
    // Atendimentos que irao ocorrer dentro do periodo da tarde
    TARDE
}
