/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.model;

import br.com.fisioforma.db.Util;
import br.com.fisioforma.model.service.RecursoService;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author allan
 */
@Entity
@Table(name = "recursos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Recurso.findAll", query = "SELECT r FROM Recurso r"),
    @NamedQuery(name = "Recurso.findById", query = "SELECT r FROM Recurso r WHERE r.id = :id"),
    @NamedQuery(name = "Recurso.findBySigla", query = "SELECT r FROM Recurso r WHERE r.sigla = :sigla"),
    @NamedQuery(name = "Recurso.likeBySigla", query = "SELECT r FROM Recurso r WHERE r.sigla like :sigla"),
    @NamedQuery(name = "Recurso.notIn", query = "SELECT r FROM Recurso r where r.id not in (:recursos)")
})
public class Recurso implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @NotEmpty(message = "A sigla é obrigatória")
    @Size(min = 1, max = 64, message = "O tramanho máximo do nome é de 64 caracteres.")
    @Column(name = "sigla", unique=true)
    private String sigla;
    
    @Lob
    @NotEmpty(message = "A descrição é obrigatória")
    @Size(max = 65535, message = "Tamanho limite estourado.")
    @Column(name = "descricao")
    private String descricao;

    public Recurso() {
    }

    public Recurso(Integer id) {
        this.id = id;
    }

    public Recurso(Integer id, String sigla) {
        this.id = id;
        this.sigla = sigla;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Recurso)) {
            return false;
        }
        Recurso other = (Recurso) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getSigla().toUpperCase();
    }
    
    public Boolean anyTratamento(){
        Session session = null;
        
        try {
            session = Util.getSessionFactory().openSession();
            
            Query query = session.createQuery("SELECT COUNT(*) from Tratamento t LEFT JOIN t.recursos r WHERE r.id = :recurso_id");
                  query.setParameter("recurso_id", getId());

            List<Object> list = query.list();
            long count = (long) list.get(0);
            return count > 0;
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        } finally {
            if (session != null) { session.close(); }
        }
        
        return false;
    }
    
    public boolean destroy(){
        return RecursoService.getInstance().delete(this);
    }
}
