/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.model;

import br.com.fisioforma.db.Util;
import br.com.fisioforma.model.service.TratamentoService;
import br.com.fisioforma.report.ReportFaturamentoDataRow;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

/**
 *
 * @author allan
 */
@Entity
@Table(name = "tratamentos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tratamento.findAll", query = "SELECT t FROM Tratamento t"),
    @NamedQuery(name = "Tratamento.findById", query = "SELECT t FROM Tratamento t WHERE t.id = :id"),
    @NamedQuery(name = "Tratamento.findByPaciente", query = "SELECT t FROM Tratamento t WHERE t.paciente = :paciente"),
    @NamedQuery(name = "Tratamento.findByNomeMedico", query = "SELECT t FROM Tratamento t WHERE t.nomeMedico = :nomeMedico"),
    @NamedQuery(name = "Tratamento.findByTiss", query = "SELECT t FROM Tratamento t WHERE t.tiss = :tiss"),
    @NamedQuery(name = "Tratamento.findByQuantidade", query = "SELECT t FROM Tratamento t WHERE t.quantidade = :quantidade"),
    @NamedQuery(name = "Tratamento.findByContinuidade", query = "SELECT t FROM Tratamento t WHERE t.status = :status"),
    @NamedQuery(name = "Tratamento.findByStatus", query = "SELECT t FROM Tratamento t WHERE t.status in (:status)"),
    @NamedQuery(name = "Tratamento.findByTipo", query = "SELECT t FROM Tratamento t WHERE t.tipo = :tipo"),
    @NamedQuery(name = "Tratamento.filtro", query = "SELECT t FROM Tratamento t WHERE t.tipo in (:tipos) and t.status in (:status) and paciente = :paciente"),
    @NamedQuery(name = "Tratamento.filtroLikeTiss", query = "SELECT t FROM Tratamento t WHERE t.tipo in (:tipos) and t.status in (:status) and paciente = :paciente and t.tiss like :tiss"),
    @NamedQuery(name = "Tratamento.filtroWithoutPaciente", query = "SELECT t FROM Tratamento t WHERE t.tipo in (:tipos) and t.status in (:status)"),
    @NamedQuery(name = "Tratamento.filtroWithoutPacienteLikeTiss", query = "SELECT t FROM Tratamento t WHERE t.tipo in (:tipos) and t.status in (:status) AND t.tiss like :tiss"),
    @NamedQuery(name = "Tratamento.reportFaturamento", query = "SELECT DISTINCT t FROM Tratamento t RIGHT JOIN t.atendimentos a WHERE t.tipo in (:tipos) AND a.status in (:statusAtendimento) AND t.paciente.empresa in (:empresas) AND t.setor in (:setores) AND a.data BETWEEN :start AND :end"),
    @NamedQuery(name = "Tratamento.CountBy", query ="SELECT COUNT(*) FROM Tratamento t WHERE t.paciente.empresa in (:empresas) AND t.status in (:status) AND t.setor in (:setores) AND t.tipo in (:tipos) AND t.updatedAt BETWEEN :start AND :end"),
    @NamedQuery(name = "Tratamento.CountGroupByRegiao", query = "SELECT r.nome, count(r.nome) FROM Tratamento t LEFT JOIN t.regioes r WHERE t.updatedAt BETWEEN :start AND :end AND t.paciente.empresa in (:empresas) GROUP BY r.nome"),
    @NamedQuery(name = "Tratamento.CountGroupByProcedimento", query = "SELECT r.nome, count(r.nome) FROM Tratamento t LEFT JOIN t.procedimentos r WHERE t.updatedAt BETWEEN :start AND :end AND t.paciente.empresa in (:empresas) GROUP BY r.nome")
})
public class Tratamento implements Serializable, Comparable<Tratamento> {
    public static final String FIND_ALL  = "Tratamento.findAll";
    public static final String FIND_BY_PACIENTE  = "Tratamento.findByPaciente";
    public static final String FILTRO = "Tratamento.filtro";
    public static final String FILTRO_WITHOUT_PACIENTE = "Tratamento.filtroWithoutPaciente";
    public static final String FILTRO_REPORT_FATURAMENTO = "Tratamento.reportFaturamento";
    public static final TratamentoStatusEnum[] STATUS_ALTA = { TratamentoStatusEnum.ALTA };
    public static final TratamentoStatusEnum[] STATUS_CONCLUIDOS = { TratamentoStatusEnum.CONCLUIDO, TratamentoStatusEnum.ALTA };
    public static final TratamentoStatusEnum[] STATUS_ANDAMENTO  = { TratamentoStatusEnum.ANDAMENTO, TratamentoStatusEnum.CANCELADO, TratamentoStatusEnum.DESISTENCIA };
    private static final long serialVersionUID = 1L;
    private static final String COUNT_BY = "Tratamento.CountBy";
    
    public static List<ReportFaturamentoDataRow> convertToReportFaturamentoDataRow( List<Tratamento> tratamentos, List<AtendimentoStatusEnum> atendimentoStatus ){
        List<ReportFaturamentoDataRow> rows = new ArrayList<ReportFaturamentoDataRow>();
        
        for( Tratamento tratamento : tratamentos ) {
            rows.addAll(tratamento.toReportFaturamentoDataRows(atendimentoStatus));
        }
        
        return rows;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @NotEmpty(message = "O nome do médico é obrigatório")
    @Size(max = 128)
    @Column(name = "nome_medico")
    private String nomeMedico;
    
    @Basic(optional = false)
    @NotEmpty(message = "O diagnóstico é  obrigatório")
    @Lob
    @Size(max = 65535)
    @Column(name = "diagnostico")
    private String diagnostico;
    
    @Size(max = 20)
    @Column(name = "tiss")
    @NotEmpty(message = "O Número da guia é obrigatório")
    private String tiss;
    
    @Basic(optional = false)
    @NotNull(message = "A quantidade é obrigatória")
    @Column(name = "quantidade")
    private int quantidade;
    
    @Basic(optional = false)
    @NotNull(message = "Deve ser selecionado o status do tratamento")
    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private TratamentoStatusEnum status = TratamentoStatusEnum.ANDAMENTO;
    
    @Lob
    @Size(max = 65535)
    @Column(name = "comentario")
    private String comentario;
    
    @Lob
    @Size(max = 65535)
    @Column(name = "observacao")
    private String observacao;
    
    @Basic(optional = false)
    @NotNull(message = "Deve ser definido o tipo de tratamento (ROTINA ou Emergencial).")
    @Column(name = "tipo") //, columnDefinition = "enum('ROTINA','EMERGENCIAL')")
    @Enumerated(EnumType.STRING)
    private TratamentoTipoEnum tipo = TratamentoTipoEnum.ROTINA;
    
    @Size(min = 1, message = "É necessário ao menos um recurso.")
    @JoinTable(name = "tratamento_recursos", joinColumns = {
        @JoinColumn(name = "tratamento_id", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "recurso_id", referencedColumnName = "id")})
    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    private Set<Recurso> recursos = new LinkedHashSet<Recurso>();
    
    @Size(min = 1, message = "É necessário ao menos um procedimento.")
    @JoinTable(name = "tratamento_procedimentos", joinColumns = {
        @JoinColumn(name = "tratamento_id", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "procedimento_id", referencedColumnName = "id")})
    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    private Set<Procedimento> procedimentos = new LinkedHashSet<Procedimento>();
    
    @Size(min = 1, message = "É necessário ao menos uma região.")
    @JoinTable(name = "tratamento_regioes", joinColumns = {
        @JoinColumn(name = "tratamento_id", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "regiao_id", referencedColumnName = "id")})
    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    private Set<Regiao> regioes = new LinkedHashSet<Regiao>();
    
    @NotNull(message = "O paciente é obrigatório")
    @JoinColumn(name = "paciente_id", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    private Paciente paciente;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tratamento", fetch = FetchType.EAGER)
    private Set<Atendimento> atendimentos = new LinkedHashSet<Atendimento>();
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tratamento", fetch = FetchType.EAGER)
    private Set<Satisfacao> satisfacoes = new LinkedHashSet<Satisfacao>();
    
    @NotNull(message = "O setor é obrigatório")
    @JoinColumn(name = "setor_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Setor setor;
    
    @Column(name="created_at")
    @Temporal(TemporalType.TIMESTAMP)
    @NotNull
    private Date createdAt = new Date();
    
    @Column(name="updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    @NotNull
    private Date updatedAt = new Date();
    
    @JoinColumn(name = "fisioterapeuta_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Fisioterapeuta fisioterapeuta;
    
    @Column(name="status_alterado_em")
    @Temporal(TemporalType.TIMESTAMP)
    private Date statusAlteradoEm = null;
    
    /**
     * O atributo inicio é utilizado para definir o dia inicial do período podem ocorrer os atendimentos.
     */
    @Column(name="inicio")
    @Temporal(TemporalType.DATE)
    @NotNull(message = "O campo de data inico não pode finar vazio")
    private Date inicio = LocalDate.now().toDate();
    
    /**
     * O atributo fim é utilizado para definir o dia final do período podem ocorrer os atendimentos.
     */
    @Column(name="fim")
    @Temporal(TemporalType.DATE)
    @NotNull(message = "O campo de data final não pode finar vazio")
    private Date fim = LocalDate.now().plusMonths(1).toDate();

    public Tratamento() {
    }

    public Tratamento(Integer id) {
        this.id = id;
    }

    public Tratamento(Integer id, String nomeMedico, String diagnostico, int quantidade, TratamentoStatusEnum status, TratamentoTipoEnum tipo) {
        this.id = id;
        this.nomeMedico = nomeMedico;
        this.diagnostico = diagnostico;
        this.quantidade = quantidade;
        this.status = status;
        this.tipo = tipo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNomeMedico() {
        return nomeMedico;
    }

    public void setNomeMedico(String nomeMedico) {
        this.nomeMedico = nomeMedico;
    }

    public String getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }

    public String getTiss() {
        return tiss;
    }

    public void setTiss(String tiss) {
        this.tiss = tiss;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public TratamentoStatusEnum getStatus() {
        return status;
    }
    
    public Date getStatusAlteradoEm(){
        return statusAlteradoEm;
    }

    public void setStatus(TratamentoStatusEnum status) {
        if( status != this.status ) {
            statusAlteradoEm = LocalDateTime.now(Util.timezone).toDate();
        }
        
        this.status = status;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }
    
    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String comentario) {
        this.observacao = observacao;
    }

    public TratamentoTipoEnum getTipo() {
        return tipo;
    }

    public void setTipo(TratamentoTipoEnum tipo) {
        if( tipo == TratamentoTipoEnum.EMERGENCIAL && tiss == null ) {
        }
        this.tipo = tipo;
    }
    
    public static Tratamento buildEmergencia(Paciente paciente){
        Tratamento tratamento = new Tratamento();
        tratamento.setPaciente(paciente);
        tratamento.setTipo(TratamentoTipoEnum.EMERGENCIAL);
        tratamento.setTiss( "EMGA_" + LocalDate.now().toString() + "_" + paciente.getId() );
        
        return tratamento;
    }

    @XmlTransient
    public Set<Recurso> getRecursos() {
        return recursos;
    }

    public void setRecursos(Set<Recurso> recursos) {
        this.recursos = recursos;
    }

    @XmlTransient
    public Set<Procedimento> getProcedimentos() {
        return procedimentos;
    }

    public void setProcedimentos(Set<Procedimento> procedimentos) {
        this.procedimentos = procedimentos;
    }

    @XmlTransient
    public Set<Regiao> getRegioes() {
        return regioes;
    }

    public void setRegioes(Set<Regiao> regioes) {
        this.regioes = regioes;
    }
    
    @Transient
    public List<Integer> getRegioesIds(){
        List<Integer> ids = new ArrayList<Integer>();
        
        for( Regiao regiao : getRegioes() ) {
            ids.add( regiao.getId() );
        }
        
        return ids;
    }
    
    @Transient
    public List<Integer> getRecursosIds(){
        List<Integer> ids = new ArrayList<Integer>();
        
        for( Recurso recuro : getRecursos() ) {
            ids.add( recuro.getId() );
        }
        
        return ids;
    }
    
    @Transient
    public List<Integer> getProcedimentosIds(){
        List<Integer> ids = new ArrayList<Integer>();
        
        for( Procedimento procedimento : getProcedimentos() ) {
            ids.add( procedimento.getId() );
        }
        
        return ids;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    @XmlTransient
    public Set<Atendimento> getAtendimentos() {
        return atendimentos;
    }

    public void setAtendimentos(Set<Atendimento> atendimentos) {
        this.atendimentos = atendimentos;
    }

    @XmlTransient
    public Set<Satisfacao> getSatisfacoes() {
        return satisfacoes;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tratamento)) {
            return false;
        }
        Tratamento other = (Tratamento) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.fisioforma.model.Tratamento[ id=" + id + " ]";
    }

    public Setor getSetor() {
        return setor;
    }

    public void setSetor(Setor setor) {
        this.setor = setor;
    }
    
    public Date getCreatedAt(){
        return createdAt;
    }
    
    public Date getUpdatedAt(){
        return updatedAt;
    }
    
    @PreUpdate
    public void setLastUpdate() {
        this.updatedAt = new Date();
    }
    
    @Transient
    public int atendimentosDisponiveis() {
        return quantidade - countAtendimentosAtendimentosEPendentes(); 
    }
    
    @Transient
    private int countAtendimentosAtendimentosEPendentes() {
        Session session = null;
        
        try {
            session = Util.getSessionFactory().openSession();
            Query query = session.getNamedQuery(Atendimento.COUNT_ATENDIMENTO_BY_TRATAMENTO_STATUS).setParameter("tratamento", this);
                  query.setParameterList("status", Atendimento.STATUS_ATENDIMENTO);
            
            return (int) (long) query.uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        } finally {
            if( session != null ) session.close();
        }
        
        return -1;
    }

    @Override
    public int compareTo(Tratamento o) {
        Tratamento other = (Tratamento) o;
        
        if( this.id > other.id ) {
            return 1;
        }
        
        if( this.id < other.id ) {
            return -1;
        }
        
        return 0;
    }

    /**
     * @return the fisioterapeuta
     */
    public Fisioterapeuta getFisioterapeuta() {
        return fisioterapeuta;
    }

    /**
     * @param fisioterapeuta the fisioterapeuta to set
     */
    public void setFisioterapeuta(Fisioterapeuta fisioterapeuta) {
        this.fisioterapeuta = fisioterapeuta;
    }

    /**
     * @return the inicio
     */
    public Date getInicio() {
        return inicio;
    }

    /**
     * @param inicio the inicio to set
     */
    public void setInicio(Date inicio) {
        this.inicio = inicio;
    }

    /**
     * @return the fim
     */
    public Date getFim() {
        return fim;
    }

    /**
     * @param fim the fim to set
     */
    public void setFim(Date fim) {
        this.fim = fim;
    }
    
    @Transient
    public List<Atendimento> atendimentosFeitos() {
        Session session = null;
        List<Atendimento> result = new ArrayList<Atendimento>();
        
        try {
            session = Util.getSessionFactory().openSession();
            
            Query query = session.getNamedQuery(Atendimento.FIND_ATENDIMENTO_BY_TRATAMENTO_STATUS);
            
            query.setParameter("tratamento", this);
            query.setParameterList("status", new AtendimentoStatusEnum[] {AtendimentoStatusEnum.COMPARECEU} );
            
            result = (List<Atendimento>) query.list();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        
        return result;
    }
    
    @Transient
    public long countAtendimentosFeitos() {
        Session session = null;
        long result = 0;
        
        try {
            session = Util.getSessionFactory().openSession();
            
            Query query = session.getNamedQuery(Atendimento.COUNT_ATENDIMENTO_BY_TRATAMENTO_STATUS);
            
            query.setParameter("tratamento", this);
            query.setParameterList("status", new AtendimentoStatusEnum[] {AtendimentoStatusEnum.COMPARECEU} );
            
            result = (long) query.uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        
        return result;
    }
    
    public long countAtendimentosByStatus(List<AtendimentoStatusEnum> status){
        Session session = null;
        long result = 0;
        
        try {
            session = Util.getSessionFactory().openSession();
            
            Query query = session.getNamedQuery(Atendimento.COUNT_ATENDIMENTO_BY_TRATAMENTO_STATUS);
            
            query.setParameter("tratamento", this);
            query.setParameterList("status", status);
            
            result = (long) query.uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        
        return result;
    }
    
    public long countAtendimentosFeitosAgendados(){
        List<AtendimentoStatusEnum> atendimentoStatus = new ArrayList();
        
        atendimentoStatus.add(AtendimentoStatusEnum.AGENDADO);
        atendimentoStatus.add(AtendimentoStatusEnum.COMPARECEU);
        
        return countAtendimentosByStatus(atendimentoStatus);
    }
    
    public long countAtendimentosAgendados(){
        List<AtendimentoStatusEnum> atendimentoStatus = new ArrayList();
        
        atendimentoStatus.add(AtendimentoStatusEnum.AGENDADO);
        
        return countAtendimentosByStatus(atendimentoStatus);
    }
    
    public long countAtendimentosFaltas(){
        List<AtendimentoStatusEnum> atendimentoStatus = new ArrayList();
        
        atendimentoStatus.add(AtendimentoStatusEnum.FALTA);
        
        return countAtendimentosByStatus(atendimentoStatus);
    }
    
    public long countAtendimentosFaltaJustificada(){
        List<AtendimentoStatusEnum> atendimentoStatus = new ArrayList();
        
        atendimentoStatus.add(AtendimentoStatusEnum.FALTA_JUSTIFICADA);
        
        return countAtendimentosByStatus(atendimentoStatus);
    }
    
    public long countAtendimentosPendentes(){
        return this.quantidade - countAtendimentosFeitosAgendados();
    }
    
    public boolean temEspacoParaAtendimentos(){
        return countAtendimentosFeitosAgendados() < this.quantidade;
    }

    @Transient
    public BigDecimal getValorPorAtendimento() {
        BigDecimal total = new BigDecimal(0.0);
        
        for( Procedimento procedimento : procedimentos ) {
            total = total.add(procedimento.getValor());
        }
        
        return total;
    }

    private List<ReportFaturamentoDataRow> toReportFaturamentoDataRows(List<AtendimentoStatusEnum> atendimentoStatus) {
        List<ReportFaturamentoDataRow> rows = new ArrayList<ReportFaturamentoDataRow>();
        
        for( Procedimento procedimento : procedimentos ) {
            ReportFaturamentoDataRow row = new ReportFaturamentoDataRow();
            
            row.atendimentos = countAtendimentosByStatus(atendimentoStatus);
            row.ciefas = procedimento.getCiefas() + "";
            row.descricao = getDiagnostico();
            row.nome = getPaciente().getNome();
            row.tiss = getTiss();
            row.valor = procedimento.getValor().doubleValue();
            row.total = procedimento.getValor().doubleValue() * row.atendimentos;
            
            rows.add(row);
        }
        
        return rows;
    }

    public String toToolTip() {
        return "TISS: " + tiss + " - Descrição: " + diagnostico;
    }
    
    public static long countBy(
       List<Empresa> empresas,
       List<TratamentoTipoEnum> tipos,
       List<TratamentoStatusEnum> status,
       List<Setor> setores,
       Date start,
       Date end
    ){
        long count = 0;
        
        Session session = null;
        
        try {
            session = Util.getSessionFactory().openSession();
            Query query = session.getNamedQuery(Tratamento.COUNT_BY);
            
            query.setParameterList("empresas", empresas);
            query.setParameterList("status", status);
            query.setParameterList("setores", setores);
            query.setParameterList("tipos", tipos);
            query.setParameter("start", start);
            query.setParameter("end"  , end  );
            
            List<Object> list = query.list();
            
            count = (long) list.get(0);
            
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        } finally {
            if( session == null ) { session.close(); } 
        }
        
        return count;
    }

    public static List<Object[]> countGroupByRegiao(
       List<Empresa> empresas,
       Date start,
       Date end
    ){
        List<Object[]> group = new ArrayList<Object[]>();
        
        Session session = null;
        
        try {
            session = Util.getSessionFactory().openSession();
            Query query = session.getNamedQuery("Tratamento.CountGroupByRegiao");
            
            query.setParameterList("empresas", empresas);
            query.setParameter("start", start);
            query.setParameter("end"  , end  );
            
            group = query.list();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        } finally {
            if( session == null ) { session.close(); } 
        }
        
        return group;
    }
    
    public static List<Object[]> countGroupByProcedimento(
       List<Empresa> empresas,
       Date start,
       Date end
    ){
        List<Object[]> group = new ArrayList<Object[]>();
        
        Session session = null;
        
        try {
            session = Util.getSessionFactory().openSession();
            Query query = session.getNamedQuery("Tratamento.CountGroupByProcedimento");
            
            query.setParameterList("empresas", empresas);
            query.setParameter("start", start);
            query.setParameter("end"  , end  );
            
            group = query.list();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        } finally {
            if( session == null ) { session.close(); } 
        }
        
        return group;
    }
    
    public boolean destroy(){
        return TratamentoService.getInstance().delete(this);
    }
}
