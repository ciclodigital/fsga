/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.model;

import br.com.fisioforma.model.service.PacienteService;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author allan
 */
@Entity
@Table(name = "pacientes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Paciente.findAll", query = "SELECT p FROM Paciente p"),
    @NamedQuery(name = "Paciente.findById", query = "SELECT p FROM Paciente p WHERE p.id = :id"),
    @NamedQuery(name = "Paciente.findByNome", query = "SELECT p FROM Paciente p WHERE p.nome = :nome"),
    @NamedQuery(name = "Paciente.findByCpf", query = "SELECT p FROM Paciente p WHERE p.cpf = :cpf"),
    @NamedQuery(name = "Paciente.findByDataNascimento", query = "SELECT p FROM Paciente p WHERE p.dataNascimento = :dataNascimento"),
    @NamedQuery(name = "Paciente.findByDepartamento", query = "SELECT p FROM Paciente p WHERE p.departamento = :departamento"),
    @NamedQuery(name = "Paciente.findByCargo", query = "SELECT p FROM Paciente p WHERE p.cargo = :cargo"),
    @NamedQuery(name = "Paciente.findByMatricula", query = "SELECT p FROM Paciente p WHERE p.matricula = :matricula"),
    @NamedQuery(name = "Paciente.findByRamal", query = "SELECT p FROM Paciente p WHERE p.ramal = :ramal"),
    @NamedQuery(name = "Paciente.findByTelefone", query = "SELECT p FROM Paciente p WHERE p.telefone = :telefone"),
    @NamedQuery(name = "Paciente.findByEmail", query = "SELECT p FROM Paciente p WHERE p.email = :email"),
    @NamedQuery(name = "Paciente.findBySexo", query = "SELECT p FROM Paciente p WHERE p.sexo = :sexo"),
    @NamedQuery(name = "Paciente.likeByNome", query = "SELECT p FROM Paciente p WHERE p.nome like :nome"),
    @NamedQuery(name = "Paciente.pesquisa", query = "SELECT p FROM Paciente p WHERE p.nome like :pesquisa or p.cpf like :pesquisa or p.email like :pesquisa or p.matricula like :pesquisa"),
    @NamedQuery(name = "Paciente.pesquisaWithEmpresa", query = "SELECT p FROM Paciente p WHERE ( p.nome like :pesquisa or p.cpf like :pesquisa or p.email like :pesquisa or p.matricula like :pesquisa ) and p.empresa in (:empresas) ")
})
public class Paciente implements Serializable {
    private static final long serialVersionUID = 1L;
    
    public static final String FIND_ALL = "Paciente.findAll";
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @NotEmpty(message = "O Nome é obrigatório.")
    @Size(min=3, max = 128, message = "O Nome é inválido.")
    @Column(name = "nome")
    private String nome;
    
    @Basic(optional = true)
    @Column(name = "cpf")
    private String cpf;
    
    @Basic(optional = false)
    @NotNull(message = "A data de nascimento é obrigatória.")
    @Column(name = "data_nascimento")
    @Temporal(TemporalType.DATE)
    private Date dataNascimento;
    
    @Basic(optional = false)
    @NotEmpty(message = "O departamento é obrigatório.")
    @Size(min = 2, max = 64, message = "Nome de departamento inválido.")
    @Column(name = "departamento")
    private String departamento;
    
    @Basic(optional = false)
    @NotEmpty(message = "O Cargo é obrigatório.")
    @Size(min = 3, max = 64, message = "Nome de cargo inválido.")
    @Column(name = "cargo")
    private String cargo;
    
    @Basic(optional = false)
    @NotEmpty(message = "A matrícula é obrigatória.")
    @Size(max = 32)
    @Column(name = "matricula")
    private String matricula;
    
    @Column(name = "ramal")
    private Integer ramal;
    
    @Column(name = "telefone")
    private Integer telefone;
    
    @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="E-mail inválido")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 128)
    @Column(name = "email")
    private String email;

    @Basic(optional = false)
    @NotNull(message = "O sexo e obrigatorio.")
    @Column(name = "sexo")
    @Enumerated(EnumType.STRING)
    private SexoEnum sexo;
    
    @NotNull(message = "A empresa e obrigatoria")
    @JoinColumn(name = "empresa_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    private Empresa empresa;
    
    @OneToMany(cascade = CascadeType.PERSIST, mappedBy = "paciente",  fetch = FetchType.EAGER)
    private Set<Tratamento> tratamentos = new LinkedHashSet<Tratamento>();

    public Paciente() {
    }

    public Paciente(Integer id) {
        this.id = id;
    }

    public Paciente(Integer id, String nome, String cpf, Date dataNascimento, String departamento, String cargo, String matricula, SexoEnum sexo) {
        this.id = id;
        this.nome = nome;
        this.cpf = cpf;
        this.dataNascimento = dataNascimento;
        this.departamento = departamento;
        this.cargo = cargo;
        this.matricula = matricula;
        this.sexo = sexo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf.replaceAll("[\\s()-]", "");
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public Integer getRamal() {
        return ramal;
    }

    public void setRamal(Integer ramal) {
        this.ramal = ramal;
    }

    public Integer getTelefone() {
        return telefone;
    }

    public void setTelefone(Integer telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public SexoEnum getSexo() {
        return sexo;
    }

    public void setSexo(SexoEnum sexo) {
        this.sexo = sexo;
    }
    
    public void setSexo(String sexo) throws Exception{
        try {
            this.sexo = SexoEnum.valueOf(sexo.toUpperCase());
        } catch ( Exception e ) {
            throw new Exception("Sexo inválido.");
        }
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }
    
    public boolean destroy(){
        boolean result = true;
        
        for( Tratamento tratamento : getTratamentos() ) {
            result = result && tratamento.destroy();
        }
        
        return PacienteService.getInstance().delete(this);// && result;
    }

    @XmlTransient
    public Set<Tratamento> getTratamentos() {
        return tratamentos;
    }

    public void setTratamentos(Set<Tratamento> tratamentos) {
        this.tratamentos = tratamentos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Paciente)) {
            return false;
        }
        Paciente other = (Paciente) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.fisioforma.model.Paciente[ id=" + id + " ]";
    }
    
}
