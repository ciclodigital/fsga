/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.swing.calendar;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JLabel;
import org.joda.time.LocalDate;

/**
 *
 * @author allan
 */
public class DatePicker extends javax.swing.JPanel {
    private LocalDate current = new LocalDate();
    public final static String[] diasDaSemana = { "Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb" };
    public final static String[] meses        = {"Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"};
    private List<LocalDate> datesSelected = new ArrayList<LocalDate>();
    private AbstractStyleDatepicker styleHeader = new StyleDatePickerHeader();
    private AbstractStyleDatepicker styleValid;
    private AbstractStyleDatepicker styleInvalid;
    private boolean updateFromView = true;
    private int maxSelectedDates = -1;
    private List<AbstractSelectedChangeListener> selectedListenings = new ArrayList<AbstractSelectedChangeListener>();
    private List<AbstractDatePickerDateValidate> validateDates = new ArrayList<AbstractDatePickerDateValidate>();
    private List<AbstractDatePickerDateValidate> currentDateListenings = new ArrayList<AbstractDatePickerDateValidate>();
    
    // this must define max os dates are possible to selected
    private int limitOfSelection = 1;
    private LocalDate minDate = null;
    private LocalDate maxDate = null;
 
    public DatePicker() {
        initComponents();
        calendario.setLayout(new java.awt.GridLayout(7,7));
        
        updateView();
        restarLayout();
    }
    
    public DatePicker( LocalDate current ){
        this.current = current;
        initComponents();
        calendario.setLayout(new java.awt.GridLayout(7,7));
        
        updateView();
        restarLayout();
    }
    
    public DatePicker( LocalDate current, int maxSelectedDates ){
        this.current = current;
        this.maxSelectedDates = maxSelectedDates;
        initComponents();
        calendario.setLayout(new java.awt.GridLayout(7,7));
        
        updateView();
        restarLayout();
    }
    
    public DatePicker(int maxSelectedDates ){
        this.maxSelectedDates = maxSelectedDates;
        initComponents();
        calendario.setLayout(new java.awt.GridLayout(7,7));
        
        updateView();
        restarLayout();
    }
    
    public void addSelectedListenings(AbstractSelectedChangeListener evt){
        if( !selectedListenings.contains(evt) ) selectedListenings.add(evt); 
    }
    
    private void triggerSelectedListenings(DatePickerEventType type, Date selected){
        for( AbstractSelectedChangeListener evt : selectedListenings ) {
            evt.changeSelectedDates(type, this, selected);
        }
    }
    
    private void triggerChangeCurrentListenings(DatePickerEventType type, Date current){
        for( AbstractSelectedChangeListener evt : selectedListenings ) {
            evt.changeCurrentDate(type, this, current);
        }
    }
    
    public int getMaxSelectedDates(){
        return maxSelectedDates;
    }
            
    public void restarLayout(){
        calendario.removeAll();
        criarCabecalho();
        createDays();
        calendario.doLayout();
    }
    
    private void setCurrent(LocalDate current){
        this.current = current;
        triggerChangeCurrentListenings(DatePickerEventType.CHANGE_CURRENT, current.toDate());
    }

    public void setStyleHeader(AbstractStyleDatepicker styleHeader){
        this.styleHeader = styleHeader;
        restarLayout();
    }
    
    public void setStyleValid(AbstractStyleDatepicker styleValid){
        this.styleValid = styleValid;
        restarLayout();
    }
    
    public void setStyleInvalid(AbstractStyleDatepicker styleInvalid){
        this.styleInvalid = styleInvalid;
        restarLayout();
    }
    
    public void setLimitOfSelection(int limitOfSelection) {
        this.limitOfSelection = limitOfSelection;
    }
    
    public int getLimitOfSelection(){
        return limitOfSelection;
    }
    
    private boolean isValidLocalDate(LocalDate day, LocalDate current){
        int dayOfWeek = day.getDayOfWeek();
        boolean isValidate = dayOfWeek > 0 && day.getDayOfWeek() < 6;
        
        if( isValidate ) {
            if( minDate != null && maxDate != null ) { isValidate = day.compareTo(minDate) >= 0 && day.compareTo(maxDate) <= 0; }
            if( minDate == null && maxDate != null ) { isValidate = day.compareTo(maxDate) <= 0; }
            if( minDate != null && maxDate == null ) { isValidate = day.compareTo(minDate) >= 0; }
            
            if( isValidate ) {
                for( AbstractDatePickerDateValidate validate : validateDates ) {
                    isValidate = isValidate && validate.validate(day, current);
                }
            }
        }
        
        return isValidate;
    }
    
    public void addValidateDates(AbstractDatePickerDateValidate validateDates) {
        this.validateDates.add(validateDates);
        restarLayout();
    }
    
    private void createDays(){

        for( Integer count = 1; count < 43; count++ ) {
            Integer dayOfWeek = getFirstDay().getDayOfWeek();
            Integer lastDayOfMonth = getLastDay().getDayOfMonth();
            
            if( count > dayOfWeek && ( lastDayOfMonth + dayOfWeek ) >= count ) {
                Integer currentDay = count - dayOfWeek;
                JToggleButtonDatePickerDay btn = new JToggleButtonDatePickerDay();
                btn.setDate(new LocalDate(current.getYear(), current.getMonthOfYear(), currentDay));
                btn.setText(currentDay.toString());
                
                if( !isValidLocalDate( btn.getDate(), current ) ) {
                    btn.setEnabled(false);
                } else {
                    btn.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mouseReleased(MouseEvent e) {
                            if( btn.isSelected() ) {
                                if( !getDatesSelected().contains(btn.getDate()) ) {
                                    if( getMaxSelectedDates() < 0 || getDatesSelected().size() < getMaxSelectedDates() ) {
                                        getDatesSelected().add(btn.getDate());
                                        triggerSelectedListenings(DatePickerEventType.ADD, btn.getDate().toDate());
                                    } else {
                                        btn.setSelected(false);
                                    }
                                }
                            } else {
                                if( getDatesSelected().contains(btn.getDate()) ) {
                                    getDatesSelected().remove(btn.getDate());
                                    triggerSelectedListenings(DatePickerEventType.REMOVE, btn.getDate().toDate());
                                }
                            }
                        }
                    });
                    
                    if( getDatesSelected().contains(btn.getDate()) ) {
                        btn.setSelected(true);
                    }
                }
                
                calendario.add(btn);
            } else {
                calendario.add(new JLabel());
            }
        }
    }
        
    private void criarCabecalho(){
        // criação do header
        for( String diaDaSemana : DatePicker.diasDaSemana  ){
            JLabel label = new JLabel(diaDaSemana);
            
            styleHeader.setStyle(label);
            
            calendario.add(label);
        }
    }
    
    public LocalDate getLastDay(){
        return getFirstDay().plusMonths(1).minusDays(1);
    }
    
    public LocalDate getFirstDay(){
        return current.withDayOfMonth(1);
    }
    
    public List<LocalDate> getDatesSelected() {
        return datesSelected;
    }
    
    public void setDatesSelected(List<LocalDate> datesSelected) {
        this.datesSelected = datesSelected;
        restarLayout();
    }
    
    private void updateDate(){
        setCurrent(
            new LocalDate(
                (int) years.getModel().getValue(),
                comboMeses.getSelectedIndex() + 1,
                1
            )
        );
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        calendario = new javax.swing.JPanel();
        prev = new javax.swing.JButton();
        next = new javax.swing.JButton();
        comboMeses = new javax.swing.JComboBox();
        years = new javax.swing.JSpinner();

        setBorder(javax.swing.BorderFactory.createEtchedBorder());

        calendario.setBackground(new java.awt.Color(254, 254, 254));
        calendario.setBorder(null);

        javax.swing.GroupLayout calendarioLayout = new javax.swing.GroupLayout(calendario);
        calendario.setLayout(calendarioLayout);
        calendarioLayout.setHorizontalGroup(
            calendarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 491, Short.MAX_VALUE)
        );
        calendarioLayout.setVerticalGroup(
            calendarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 230, Short.MAX_VALUE)
        );

        prev.setText("<");
        prev.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                prevActionPerformed(evt);
            }
        });

        next.setText(">");
        next.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nextActionPerformed(evt);
            }
        });

        comboMeses.setModel(new javax.swing.DefaultComboBoxModel(
            DatePicker.meses
        ));
        comboMeses.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                comboMesesItemStateChanged(evt);
            }
        });
        comboMeses.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                comboMesesMouseReleased(evt);
            }
        });
        comboMeses.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboMesesActionPerformed(evt);
            }
        });

        years.setValue( current.getYear() );
        years.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                yearsStateChanged(evt);
            }
        });
        years.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                yearsMouseReleased(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(calendario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(prev, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(years, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(comboMeses, 0, 259, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(next, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(comboMeses, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(prev)
                        .addComponent(next)
                        .addComponent(years, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(calendario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void updateView(){
        years.setValue( current.getYear() );
        comboMeses.setSelectedIndex(
            current.getMonthOfYear() - 1
        );
    }
    
    private void yearsStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_yearsStateChanged
        if( updateFromView ) {
            updateDate();
            restarLayout();
        }
        
    }//GEN-LAST:event_yearsStateChanged

    private void comboMesesItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_comboMesesItemStateChanged
//        updateDate();
    }//GEN-LAST:event_comboMesesItemStateChanged

    private void comboMesesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboMesesActionPerformed
        if( updateFromView ) {
            updateDate();
            restarLayout();
        }
    }//GEN-LAST:event_comboMesesActionPerformed

    private void nextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nextActionPerformed
        updateFromView = false;
        if( current.getMonthOfYear() == 12 ) {
            LocalDate newDate = new LocalDate(current.plusYears(1).getYear(), 1, 1);
            setCurrent(newDate);
        } else {
            setCurrent(current.plusMonths(1));
        }
        updateView();
        restarLayout();
        updateFromView = true;
    }//GEN-LAST:event_nextActionPerformed

    private void prevActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_prevActionPerformed
        updateFromView = false;
        if( current.getMonthOfYear() == 1 ) {
            setCurrent(
                new LocalDate(current.minusYears(1).getYear(), 12, 1)
            );
        } else {
            setCurrent(current.minusMonths(1));
        }
        updateView();
        restarLayout();
        updateFromView = true;
    }//GEN-LAST:event_prevActionPerformed

    private void yearsMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_yearsMouseReleased

    }//GEN-LAST:event_yearsMouseReleased

    private void comboMesesMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_comboMesesMouseReleased
        
    }//GEN-LAST:event_comboMesesMouseReleased


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel calendario;
    private javax.swing.JComboBox comboMeses;
    private javax.swing.JButton next;
    private javax.swing.JButton prev;
    private javax.swing.JSpinner years;
    // End of variables declaration//GEN-END:variables

    public void removeDate(Date date) {
        datesSelected.remove( new LocalDate(date) );
        
        restarLayout();
    }

    public void reset() {
        datesSelected.clear();
        
        restarLayout();
    }

    public void setMinDate(LocalDate minDate) {
        this.minDate = minDate;
        
        restarLayout();
    }

    public LocalDate getCurrentDate() {
        return current;
    }

    public void setMaxDate(LocalDate maxDate) {
        this.maxDate = maxDate;
        
        restarLayout();
    }

}


class DatePickerMouseListener extends MouseAdapter {
    private JToggleButtonDatePickerDay button;
    private final DatePicker datePicker;
    
    public DatePickerMouseListener(JToggleButtonDatePickerDay button, DatePicker datePicker) {
        this.button = button;
        this.datePicker = datePicker;
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if( button.isSelected() ) {
            if( !datePicker.getDatesSelected().contains(button.getDate()) ) {
                if( datePicker.getMaxSelectedDates() < 0 || datePicker.getDatesSelected().size() < datePicker.getMaxSelectedDates() ) {
                    datePicker.getDatesSelected().add(button.getDate());
                } else {
                    button.setSelected(false);
                }
            }
        } else {
            if( datePicker.getDatesSelected().contains(button.getDate()) ) {
                datePicker.getDatesSelected().remove(button.getDate());
            }
        }
    }
}