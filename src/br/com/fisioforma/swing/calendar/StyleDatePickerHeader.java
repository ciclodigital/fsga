/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.swing.calendar;

import javax.swing.JComponent;
import java.awt.Color;
import javax.swing.JLabel;

/**
 *
 * @author allan
 */
class StyleDatePickerHeader extends AbstractStyleDatepicker {

    public StyleDatePickerHeader() {}

    @Override
    public void setStyle(JComponent component) {
        JLabel label = (JLabel) component;
        
        label.setBackground(Color.white);
        label.setVerticalAlignment(JLabel.CENTER);
        label.setHorizontalAlignment(JLabel.CENTER);
    }
}
