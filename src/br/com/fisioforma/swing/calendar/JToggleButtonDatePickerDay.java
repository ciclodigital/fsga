/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.swing.calendar;

import javax.swing.JToggleButton;
import org.joda.time.LocalDate;

/**
 *
 * @author allan
 */
public class JToggleButtonDatePickerDay extends JToggleButton {
    private LocalDate currentDate;
    
    public void setDate(LocalDate currentDate){
        this.currentDate = currentDate;
    }
    
    public LocalDate getDate(){
        return currentDate;
    }
}
