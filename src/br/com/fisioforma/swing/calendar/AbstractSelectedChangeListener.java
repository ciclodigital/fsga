/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.swing.calendar;

import java.util.Date;
import java.util.EventListener;

/**
 *
 * @author allan
 */
public abstract class AbstractSelectedChangeListener implements EventListener  {
    public abstract void changeSelectedDates(DatePickerEventType evt, DatePicker datePicker, Date selected);
    public abstract void changeCurrentDate(DatePickerEventType evt, DatePicker datePicker, Date current);
}
