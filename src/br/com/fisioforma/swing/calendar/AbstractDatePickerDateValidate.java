/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.swing.calendar;

import org.joda.time.LocalDate;

/**
 *
 * @author allan
 */
public abstract class AbstractDatePickerDateValidate {
    public abstract boolean validate(LocalDate day, LocalDate current);
}
