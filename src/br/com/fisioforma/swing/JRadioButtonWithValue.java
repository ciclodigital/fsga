/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.swing;

import javax.swing.JRadioButton;

/**
 *
 * @author allan
 */
public class JRadioButtonWithValue extends JRadioButton {
    private Object value = null;

    /**
     * @return the value
     */
    public Object getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(Object value) {
        this.value = value;
    }
}
