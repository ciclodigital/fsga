/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.swing;

import java.util.Date;
import javax.swing.DefaultButtonModel;
import javax.swing.Icon;
import javax.swing.JButton;

/**
 *
 * @author allan
 */
public class JButtonDate extends JButton {
    private Date date;
    
    public JButtonDate(String text, Icon icon) {
        // Create the model
        setModel(new DefaultButtonModel());

        // initialize
        init(text, icon);
    }
    
    public Date getDate(){
        return date;
    }
    
    public void setDate(Date d) {
        date = d;
    }
}
