/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.swing;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 *
 * @author allan
 */
public class ListModel<T> extends javax.swing.AbstractListModel {
    // metodo invocado pelo JList para obter o numero de elementos
    private List<T> models;
    
    public ListModel(List<T> models){
        this.models = models;
    }
    
    public ListModel(Set<T> models){
        this.models = new ArrayList<T>(models);
    }

    public int getSize() {
        return models.size();
    }

    // metodo chamado pelo JList para apresentar o elemento da linha informada
    public Object getElementAt(int linha) {
        return (Object) models.get(linha);
    }
}
