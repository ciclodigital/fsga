/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.swing;

import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.ImageIcon;

/**
 *
 * @author allan
 */
public abstract class Icons {
    private Icons(){};
    
    private static final String relativePath = System.getProperty("user.dir");
    
    public static final ImageIcon CLOSE = createIcon("icons/cross.png");
    public static final ImageIcon CICLO = createIcon("icon-ciclo.png");
    public static final ImageIcon FISIOFORMA = createIcon("icon_fisio.png");
    public static final ImageIcon LOADING = createIcon("spinner.gif");
    
    public static final ImageIcon createIcon(String image){
        return new ImageIcon(relativePath + "/img/" + image);
    }
    
    public static final Image createImage(String image){
        return Toolkit.getDefaultToolkit().getImage(relativePath + "/img/" + image);
    }
    
    public static final String pathToImage(String image){
        return relativePath + "/img/" + image;
    }
}
