/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.db;

import br.com.fisioforma.swing.ErrorMessage;
import javax.swing.JDialog;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.joda.time.DateTimeZone;

/**
 *
 * @author allan
 */
public final class Util {
    public  static final DateTimeZone timezone = DateTimeZone.forID("America/Sao_Paulo");
    private static SessionFactory sessionFactory;
    private static final Logger log = Logger.getLogger(Util.class);

    public static void fatalErrorFlow(Logger log, Exception e) {
        log.error(e.getLocalizedMessage().toString(), e);
        JDialog ew = new ErrorMessage("O sistema será finalizado devido ao erro", e);
        ew.setTitle(e.getLocalizedMessage().toString());
        ew.setVisible(true);
        System.exit(0);
    }
    
    private Util(){}
    
    static
    {
       try
       {
            Configuration configuration = new Configuration().configure();
            StandardServiceRegistryBuilder serviceRegistryBuilder = new StandardServiceRegistryBuilder();
            serviceRegistryBuilder.applySettings(configuration.getProperties());
            ServiceRegistry serviceRegistry = serviceRegistryBuilder.build();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            log.info("Sessão criada com sucesso!");
       } catch (Exception e){
            log.error("Falha ao inicializar o SessionFactory", e);
            JDialog ew = new ErrorMessage("O sistema será finalizado devido ao erro", e);
            ew.setTitle("Falha ao inicializar o SessionFactory");
            ew.setVisible(true);
            System.exit(0);
       }
    }
    
    public static SessionFactory getSessionFactory()
    {
       return sessionFactory;
    }
 
    public static void shutdown()
    {  
       getSessionFactory().close();
    }
}
