/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.db;

import br.com.fisioforma.db.Util;
import java.util.List;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 * Esta classe abstrata tem como objetivo criar um serviço ROTINA a todos
 * os modelos
 * @author allan
 */
public abstract class Service<C, T> implements IService<C, T> {

    protected Transaction transation;
    protected ValidatorFactory validationFactory = Validation.buildDefaultValidatorFactory();
    protected Validator validator = validationFactory.getValidator();
    protected Class clazz;

    public Service( Class clazz ) {
        this.clazz = clazz;
    }

    @Override
    public void persist(T entity) throws Exception {
        Session session = null;
        
        try {
            session = Util.getSessionFactory().openSession();
            
            session.beginTransaction();
            session.persist(entity);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            throw new Exception(e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
    
    @Override
    public T saveOrUpdate(T entity) throws Exception {
        Session session = null;

        try {
            session = Util.getSessionFactory().openSession();
            session.beginTransaction();
            session.saveOrUpdate(entity);
            session.getTransaction().commit();
        } catch (Exception e) {
            if ( session != null) session.getTransaction().rollback();
            throw new Exception(e);
        } finally {
            if (session != null) session.close();
        }
        
        return entity;
    }

    @Override
    public T findBy(String attName, String attValue) throws Exception {
        T model = null;
        Session session = null;
                
        try {
            session = Util.getSessionFactory().openSession();
            
            Criteria criteria = session.createCriteria(this.clazz);
            criteria.add(Restrictions.eq(attName, attValue));
            
            model = (T) criteria.uniqueResult();
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return model;
    }
    
    @Override
    public T find(int id) throws Exception {
        T model = null;
        Session session = null;
        
        try {
            session = Util.getSessionFactory().openSession();
            
            model = (T) session.get(clazz, id);
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        
        return model;
    }

    @Override
    public List<T> list() throws Exception {
        Session session = null;
        List<T> entities = null;
    
        try {
            session = Util.getSessionFactory().openSession();
            entities = session.createQuery("from " + clazz.getName()).list();
        }  catch (Exception e) {
            throw new Exception(e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        
        return entities;
    }

    @Override
    public Set<ConstraintViolation<T>> validate(T obj) {
        return validator.validate(obj);
    }

    @Override
    public String getFormatedValidationMessages(Set<ConstraintViolation<T>> validations) {
        StringBuilder messages = new StringBuilder();

        for (ConstraintViolation<T> con : validations) {
            messages.append(con.getMessage()).append("\n");
        }

        return messages.toString();
    }
    
    @Override
    public void query(AbstractServiceQuery serviceQuery) throws Exception {
//        Session session = null;
//        
//        try {
//            System.out.println("public void query(AbstractServiceQuery serviceQuery) throws Exception");
//            session = Util.getSessionFactory().openSession();
//            serviceQuery.execute(session, this);
//        } catch ( Exception e ) {
//            throw new Exception(e);
//        } finally {
//            if( session != null ) session.close();
//        }
    }
    
    @Override
    public Boolean delete(T obj) {
        Session session = null;
        Boolean result = false;
    
        try {
            session = Util.getSessionFactory().openSession();
            Transaction tx = session.beginTransaction();
            session.delete(obj);
            System.out.println("Recurso " + obj.toString() + " removido com succeso!");
            session.flush();
            tx.commit();
            result = true;
        }  catch (Exception e) {
            e.printStackTrace();
            result = false;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        
        return result;
    }
}
