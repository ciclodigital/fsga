/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.db;

import java.util.List;
import java.util.Set;
import javax.validation.ConstraintViolation;
import org.hibernate.HibernateException;

/**
 * Este interface é responsável por definir os métodos de pequisa mais comuns
 * @author Allan Batista
 */
public interface IService<C, T> {

    T findBy(String attName, String attValue) throws Exception;

    String getFormatedValidationMessages(Set<ConstraintViolation<T>> validations)  throws Exception ;

    List<T> list()  throws Exception;

    void  persist(T object) throws Exception;

    Set<ConstraintViolation<T>> validate(T obj) throws Exception;
    
    T saveOrUpdate(T entity) throws Exception;
    
    T find(int id) throws Exception;
    
    void query(AbstractServiceQuery serviceQuery) throws Exception;
    
    Boolean delete(T Object);
}
