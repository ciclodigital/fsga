/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.db;

import org.hibernate.Session;

/**
 *
 * @author allan
 */
public abstract class AbstractServiceQuery {
    public abstract void execute( Session session, IService service ); 
}
