/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.report;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author allan
 */
public class FaturamentoTest {
    
    public static List<ReportFaturamentoDataRow> createRows(){
        List<ReportFaturamentoDataRow> rows = new ArrayList<ReportFaturamentoDataRow>();
        
        for( int i= 0; i < 25; i++ ) {
            ReportFaturamentoDataRow row = new ReportFaturamentoDataRow();
            
            row.tiss = "123456";
            row.nome = "NOME DO INDIVIDUO";
            row.ciefas = "12345678";
            row.descricao = "DESCRICAO DO ATENDIMENTO COMPLETO";
            row.atendimentos = 10;
            row.valor = 10.0;
            row.total = 100.0;
            
            rows.add(row);
        }
        
        return rows;
    }
    
    public static void main(String[] args) {
        ReportFaturamentoData data = new ReportFaturamentoData("99999", createRows());
        
        Faturamento faturamento = new Faturamento("Faturamento.pdf", data);
        faturamento.generate();
        
        System.exit(0);
    }
}
