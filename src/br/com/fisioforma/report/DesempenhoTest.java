/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.report;

import static br.com.fisioforma.report.FaturamentoTest.createRows;
import java.util.ArrayList;

/**
 *
 * @author cdanner
 */
public class DesempenhoTest {
    
     public static void main(String[] args) {
              
        Desempenho desempenho = new Desempenho("Desempenho.pdf", new ReportDesempenhoData());
        desempenho.generate();
        
        System.exit(0);
    }
}
