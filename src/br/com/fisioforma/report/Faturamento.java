/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.report;

import br.com.fisioforma.pdf.HeaderSingleton;
import br.com.fisioforma.pdf.TableHeaderSingleton;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;

/**
 *
 * @author allan
 */
public class Faturamento implements IRelatorio {

    private final String path;
    private final ReportFaturamentoData data;
    private Paragraph paragraph;
    private PdfPCell pdfPCell;
    private final int SIZE_MAXIMUM_TABLE = 19;
    private int currentPosition;
    private final int listSize;
    private double grandTotal;
    private int quantityProcedures;
    
    private final Font FONT_HEADER_SECONDARY = new Font(Font.FontFamily.HELVETICA, 18, Font.NORMAL);
    private final Font FONT_HEADER_THIRTY = new Font(Font.FontFamily.HELVETICA, 24, Font.BOLD);
    private final Font FONT_HEADER_PRIMARY = new Font(Font.FontFamily.COURIER, 10, Font.NORMAL);
    
    private final Font FONT_HEADER_COD_CRED = new Font(Font.FontFamily.HELVETICA, 20, Font.BOLD);
    private final Font FONT_TABLE_HEADER = new Font(Font.FontFamily.COURIER, 8, Font.NORMAL);
    private final Font FONT_TABLE_BODY = new Font(Font.FontFamily.COURIER, 8, Font.NORMAL);
    private final Font FONT_FOOTER_TOTAL = new Font(Font.FontFamily.COURIER, 10, Font.BOLD);

    public Faturamento(String path, ReportFaturamentoData reportFaturamentoData) {       
        this.path = path;
        this.data = reportFaturamentoData;
        this.currentPosition = 0;
        this.listSize = reportFaturamentoData.getRows().size();
        this.grandTotal = 0.0;
        this.quantityProcedures = 0;
    }

    @Override
    public boolean generate() {
        createTable();
        return true;
    }

    @Override
    public String generateFileName() {
        if (path.contains(".pdf")) {
            return path;
        }
        return path + ".pdf";
    }

    private void createTable() {
        try {
            // step 1
            Document document = new Document(PageSize.A4.rotate(), 20, 20, 20, 20);
            // step 2
            PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(path));
            // step 3
            document.open();
            // step 4
            createModelTable(document);

            document.close();

        } catch (FileNotFoundException | DocumentException ex) {
            ex.printStackTrace();
        }
    }

    private void createModelTable(Document document) {
        // step 5
        createDocumentHeader(document);
        // step 6
        float[] columnWidths = { 2, 8, 2, 14, 2, 1, 2 };
        PdfPTable pdfPTable = new PdfPTable(columnWidths); // Quantidade de colunas do PDF = 6
        pdfPTable.setWidthPercentage(100);
        pdfPTable.getDefaultCell().setUseAscender(true);
        pdfPTable.getDefaultCell().setUseDescender(true);
        // step 7
        createTableHeader(pdfPTable);
        // step 8
        createTableRows(document, pdfPTable);
        // step 9
        createDocumentFooter(document, pdfPTable);
        // step 10
        if (currentPosition < listSize) {
            document.newPage();
            createModelTable(document);
        }
    }

    private void createDocumentHeader(Document document) {
        float[] columnWidths = { 2, 18 };
        PdfPTable pdfPTable = new PdfPTable(columnWidths); // Quantidade de colunas do PDF = 2
        pdfPTable.setWidthPercentage(100);
        
        PdfPCell pdfPCell = null;
        pdfPCell = new PdfPCell();
        Phrase codCred = new Phrase("CÓD. CRED.", FONT_TABLE_BODY);
        codCred.setLeading(10);
        pdfPCell.addElement(codCred);
        Phrase cod  = new Phrase(data.getCodCred(), FONT_HEADER_COD_CRED);
        cod.setLeading(18);
        pdfPCell.setVerticalAlignment(Element.ALIGN_BOTTOM);
        pdfPCell.addElement(cod);
        pdfPCell.setPaddingBottom(10);
        pdfPCell.setBorder(0);
        pdfPTable.addCell(pdfPCell);
        
        pdfPCell = new PdfPCell();
        for (int i = 0; i < HeaderSingleton.getInstance().getHeaderList().size(); i++) {
            Paragraph paragraph = null;
            
            if (i == 0){
                paragraph = new Paragraph(HeaderSingleton.getInstance().getHeaderList().get(i), FONT_HEADER_PRIMARY);
                paragraph.setLeading(10);
            }else if (i == 1){
                paragraph = new Paragraph(HeaderSingleton.getInstance().getHeaderList().get(i), FONT_HEADER_SECONDARY);
                paragraph.setLeading(20);
            }else{
                paragraph = new Paragraph(HeaderSingleton.getInstance().getHeaderList().get(i), FONT_HEADER_THIRTY);
                paragraph.setLeading(26);
            }
            
            paragraph.setAlignment(Element.ALIGN_CENTER);
            
            pdfPCell.addElement(paragraph);
        }
        
        pdfPCell.setPaddingBottom(10);
        pdfPCell.setBorder(0);
        pdfPTable.addCell(pdfPCell);
        
        try {
            document.add(pdfPTable);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    private void createTableHeader(PdfPTable pdfPTable) {

        try {
            for (int i = 0; i < TableHeaderSingleton.getInstance().getTableHeaderList().size(); i++) {
                pdfPCell = new PdfPCell(new Phrase(TableHeaderSingleton.getInstance().getTableHeaderList().get(i), FONT_TABLE_HEADER));
                pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                pdfPCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                pdfPTable.addCell(pdfPCell);
            }
            pdfPTable.setHeaderRows(1);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void createTableRows(Document document, PdfPTable pdfPTable) {
        try {
            for (int i = 0; i < SIZE_MAXIMUM_TABLE; i++) {
                if (currentPosition < listSize) {
                    fillsCellInOrder(pdfPTable, data.getRows().get(currentPosition));
                    currentPosition++;
                    quantityProcedures++;
                } else {
                    insertEmptyCell(pdfPTable);
                }
            }
            document.add(pdfPTable);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void createDocumentFooter(Document document, PdfPTable pdfPTable) {
        try {
            // a table with three columns
            float[] columnWidths = { 9, 6, 8, 4 };
            PdfPTable footerPdfPTable = new PdfPTable(columnWidths);
            footerPdfPTable.setWidthPercentage(100);
            pdfPCell = new PdfPCell(new Phrase("RECEBIDO PELA FAPES EM __/__/__", FONT_FOOTER_TOTAL));
            pdfPCell.setBorder(0);
            footerPdfPTable.addCell(pdfPCell);
            pdfPCell = new PdfPCell(new Phrase("Qtd. Procedimentos: " + quantityProcedures, FONT_HEADER_PRIMARY));
            pdfPCell.setBorder(0);
            pdfPCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            footerPdfPTable.addCell(pdfPCell);
            pdfPCell = new PdfPCell(new Phrase("TOTAL GERAL     ", FONT_FOOTER_TOTAL));
            pdfPCell.setBorder(0);
            pdfPCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            pdfPCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            footerPdfPTable.addCell(pdfPCell);
            pdfPCell = new PdfPCell(new Phrase("R$      " + formatAmount(grandTotal), FONT_FOOTER_TOTAL));
            pdfPCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pdfPCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            pdfPCell.setFixedHeight(0F);
            footerPdfPTable.addCell(pdfPCell);
            document.add(footerPdfPTable);
            
            grandTotal = 0.0;
            quantityProcedures = 0;
                
            document.add(new Paragraph(" ")); // quebra de linha
            
            float[] columnWidths2 = {4, 4, 9, 15};
            PdfPTable footerPdfPTable2 = new PdfPTable(columnWidths2);
            footerPdfPTable2.setWidthPercentage(100);
            pdfPCell = new PdfPCell(new Phrase("CONFERIDO POR", FONT_FOOTER_TOTAL));
            pdfPCell.setFixedHeight(60f);
            footerPdfPTable2.addCell(pdfPCell);
            pdfPCell = new PdfPCell(new Phrase("LANÇADO EM", FONT_FOOTER_TOTAL));
            pdfPCell.setFixedHeight(60f);
            footerPdfPTable2.addCell(pdfPCell);
            pdfPCell = new PdfPCell(new Phrase("Data de Emissão:", FONT_HEADER_PRIMARY));
            pdfPCell.setFixedHeight(60f);
            footerPdfPTable2.addCell(pdfPCell);
            pdfPCell = new PdfPCell(new Phrase("Assinatura e Carimbo do Credenciado:", FONT_HEADER_PRIMARY));
            pdfPCell.setFixedHeight(60f);
            footerPdfPTable2.addCell(pdfPCell);
            document.add(footerPdfPTable2);

            paragraph = new Paragraph("_______________________________________________________________________________________________________________________");
            paragraph.setAlignment(Element.ALIGN_CENTER);
            document.add(paragraph);

            paragraph = new Paragraph("FISIOFORMA Fisioterapia de Resultados", FONT_TABLE_BODY);
            paragraph.setAlignment(Element.ALIGN_CENTER);
            document.add(paragraph);

            paragraph = new Paragraph("Sistema Fisiometer", FONT_HEADER_PRIMARY);
            paragraph.setAlignment(Element.ALIGN_CENTER);
            document.add(paragraph);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void fillsCellInOrder(PdfPTable pdfPTable, ReportFaturamentoDataRow row) {

        for (int i = 0; i < 7; i++) {
            Phrase phase = null;
            
            switch (i) {
                case 0:
                    phase = new Phrase(row.getTissFormated(), FONT_TABLE_BODY);
                    pdfPCell = new PdfPCell(phase);
                    pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    pdfPCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    break;
                case 1:
                    phase = new Phrase(row.getNome(), FONT_TABLE_BODY);
                    pdfPCell = new PdfPCell(phase);
                    break;
                case 2:
                    phase = new Phrase(row.getCiefas(), FONT_TABLE_BODY);
                    pdfPCell = new PdfPCell(phase);
                    pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    pdfPCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    break;
                case 3:
                    phase = new Phrase(row.getDescricao(), FONT_TABLE_BODY);
                    pdfPCell = new PdfPCell(phase);
                    break;
                case 4:
                    phase = new Phrase(String.valueOf(row.getFormatedValor()), FONT_TABLE_BODY);
                    pdfPCell = new PdfPCell(phase);
                    pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    pdfPCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    break;
                case 5:
                    phase = new Phrase(String.valueOf(row.atendimentos), FONT_TABLE_BODY);
                    pdfPCell = new PdfPCell(phase);
                    pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    pdfPCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    break;
                case 6:
                    phase = new Phrase(String.valueOf(row.getFormatedTotal()), FONT_TABLE_BODY);
                    pdfPCell = new PdfPCell(phase);
                    pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    pdfPCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    grandTotal += row.getTotal();
                    break;
            }

            phase.setLeading(10);
            pdfPCell.setPadding(4);
            pdfPTable.addCell(pdfPCell);
        }
    }

    private void insertEmptyCell(PdfPTable pdfPTable) {
        for (int i = 0; i < 6; i++) {
            Phrase phase = new Phrase(" ", FONT_TABLE_BODY);
            pdfPCell = new PdfPCell(phase);
            phase.setLeading(10);
            pdfPCell.setPadding(4);
            pdfPTable.addCell(pdfPCell);
        }
    }
    
    private String formatAmount(double amount) {
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance();
        DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
        decimalFormatSymbols.setCurrencySymbol("");
        decimalFormatSymbols.setGroupingSeparator('.');
        decimalFormatSymbols.setMonetaryDecimalSeparator(',');
        ((DecimalFormat) numberFormat).setDecimalFormatSymbols(decimalFormatSymbols);
        return numberFormat.format(amount);
    }
}