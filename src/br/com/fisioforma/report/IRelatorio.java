/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.report;

/**
 *
 * @author allan
 */
public interface IRelatorio {
    public boolean generate();
    public String  generateFileName();
}
