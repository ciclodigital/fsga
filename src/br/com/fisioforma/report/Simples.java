/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.report;

import br.com.fisioforma.pdf.LineTypeEnum;
import br.com.fisioforma.pdf.PdfProvider;
import br.com.fisioforma.pdf.PrintLine;
import br.com.fisioforma.pdf.TableType;
import br.com.fisioforma.pdf.TextType;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import java.util.ArrayList;

/**
 *
 * @author allan
 */
public class Simples  implements IRelatorio {
    
    private final Font TEXT_BLACK = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.BLACK);
    private final Font TEXT_BLACK_ITALIC = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.ITALIC, BaseColor.BLACK);
    
    private PdfProvider provider = new PdfProvider();
    private String[]    headers  = {};
    private String[][]  lines    = {};
    private String      header   = "";
    private String      obs      = "";
    private String      footer   = "Fisioforma \n" +
                                   "Francinett Dias \n" +
                                   "Nivalda Marques";
    private final String      path;
    
    public Simples(String path) {
        this.path = path;
        this.provider.setmIsPortrait(true);
    }

    @Override
    public boolean generate() {
        provider.setPath(generateFileName());
        
        createHeader(provider.getLinesToPrint());
         createTable(provider.getLinesToPrint());
        createFooter(provider.getLinesToPrint());
        
        try {
            return provider.execute();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        } finally {
            return false;
        }
    }
    
    private void addEmptyLines(int times, ArrayList<PrintLine> lines ){
        for (int i = 0; i < times; i++) {
            TextType type = new TextType(" ", Element.ALIGN_CENTER, TEXT_BLACK);
            lines.add(new PrintLine(LineTypeEnum.TEXT, (TextType) type));
        }
    }
    
    private void createTable(ArrayList<PrintLine> lines){
        TableType tableType = new TableType();
        ArrayList<ArrayList<TextType>> tableTypeList = new ArrayList<>();
        tableTypeList.add(0, createTableHeaders());
        tableTypeList.add(1, createTableBody());
        tableType.setTableTypeList(tableTypeList);
        lines.add(new PrintLine(LineTypeEnum.TABLE, (TableType) tableType));
    }
    
    private ArrayList<TextType> createTableHeaders(){
        ArrayList<TextType> headerList = new ArrayList<>();
        
        for( String text : headers ) {
            TextType type = new TextType(text, Element.ALIGN_CENTER, TEXT_BLACK);
            headerList.add(type);
        }
        
        return headerList;
    }
    
    private ArrayList<TextType> createTableBody(){
        ArrayList<TextType> rowsList = new ArrayList<>();
        
        for( String[] line : lines ){
            ArrayList<TextType> row = new ArrayList<TextType>();
            
            for( String column : line ) {
                TextType type = new TextType(column, Element.ALIGN_CENTER, TEXT_BLACK);
                rowsList.add( type );
            }
        } 
        
        return rowsList;
    }
    
    private void createHeader(ArrayList<PrintLine> lines){
        TextType type = new TextType(header, Element.ALIGN_CENTER, TEXT_BLACK);
        
        addEmptyLines(3, lines);
        lines.add(new PrintLine(LineTypeEnum.TEXT, (TextType) type));
        addEmptyLines(3, lines);
    }
    
    private void createFooter( ArrayList<PrintLine> lines ){
        TextType obsType = new TextType(obs, Element.ALIGN_JUSTIFIED, TEXT_BLACK_ITALIC);
        TextType footerType = new TextType(footer, Element.ALIGN_LEFT, TEXT_BLACK);
        
        lines.add(new PrintLine(LineTypeEnum.TEXT, (TextType) obsType));
        addEmptyLines(3, lines);
        lines.add(new PrintLine(LineTypeEnum.TEXT, (TextType) footerType));
    }
    
    @Override
    public String  generateFileName() {
        if ( path.contains(".pdf") ) return path;
        return path + ".pdf";
    }
    
    
    public PdfProvider getProvider() {
        return provider;
    }

    public void setProvider(PdfProvider provider) {
        this.provider = provider;
    }

    public String[] getHeaders() {
        return headers;
    }

    public void setHeaders(String[] headers) {
        this.headers = headers;
    }

    public String[][] getLines() {
        return lines;
    }

    public void setLines(String[][] lines) {
        this.lines = lines;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public String getFooter() {
        return footer;
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }
    
}
