/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.report;

import br.com.fisioforma.model.Empresa;
import br.com.fisioforma.model.SatisfacaoEnum;
import com.itextpdf.text.pdf.StringUtils;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.joda.time.LocalDate;

/**
 *
 * @author allan
 */
public class ReportDesempenhoData {
    private final DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    
    // Header
    public List<Empresa> empresas = new ArrayList<Empresa>();
    public String rua = "";
    public Date inicio;
    public Date fim;
    
    // Demanda Consolidada
    public long capacidadeAtendimento = 1;
    public long atendimentosSolicitados = 1;
    
    //  Origem do Paciente  
    public int pas;
    public int pat;
    public int eme;
    
    // Atividade | Capacidade de Atendimento | Demanda Solicitada | % Demanda / Capacidade | Atendimentos |  % Ocupacao
    public List<String[]> departamentos = new ArrayList<String[]>();
    
    public int demanda = 0;
    public int atendimentos = 0;
    public int faltas = 0;
    public int altas = 0;
    public int concluidas = 0;
    public int emAndamento = 0;
    public int totalGuias = 0;

    public Map<String, Integer> fluxoDeDemanda = new HashMap<String, Integer>();
    public Map<String, Integer> fluxoTiss = new HashMap<String, Integer>();
    public Map<String, Integer> satisfacao = new HashMap<String, Integer>();
    public Map<String, Integer> perdas = new HashMap<String, Integer>();
    public Map<String, Integer> demandas = new HashMap<String, Integer>();
    public Map<String, Integer> procedimentos = new HashMap<String, Integer>();

    public List<String[]> getDepartamentos() {
        return departamentos;
    }

    public String getFormatedDate() {
        return df.format(inicio) + " à " + df.format(fim);
    }

    public String getFormatedEmpresas() {
        List<String> list = new ArrayList<String>();

        for( Empresa empresa : empresas ) {
            list.add(empresa.getNome());
        }

        return String.join(" / ", list);
    }
    
    public double getTaxaDeOcupacao(){
        return atendimentosSolicitados * 100 / capacidadeAtendimento;
    }

    public double getTaxaDeOcupacaoEfetiva() {
        return ( atendimentosSolicitados - faltas ) * 100 / capacidadeAtendimento;
    }

    public Map<String, Integer> getFluxoDeDemanda() {
        return fluxoDeDemanda;
    }

    public Map<String, Integer> getFluxoTiss() {
        return fluxoTiss;
    }

    public Map<String, Integer> getPerdas() {
        return perdas;
    }
    
    
}
