/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.report;

import java.text.NumberFormat;

/**
 *
 * @author allan
 */
public class ReportFaturamentoDataRow {
    public String tiss;
    public String nome;
    public String ciefas;
    public String descricao;
    public long atendimentos;
    public double valor;
    public double total;
    
    public String getTiss() {
        return tiss;
    }

    public String getNome() {
        return nome;
    }

    public String getCiefas() {
        return ciefas;
    }

    public String getDescricao() {
        return descricao;
    }

    public long getAtendimentos() {
        return atendimentos;
    }

    public double getValor() {
        return valor;
    }
    
    public String getFormatedValor(){
        return NumberFormat.getCurrencyInstance().format( getValor() ).replace("R$ ", "");
    }

    public double getTotal() {
        return total;
    }
    
    public String getFormatedTotal(){
        return NumberFormat.getCurrencyInstance().format( getTotal() ).replace("R$ ", "");
    }
    
    public String toString(){
        return tiss + " - " + nome + " - " + ciefas + " - " + descricao + " - " + atendimentos + " - " + valor + " - " + total;
    }

    public String getTissFormated() {
        return getTiss().substring(getTiss().length() - 6);
    }
}
