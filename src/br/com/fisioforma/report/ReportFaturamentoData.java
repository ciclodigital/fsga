/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.report;

import java.util.List;

/**
 *
 * @author allan
 */
public class ReportFaturamentoData {
    private String codCred;
    private List<ReportFaturamentoDataRow> rows;
    
    public ReportFaturamentoData(String codCred) {
        this.codCred = codCred;
    }
    
    public ReportFaturamentoData(String codCred, List<ReportFaturamentoDataRow> rows) {
        this.codCred = codCred;
        this.rows = rows;
    }

    /**
     * @return the codCred
     */
    public String getCodCred() {
        return codCred;
    }

    /**
     * @param codCred the codCred to set
     */
    public void setCodCred(String codCred) {
        this.codCred = codCred;
    }

    /**
     * @return the rows
     */
    public List<ReportFaturamentoDataRow> getRows() {
        return rows;
    }

    /**
     * @param rows the rows to set
     */
    public void setRows(List<ReportFaturamentoDataRow> rows) {
        this.rows = rows;
    }
}
