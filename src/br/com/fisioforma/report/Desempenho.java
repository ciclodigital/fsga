/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.report;

import br.com.fisioforma.model.SatisfacaoEnum;
import br.com.fisioforma.pdf.DepartamentoSingleton;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;

import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.plot.PlotOrientation;
import com.itextpdf.awt.PdfGraphics2D;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCellEvent;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.title.LegendTitle;
import org.jfree.ui.RectangleEdge;

/**
 *
 * @author allan
 */
public class Desempenho implements IRelatorio {

    private final String path;
    private final ReportDesempenhoData data;
    private Paragraph paragraph;
    private PdfPCell pdfPCell;

    private final Font FONT_HEADER_LOGO = new Font(Font.FontFamily.COURIER, 20, Font.BOLD, BaseColor.BLUE);
    private final Font FONT_TEXT_DEFAULT = new Font(Font.FontFamily.COURIER, 8, Font.NORMAL);
    private final Font FONT_TEXT_DEFAULT_BOLD = new Font(Font.FontFamily.COURIER, 8, Font.BOLD);
    private final Font FONT_TEXT_DEFAULT_LITTLE = new Font(Font.FontFamily.COURIER, 5, Font.NORMAL);
    private final Font FONT_TEXT_DEFAULT_ACTIVITIES = new Font(Font.FontFamily.COURIER, 7, Font.BOLD);
    private final Font FONT_TEXT_DEFAULT_BLANK = new Font(Font.FontFamily.COURIER, 0, Font.BOLD);
    private final Font FONT_TEXT_DEFAULT_BLUE = new Font(Font.FontFamily.COURIER, 7, Font.NORMAL, BaseColor.BLUE);
    private final java.awt.Font FONT_CHART_TITLE_BOLD = new java.awt.Font("COURIER", 8, Font.BOLD);

    public Desempenho(String path, ReportDesempenhoData reportDesempenhoData) {
        this.data = reportDesempenhoData;
        this.path = path;
    }

    @Override
    public boolean generate() {
        try {
            createTable();
        } catch (IOException ex) {
            Logger.getLogger(Desempenho.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }

    @Override
    public String generateFileName() {
        if (path.contains(".pdf")) {
            return path;
        }
        return path + ".pdf";
    }

    private void createTable() throws IOException {
        try {
            Document document = new Document(PageSize.A4, 20, 20, 20, 20);
            // step 2
            PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(path));
            // step 3
            document.open();
            // step 4
            createTableHeader(document);
            
            consolidatedDemandTable(document);
                        
            activitiesTable(document);
         
            charts(document);   
            
            document.add(new Paragraph(" ", FONT_TEXT_DEFAULT_ACTIVITIES));
            
            satisfaction(document);
            
            consolidatedDemandTable2(document);
            
            paragraph = new Paragraph("FISIOFORMA", FONT_TEXT_DEFAULT);
            paragraph.setAlignment(Element.ALIGN_CENTER);
            document.add(paragraph);
            
            document.close();
        } catch (FileNotFoundException | DocumentException ex) {
            ex.printStackTrace();
        }
    }

    private void createTableHeader(Document document) {
        try {
            float[] columnWidths = {1, 1, 1};
            PdfPTable pdfPTable = new PdfPTable(columnWidths); //
            pdfPTable.setWidthPercentage(100);
            pdfPCell = new PdfPCell();
            pdfPCell.setBorder(0);
            pdfPCell.setPaddingBottom(5);
            Phrase logo = new Phrase("Fisioforma", FONT_HEADER_LOGO);
            logo.setLeading(20f);
            pdfPCell.addElement(logo);
            pdfPCell.addElement(new Phrase("Fisioterapia de resultados", FONT_TEXT_DEFAULT));
            pdfPTable.addCell(pdfPCell);
            
            pdfPCell = new PdfPCell();
            pdfPCell.setBorder(0);
            pdfPCell.setPaddingBottom(5);
            pdfPCell.addElement(new Phrase("Empresa..: " + data.getFormatedEmpresas() , FONT_TEXT_DEFAULT));
            pdfPCell.addElement(new Phrase("Rua......: " + data.rua , FONT_TEXT_DEFAULT));
            pdfPCell.setVerticalAlignment(Element.ALIGN_BOTTOM);
            pdfPTable.addCell(pdfPCell);
            
            pdfPCell = new PdfPCell();
            pdfPCell.setBorder(0);
            pdfPCell.setPaddingBottom(5);
            pdfPCell.addElement(new Phrase("Data:", FONT_TEXT_DEFAULT));
            pdfPCell.addElement(new Phrase(data.getFormatedDate(), FONT_TEXT_DEFAULT));
            pdfPCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pdfPCell.setVerticalAlignment(Element.ALIGN_BOTTOM);
            pdfPTable.addCell(pdfPCell);
            
            addBoxBorder(document, pdfPTable);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void consolidatedDemandTable(Document document) {
        try {
            float[] columnWidths = {1, 1, 1};
            PdfPTable pdfPTable = new PdfPTable(columnWidths);
            pdfPTable.setWidthPercentage(100);
            pdfPCell = new PdfPCell();
            pdfPCell.setBorder(0);
            pdfPCell.setPaddingBottom(5);
            pdfPCell.addElement(new Phrase("Demanda Consolidade", FONT_TEXT_DEFAULT_BOLD));
            pdfPCell.addElement(new Phrase("Capacidade de Atendimento : " + data.capacidadeAtendimento , FONT_TEXT_DEFAULT));
            pdfPCell.addElement(new Phrase("Solicitação de Atendimento: " + data.atendimentosSolicitados, FONT_TEXT_DEFAULT));
            pdfPCell.addElement(new Phrase("Taxa de Ocupação Demanda  : " + data.getTaxaDeOcupacao() + "%", FONT_TEXT_DEFAULT));
            pdfPCell.addElement(new Phrase("Taxa de Ocupação Efetivada: " + data.getTaxaDeOcupacaoEfetiva() + "%", FONT_TEXT_DEFAULT));
            pdfPTable.addCell(pdfPCell);
            
            pdfPCell = new PdfPCell();
            pdfPCell.setPaddingBottom(5);
            pdfPCell.setBorder(0);
            pdfPTable.addCell( pdfPCell );
            
            pdfPCell = new PdfPCell();
            pdfPCell.setBorder(PdfPCell.LEFT);
            pdfPCell.setPaddingBottom(5);
            pdfPCell.addElement(new Phrase("Origem do paciente:", FONT_TEXT_DEFAULT_BOLD));
            pdfPCell.addElement(new Phrase("PAS: "+ data.pas +"   PAT: " + data.pat, FONT_TEXT_DEFAULT));
            pdfPCell.addElement(new Phrase("EME: "+ data.eme, FONT_TEXT_DEFAULT));
            pdfPCell.addElement(new Phrase("PAS = Paciente Assistência Suplementar", FONT_TEXT_DEFAULT_LITTLE));
            pdfPCell.addElement(new Phrase("EME = Paciente Emergencia", FONT_TEXT_DEFAULT_LITTLE));
            pdfPCell.addElement(new Phrase("PAT = Paciente Acidente de Trabalho", FONT_TEXT_DEFAULT_LITTLE));
            pdfPCell.setVerticalAlignment(Element.ALIGN_BOTTOM);
            pdfPTable.addCell(pdfPCell);
            
            addBoxBorder(document, pdfPTable);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void activitiesTable(Document document) {
         try {
            float[] columnWidths = {3, 3, 3, 4, 3, 3};
            PdfPTable pdfPTable = new PdfPTable(columnWidths);
            pdfPTable.setWidthPercentage(100);
            
            for (int i = 0; i < DepartamentoSingleton.getInstance().getHeaderList().size(); i++) {
                Phrase phase = new Phrase(DepartamentoSingleton.getInstance().getHeaderList().get(i), FONT_TEXT_DEFAULT_ACTIVITIES);
                pdfPCell = new PdfPCell(phase);
                pdfPCell.setPaddingBottom(4);
                pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                pdfPTable.addCell(pdfPCell);
            }
            pdfPTable.setHeaderRows(1);

            for( String[] row : data.departamentos ) {
                insertRowsCell(pdfPTable, row);
            }

            addBoxBorder(document, pdfPTable);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void addBoxBorder(Document document, PdfPTable pdfPTable) throws Exception {
        float[] newcolumnWidths = {1};
        PdfPTable wrapper = new PdfPTable(newcolumnWidths); //
        wrapper.setWidthPercentage(100);
        wrapper.getDefaultCell().setBorder(PdfPCell.BOX);
        wrapper.getDefaultCell().setPadding(0);
        wrapper.addCell(pdfPTable);

        document.add(wrapper);
    }
    
    private void insertRowsCell(PdfPTable pdfPTable, String[] row) {
        for (int i = 0; i < 6; i++) {
            Phrase phase = new Phrase(row[i], FONT_TEXT_DEFAULT_ACTIVITIES);
            pdfPCell = new PdfPCell(phase);
            pdfPCell.setPaddingBottom(4);
            pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            pdfPTable.addCell(pdfPCell);
        }
    }
    
    private JFreeChart generateBarChart(Map<String, Integer> chart, String title) throws IOException {
                
        DefaultCategoryDataset dataSet = new DefaultCategoryDataset();
         
        for (Map.Entry<String, Integer> entry : chart.entrySet()) {
            dataSet.setValue(entry.getValue(), entry.getKey(), entry.getValue());
        }

        JFreeChart jFreeChart = ChartFactory.createBarChart(title, " ", " ", dataSet, PlotOrientation.VERTICAL, false, true, false);

        jFreeChart.getTitle().setFont(new java.awt.Font("Courier New", java.awt.Font.PLAIN, 7));

        CategoryPlot plot = (CategoryPlot) jFreeChart.getPlot();
        plot.setBackgroundPaint(Color.white);

        plot.getDomainAxis().setTickLabelFont(new java.awt.Font("Courier New", java.awt.Font.PLAIN, 7));
        plot.getRangeAxis().setTickLabelFont(new java.awt.Font("Courier New", java.awt.Font.PLAIN, 7));
        
        LegendTitle legendTitle = new LegendTitle(plot.getRenderer(0));
        legendTitle.setItemFont(new java.awt.Font("Courier New", java.awt.Font.PLAIN, 6));
        legendTitle.setPosition(RectangleEdge.TOP); 
        jFreeChart.addLegend(legendTitle); 

        return jFreeChart;
    }
    
    private void charts(Document document) throws IOException {
        try {
            PdfPTable pdfPTable = new PdfPTable(2);
            pdfPTable.setWidthPercentage(100);
            pdfPCell = new PdfPCell();
            pdfPCell.setBorder(0);
            pdfPCell.setCellEvent(new JFreeChartEvent(generateBarChart(data.getFluxoDeDemanda(), "Fluxo da Demanda")));
            pdfPCell.setFixedHeight(180.f);
            pdfPTable.addCell(pdfPCell);

            pdfPCell = new PdfPCell();
            pdfPCell.setBorder(0);
            pdfPCell.setCellEvent(new JFreeChartEvent(generateBarChart(data.getFluxoTiss(), "Fluxo TISS")));
            pdfPCell.setFixedHeight(180.f);
            pdfPTable.addCell(pdfPCell);
            
            PdfPTable npdfPTable = new PdfPTable(1);
            npdfPTable.setWidthPercentage(100);
            npdfPTable.getDefaultCell().setBorder(0);
            npdfPTable.addCell(new Paragraph(" ", FONT_TEXT_DEFAULT_BLANK));
            npdfPTable.addCell(pdfPTable);
            npdfPTable.addCell(new Paragraph(" ", FONT_TEXT_DEFAULT_BLANK));

            addBoxBorder(document, npdfPTable);
        } catch (DocumentException ex) {
            Logger.getLogger(Desempenho.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(Desempenho.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void satisfaction(Document document) throws IOException {
        try {
            PdfPTable pdfPTable = new PdfPTable(2);
            pdfPTable.setWidthPercentage(100);
            pdfPCell = new PdfPCell();
            pdfPCell.setBorder(0);
            pdfPCell.setFixedHeight(180.f);
            pdfPCell.addElement(new Phrase("Satisfação do cliente FAPES quanto ao atendimento", FONT_TEXT_DEFAULT_BLUE));
            pdfPCell.addElement(new Phrase(" ", FONT_TEXT_DEFAULT));
            pdfPCell.addElement(new Phrase("Superou as expectativas:        " + data.satisfacao.get( SatisfacaoEnum.SUPEROU_AS_ESPECTATIVAS.toString() ), FONT_TEXT_DEFAULT));
            pdfPCell.addElement(new Phrase("             Satisfeito:        " + data.satisfacao.get( SatisfacaoEnum.SATISFEITO.toString() ), FONT_TEXT_DEFAULT));
            pdfPCell.addElement(new Phrase("Parcialmente Satisfeito:        " + data.satisfacao.get( SatisfacaoEnum.PARCIALMENTE_SATISFEITO.toString() ), FONT_TEXT_DEFAULT));
            pdfPCell.addElement(new Phrase("           Insatisfeito:        " + data.satisfacao.get( SatisfacaoEnum.INSATISFEITO.toString() ), FONT_TEXT_DEFAULT));
            pdfPCell.addElement(new Phrase(" ", FONT_TEXT_DEFAULT));
            pdfPCell.addElement(new Phrase(" ", FONT_TEXT_DEFAULT));
            pdfPCell.addElement(new Phrase("  Total de Atendimentos:       " + data.atendimentos, FONT_TEXT_DEFAULT));
            pdfPCell.addElement(new Phrase("         Total de Altas:       " + data.altas, FONT_TEXT_DEFAULT));
            pdfPTable.addCell(pdfPCell);
            
            pdfPCell = new PdfPCell();
            pdfPCell.setBorder(0);
            pdfPCell.setCellEvent(new JFreeChartEvent(generateBarChart(data.getPerdas(), "Distribuição das Perdas")));
            pdfPCell.setFixedHeight(180.f);
            pdfPTable.addCell(pdfPCell);
            
            PdfPTable npdfPTable = new PdfPTable(1);
            npdfPTable.setWidthPercentage(100);
            npdfPTable.getDefaultCell().setBorder(0);
            npdfPTable.addCell(new Paragraph(" ", FONT_TEXT_DEFAULT_BLANK));
            npdfPTable.addCell(pdfPTable);
            npdfPTable.addCell(new Paragraph(" ", FONT_TEXT_DEFAULT_BLANK));

            addBoxBorder(document, npdfPTable);
        } catch (DocumentException ex) {
            Logger.getLogger(Desempenho.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(Desempenho.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void consolidatedDemandTable2(Document document) {
        try {
            PdfPTable pdfPTable = new PdfPTable(2); //
            pdfPTable.setWidthPercentage(100);
            pdfPCell = new PdfPCell();
            pdfPCell.setBorder(0);
           
            pdfPCell.addElement(new Phrase("Demanda por localização", FONT_TEXT_DEFAULT_BOLD));
            for( Map.Entry<String, Integer> entry : data.demandas.entrySet() ) {
                pdfPCell.addElement(new Phrase(entry.getKey() + ": " +entry.getValue(), FONT_TEXT_DEFAULT));
            }
            
            pdfPCell.setPaddingBottom(4);
            pdfPTable.addCell(pdfPCell);
            pdfPCell = new PdfPCell();
            pdfPCell.setBorder(0);
            pdfPCell.setPaddingLeft(20);
            pdfPCell.addElement(new Phrase("Procedimentos Consolidados", FONT_TEXT_DEFAULT_BOLD));
            
            for( Map.Entry<String, Integer> entry : data.procedimentos.entrySet() ) {
                pdfPCell.addElement(new Phrase(entry.getKey() + ": " +entry.getValue(), FONT_TEXT_DEFAULT));
            }

            pdfPCell.setPaddingBottom(4);
            pdfPTable.addCell(pdfPCell);
            
            addBoxBorder(document, pdfPTable);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public class JFreeChartEvent implements PdfPCellEvent {

        private final JFreeChart jFreeChart;

        public JFreeChartEvent(final JFreeChart jFreeChart) {
            this.jFreeChart = jFreeChart;
        }

        @Override
        public void cellLayout(PdfPCell arg0, Rectangle arg1, PdfContentByte[] arg2) {
            PdfContentByte cb = arg2[PdfPTable.TEXTCANVAS]; //optional, can be other canvas
            PdfTemplate pie = cb.createTemplate(arg1.getWidth(), arg1.getHeight());
            Graphics2D g2d1 = new PdfGraphics2D(pie, arg1.getWidth(), arg1.getHeight());
            Rectangle2D r2d1 = new Rectangle2D.Double(0, 0, arg1.getWidth(), arg1.getHeight());
            jFreeChart.draw(g2d1, r2d1);
            g2d1.dispose();
            cb.addTemplate(pie, arg1.getLeft(), arg1.getBottom());
        }
    }
}
