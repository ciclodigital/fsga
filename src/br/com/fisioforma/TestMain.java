/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma;

import br.com.fisioforma.db.Util;
import br.com.fisioforma.model.Atendimento;
import br.com.fisioforma.model.AtendimentoStatusEnum;
import br.com.fisioforma.model.Empresa;
import br.com.fisioforma.model.Paciente;
import br.com.fisioforma.model.Recurso;
import br.com.fisioforma.model.Setor;
import br.com.fisioforma.model.Tratamento;
import br.com.fisioforma.model.Usuario;
import br.com.fisioforma.model.service.EmpresaService;
import br.com.fisioforma.model.service.RecursoService;
import br.com.fisioforma.model.service.SetorService;
import br.com.fisioforma.report.ReportFaturamentoData;
import br.com.fisioforma.report.ReportFaturamentoDataRow;
import br.com.fisioforma.swing.ErrorMessage;
import br.com.fisioforma.view.Login;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author allan
 */
public class TestMain {
    private static final Logger log = Logger.getLogger(Main.class.getName());
    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) {       
        List<Object[]> group = new ArrayList<Object[]>();
        
        Session session = null;
        
        try {
            session = Util.getSessionFactory().openSession();
            Query query = session.createQuery("SELECT a FROM Atendimento a WHERE a.data BETWEEN '2017-05-01' AND '2017-05-31'");
            Query queryTratamentos = session.createQuery("SELECT distinct t FROM Atendimento a LEFT JOIN a.tratamento t WHERE a.data BETWEEN '2017-05-01' AND '2017-05-31'");
            
            List<Atendimento> atendimentos = (List<Atendimento>) query.list();
            List<Setor> setores = SetorService.getInstance().list();
            List<Tratamento> tratamentos = (List<Tratamento>) queryTratamentos.list();
            
            Map<Setor, Paciente> setorPaciente = new HashMap<Setor, Paciente>();
            
            for( Tratamento tratamento : tratamentos  ) {
                System.out.println(tratamento.getSetor().getNome() + "," + tratamento.getPaciente().getNome());
//               System.out.println( item.getTratamento().getPaciente().getNome() + "," + item.getData() + " , " + item.getTratamento().getSetor().getNome() );
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        } finally {
            if( session == null ) { session.close(); } 
        }
        
        System.exit(0);
    }
}
