/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.util;

import br.com.fisioforma.model.Usuario;
import br.com.fisioforma.model.service.UsuarioService;
import org.apache.log4j.Logger;

/**
 *
 * @author allan
 */
public abstract class Autenticacao {
    private static final Logger log = Logger.getLogger(Autenticacao.class);
    private static Usuario usuarioAutenticado = null;
    
    public static Usuario login(String login, String senha) throws AutenticacaoException, Exception {
        Usuario usuario = null;
        
        try {
            usuario = UsuarioService.getInstance().findBy("login", login);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }        
        
        if( usuario != null && usuario.getSenha().equals(senha) ) {
            usuarioAutenticado = usuario;
        } else {
            throw new AutenticacaoException("Usuario ou senha incorretos");
        }
        
        return getUsuarioAutenticado();
    }
    
    
    public static Usuario getUsuarioAutenticado(){
        return usuarioAutenticado;
    }
    
    public static boolean isLogged (){
        return usuarioAutenticado != null;
    }
}
