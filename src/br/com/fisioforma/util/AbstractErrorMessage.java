/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.util;

import br.com.fisioforma.swing.ErrorMessage;
import javax.swing.JDialog;
import org.apache.log4j.Logger;

/**
 *
 * @author allan
 */
public abstract class AbstractErrorMessage {
    private static final Logger log = Logger.getLogger(AbstractErrorMessage.class);
    public abstract void execute() throws Exception;
    
    public AbstractErrorMessage() {
        try {
            this.execute();
        } catch (Exception e) {
            log.error(e.getLocalizedMessage().toString(), e);
            JDialog ew = new ErrorMessage("O sistema será finalizado devido ao erro", e);
            ew.setTitle(e.getLocalizedMessage().toString());
            ew.setVisible(true);
            System.exit(0);
        }
    }
}
