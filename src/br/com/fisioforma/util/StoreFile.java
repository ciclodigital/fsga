/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.util;

import br.com.fisioforma.db.Util;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import org.apache.log4j.Logger;


/**
 *
 * @author allan
 */
public class StoreFile {
    private static final Logger log = Logger.getLogger(StoreFile.class);
    static private String path = System.getProperty("user.dir") + "/db/store";
    private StoreFile() {}
    
    static {
        File dir = new File(path);
        
        if( !dir.exists() ) {
            if( dir.mkdirs() ) {
                log.info("Diretórios criados com sucesso");
            } else {
                log.warn("Não foi possível criar os diretórios");
            }
        }
    }
    
    static public boolean store(String source, String target){
        Path sourcePath = new File(source).toPath();
        Path targetPath = new File(path + "/" + target).toPath();
        
        try {
            Files.copy(sourcePath, targetPath);
            return true;
        } catch (Exception e){
            Util.fatalErrorFlow(log, e);
            return false;
        }
    }
    
    static public String getStorePath(String target){
        return path + "/" + target;
    }
}
