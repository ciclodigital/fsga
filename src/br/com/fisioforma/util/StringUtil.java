/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.util;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author allan
 */
public class StringUtil {
    private static NumberFormat numberFormat = NumberFormat.getIntegerInstance();
    
    public static Integer stringToInteger( String enter ) throws ParseException {
        enter = enter.replaceAll("[\\s()-]", "").replaceAll("[\\s().]", "");
        
        if( enter.length() == 0 ) return null;
        
        return numberFormat.parse(enter).intValue();
    }
    
    public static String onlyLetter(String string){
        return string.replaceAll("\\D+","");
    }
    
    public static Date toDate( String format, String string ) throws ParseException {
        DateFormat formatter = new SimpleDateFormat(format);
        return (Date) formatter.parse(string);
    }
}
