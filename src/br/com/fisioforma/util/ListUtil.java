/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fisioforma.util;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author allan
 */
public class ListUtil {
    private ListUtil(){}
    
    public static <T> List<List<T>> group( final List<T> list, final int batchSize){
        List<List<T>> groups = new ArrayList<List<T>>();
        
        for(int start = 0; start < list.size(); start += batchSize) {
            groups.add(list.subList(start, Math.min(start + batchSize, list.size())));
        }
        
        return groups;
    }
}
