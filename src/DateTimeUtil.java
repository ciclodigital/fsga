
import java.util.Calendar;
import java.util.Date;
import org.joda.time.LocalTime;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author allan
 */
public abstract class DateTimeUtil {
    private DateTimeUtil(){};
    
    public long getOnlyTime(Date date){
        LocalTime l = new LocalTime(date);
        return horaToMilisseconds( l.getHourOfDay() ) + minutoToMilisseconds( l.getMinuteOfHour() ) + segundoToMilisseconds( l.getSecondOfMinute() );
    }
    
    private long horaToMilisseconds(int hora){
        return hora * 60 * 60 * 1000;
    }
    
    private long minutoToMilisseconds(int minuto){
        return minuto * 60 * 1000;
    }
    
    private long segundoToMilisseconds(int segundo){
        return segundo * 1000;
    }
}
